package com.globallogic.koszalin.shared.view;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.IntDef;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;

import com.globallogic.koszalin.shared.Utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * AppCompatMsgDialog
 * <hr /> Created by damian.giedrys on 2016-05-17.
 */
@SuppressWarnings({"WeakerAccess", "UnusedReturnValue", "SameParameterValue"})
public class AppCompatMsgDialog extends DialogFragment implements DialogInterface.OnClickListener {

    @IntDef({TYPE_MSG, TYPE_QUESTION_YES_NO, TYPE_OK_CANCEL, TYPE_WARNING, TYPE_INPUT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {
    }

    public static final int TYPE_MSG = 1;
    public static final int TYPE_QUESTION_YES_NO = 2;
    public static final int TYPE_OK_CANCEL = 3;
    public static final int TYPE_WARNING = 4;
    public static final int TYPE_INPUT = 5;

    public static final String BUNDLE_KEY_MSG = "bundle_key_msg";
    public static final String BUNDLE_KEY_TITLE = "bundle_key_title";
    public static final String BUNDLE_KEY_TYPE = "bundle_key_type";
    public static final String BUNDLE_KEY_CALLBACK = "bundle_key_callback";
    public static final String BUNDLE_KEY_TAG = "bundle_key_tag";

    private static final String TAG = AppCompatMsgDialog.class.getSimpleName();
    private static long ID = 0;

    // info
    public static AppCompatMsgDialog info(@NonNull FragmentManager fm, String msg) {
        return info(fm, null, msg, null);
    }

    public static AppCompatMsgDialog info(@NonNull FragmentManager fm, String title, String msg) {
        return info(fm, title, msg, null);
    }

    public static AppCompatMsgDialog info(@NonNull FragmentManager fm, String msg, ResultReceiver callback) {
        return info(fm, null, msg, callback);
    }

    public static AppCompatMsgDialog info(@NonNull FragmentManager fm, String title, String msg,
                                          ResultReceiver callback) {
        return createInstance(fm, TYPE_MSG, title, msg, callback);
    }

    // warning
    public static AppCompatMsgDialog warning(@NonNull FragmentManager fm, String msg) {
        return warning(fm, msg, null);
    }

    public static AppCompatMsgDialog warning(@NonNull FragmentManager fm, String msg, ResultReceiver callback) {
        return warning(fm, null, msg, callback);
    }

    public static AppCompatMsgDialog warning(@NonNull FragmentManager fm, String title, String msg, ResultReceiver callback) {
        return createInstance(fm, TYPE_WARNING, title, msg, callback);
    }

    // question
    public static AppCompatMsgDialog question(@NonNull FragmentManager fm, String msg, ResultReceiver callback) {
        return question(fm, null, msg, callback);
    }

    public static AppCompatMsgDialog question(@NonNull FragmentManager fm, String title, String msg, ResultReceiver callback) {
        return createInstance(fm, TYPE_QUESTION_YES_NO, title, msg, callback);
    }

    // ok|cancel
    public static AppCompatMsgDialog ok_cancel(@NonNull FragmentManager fm, String msg, ResultReceiver callback) {
        return ok_cancel(fm, null, msg, callback);
    }

    public static AppCompatMsgDialog ok_cancel(@NonNull FragmentManager fm, String title, String msg, ResultReceiver callback) {
        return createInstance(fm, TYPE_OK_CANCEL, title, msg, callback);
    }

    // custom view
    public static AppCompatMsgDialog customView(@NonNull FragmentManager fm, String msg, ResultReceiver callback, @LayoutRes int customView) {
        return createInstance(fm, TYPE_OK_CANCEL, null, msg, callback);
    }


    private static AppCompatMsgDialog createInstance(@NonNull FragmentManager fm, @Type int type, String msg,
                                                     ResultReceiver callback) {
        return createInstance(fm, type, null, msg, callback);
    }


    private static AppCompatMsgDialog createInstance(@NonNull FragmentManager fm, @Type int type, String title,
                                                     String msg, ResultReceiver callback) {

        AppCompatMsgDialog ret = new AppCompatMsgDialog();

        Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_MSG, msg);
        args.putString(BUNDLE_KEY_TITLE, title);
        args.putInt(BUNDLE_KEY_TYPE, type);
        args.putParcelable(BUNDLE_KEY_CALLBACK, callback);

        if (ID == Long.MAX_VALUE - 1) {
            ID = 0;
        } else {
            ID++;
        }
        String tag = TAG + '_' + ID;
        args.putString(BUNDLE_KEY_TAG, tag);
        Log.d(TAG, "Creating dialog with tag: " + tag);

        ret.setArguments(args);

        ret.show(fm, tag);
        return ret;
    }

    private int mWhichButtonClicked;
    private ResultReceiver callback;

    @NonNull
    @SuppressLint("NewApi")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Log.d(TAG, "onCreateDialog, savedInstanceState=" + savedInstanceState);

        mWhichButtonClicked = DialogInterface.BUTTON_NEGATIVE;

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        Bundle args = getArguments();
        if (args != null) {
            String title = args.getString(BUNDLE_KEY_TITLE);
            if (!Utils.isNullOrEmpty(title)) {
                builder.setTitle(title);
            }

            String message = args.getString(BUNDLE_KEY_MSG);
            if (!Utils.isNullOrEmpty(message)) {
                builder.setMessage(Html.fromHtml(message));
            }



            int type = args.getInt(BUNDLE_KEY_TYPE);
            if (type > 0) {
                // ok|yes
                builder.setPositiveButton(
                        type == TYPE_QUESTION_YES_NO ? android.R.string.yes : android.R.string.ok, this
                );

                if (type == TYPE_QUESTION_YES_NO || type == TYPE_OK_CANCEL || type == TYPE_INPUT) {
                    builder.setNegativeButton(type == TYPE_QUESTION_YES_NO ?
                            android.R.string.no : android.R.string.cancel, this
                    );
                }

                if (type == TYPE_WARNING) {
//                    if (AndroidUtils.isApi11plus()) {
                        builder.setIconAttribute(android.R.attr.alertDialogIcon);
//                    }
                }
            }

            callback = args.getParcelable(BUNDLE_KEY_CALLBACK);

        } else {
            Log.w(TAG, "Args are empty!");
        }

        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        onClick(dialog, mWhichButtonClicked);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        mWhichButtonClicked = which;
        if (callback != null) {
            switch (mWhichButtonClicked) {
                case DialogInterface.BUTTON_POSITIVE:
                    callback.send(DialogInterface.BUTTON_POSITIVE, null);
                    break;
                case DialogInterface.BUTTON_NEUTRAL:
                    callback.send(DialogInterface.BUTTON_NEUTRAL, null);
                    break;
                default:
                    callback.send(mWhichButtonClicked, null);
                    break;
            }
        }
    }

    public String getDialogTag() {
        return getArguments().getString(BUNDLE_KEY_TAG);
    }

    public void setCallback(ResultReceiver callback) {
        this.callback = callback;
        getArguments().putParcelable(BUNDLE_KEY_CALLBACK, callback);
    }

}
