package com.globallogic.koszalin.shared.view;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;

/**
 * AbstractV4Fragment
 * <hr /> Created by damian.giedrys on 2016-05-16.
 */
public class AbstractV4Fragment extends Fragment {


//    @Deprecated
//    protected Button initBtn(@NonNull View.OnClickListener clickListener, @NonNull View rootView,
//                             @IdRes int btnId) {
//
//        Button btn = (Button) rootView.findViewById(btnId);
//        btn.setOnClickListener(clickListener);
//        return btn;
//    }

    protected <T extends View> T initView(@NonNull View rootView, @IdRes int btnId) {
        return initView(rootView, btnId, (View.OnClickListener)null);
    }

    protected <T extends View> T initView(@NonNull View rootView, @IdRes int btnId, @NonNull View.OnClickListener clickListener) {
        T btn = (T) rootView.findViewById(btnId);
        if (clickListener != null) {
            btn.setOnClickListener(clickListener);
        }
        return btn;
    }


    protected <T extends AdapterView> T initView(@NonNull View rootView, @IdRes int btnId, @NonNull AdapterView.OnItemClickListener clickListener) {

        T btn = (T) rootView.findViewById(btnId);
        if (clickListener != null) {
            btn.setOnItemClickListener(clickListener);
        }
        return btn;
    }

}
