package com.globallogic.koszalin.shared.logic;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * HasDrawerTitle
 * <hr /> Created by damian.giedrys on 2016-04-28.
 */
public interface HasDrawerTitle {

    String getDrawerTitle(@NonNull Context ctx);

}
