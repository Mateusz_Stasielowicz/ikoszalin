package com.globallogic.koszalin.shared.view;

import android.content.Context;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

import java.util.List;


public abstract class AbstractRecyclerViewAdapter2<S> extends RecyclerView.Adapter implements View.OnClickListener {

    protected List<S> mDataset;
    protected Context mContext;
    protected AdapterView.OnItemClickListener mOnClickListener;


    public AbstractRecyclerViewAdapter2(List<S> mDataset, Context mContext) {
        this.mDataset = mDataset;
        this.mContext = mContext;
    }

    public AbstractRecyclerViewAdapter2(List<S> mDataset, Context mContext, AdapterView.OnItemClickListener onClickListener) {
        this.mDataset = mDataset;
        this.mContext = mContext;
        this.mOnClickListener = onClickListener;
    }


    @Override
    public void onClick(View v) {
        RecyclerView.ViewHolder holder = (RecyclerView.ViewHolder) v.getTag();
        int position = holder.getPosition();
        if (mOnClickListener != null) {
            mOnClickListener.onItemClick(null, v, position, position);
        }
    }

    public S getItem(int position) {
        if (mDataset.size() != 0)
            return mDataset.get(position);
        return null;
    }


    public static class T extends RecyclerView.ViewHolder {
        public T(View v) {
            super(v);
        }
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onClickListener) {
        this.mOnClickListener = onClickListener;
    }

//    public void addAll(List<S> items) {
//        mDataset.beginBatchedUpdates();
//        for (S item : items) {
//            mDataset.add(item);
//        }
//        mDataset.endBatchedUpdates();
//    }

}
