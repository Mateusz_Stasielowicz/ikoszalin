package com.globallogic.koszalin.shared.logic;

/**
 * BoolCallback
 * <hr /> Created by damian.giedrys on 2016-05-18.
 */
public interface BoolCallback {

    void run(boolean value);

}
