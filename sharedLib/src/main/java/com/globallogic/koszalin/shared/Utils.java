package com.globallogic.koszalin.shared;

import android.content.Context;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * com.globallogic.ikoszalin.shared.Utils
 * <hr /> Created by damian.giedrys on 2016-05-12.
 */
public class Utils {

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean isNullOrEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

    public static String getAppPath(Context ctx) {
//        PackageManager m = ctx.getPackageManager();
//        String s = ctx.getPackageName();
//        PackageInfo p = null;
//        try {
//            p = m.getPackageInfo(s, 0);
//            s = p.applicationInfo.dataDir;
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }

        // /data/data/com.globallogic.ikoszalin.debug
        String ret = ctx.getApplicationInfo().dataDir;
        if (!ret.endsWith("/")) {
            ret += '/';
        }
        return ret;
    }

    public static double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        if (value == Double.POSITIVE_INFINITY || value == Double.NEGATIVE_INFINITY)
            return value;

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
