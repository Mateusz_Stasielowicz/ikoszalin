package com.globallogic.koszalin.shared.view;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.globallogic.koszalin.shared.AndroidUtils;

/**
 * ViewPagerBubbleIndicatorListener
 * <hr /> Created by damian.giedrys on 2016-05-31.
 */
public class ViewPagerBubbleIndicatorListener implements ViewPager.OnPageChangeListener {

    private LinearLayout imagesPagerIndicator;
    private ImageView[] dots;


    private Context ctx;
    private final @DrawableRes int idSelected;
    private final @DrawableRes int idNonSelected;
    private final int count;

    public ViewPagerBubbleIndicatorListener (Context ctx, LinearLayout indicator, @DrawableRes int idSelected,
                                             @DrawableRes int idNonSelected, int count) {
        this.ctx = ctx;
        this.imagesPagerIndicator = indicator;
        this.idSelected = idSelected;
        this.idNonSelected = idNonSelected;
        this.count = count;
        setUiPageViewController(count);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (count > 1) {
            for (int i = 0; i < count; i++) {
                dots[i].setImageDrawable(AndroidUtils.getDrawable(ctx, idNonSelected));
            }
            dots[position].setImageDrawable(AndroidUtils.getDrawable(ctx, idSelected));

//        if (position + 1 == dotsCount) {
//            btnNext.setVisibility(View.GONE);
//            btnFinish.setVisibility(View.VISIBLE);
//        } else {
//            btnNext.setVisibility(View.VISIBLE);
//            btnFinish.setVisibility(View.GONE);
//        }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void setUiPageViewController(int count) {

        if (count > 1) {
            dots = new ImageView[count];

            for (int i = 0; i < count; i++) {
                dots[i] = new ImageView(ctx);
                dots[i].setImageDrawable(AndroidUtils.getDrawable(ctx, idNonSelected));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                params.setMargins(4, 0, 4, 0);

                imagesPagerIndicator.addView(dots[i], params);
            }

            dots[0].setImageDrawable(AndroidUtils.getDrawable(ctx, idSelected));
        }
    }
}
