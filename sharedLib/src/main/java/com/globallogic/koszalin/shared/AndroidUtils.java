package com.globallogic.koszalin.shared;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

/**
 * AndroidUtils
 * <hr /> Created by damian.giedrys on 2016-05-13.
 */
public class AndroidUtils {

    public static final String GOOGLE_MAPS_PACKAGE_NAME = "com.google.android.apps.maps";
    public static final String GOOGLE_PLAY_ID_OLD = "com.google.market";
    public static final String GOOGLE_PLAY_ID_NEW = "com.android.vending";

    /**
     * API_21 - Android 5.0 - LOLLIPOP
     *
     * @return 'true' gdy spełnia api level
     */
    public static boolean isApi21plus() {
        return Build.VERSION.SDK_INT >= 21; // LOLLIPOP
    }

    /**
     * API_23 - Android 6.0 - MARSHMALLOW
     *
     * @return 'true' gdy spełnia api level
     */
    public static boolean isApi23plus() {
        return Build.VERSION.SDK_INT >= 23; // MARSHMALLOW
    }


    /**
     * Wyswietla okno by wybrac apke do wysłania maila (nie wysyla sam!)
     *
     * @param act               Activity
     * @param intentChooseTitle IntentChoose title, np. intent_send_email_via
     * @param email             e-mail
     * @param subject           Subject
     * @param text              Text
     * @return True jeżeli odnalazł jakąś apke co może obsłużyć akcję
     */
    public static boolean sendEmail(@NonNull Activity act, String intentChooseTitle,
                                    @NonNull String email, @Nullable String subject,
                                    @Nullable String text) {

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject != null ? subject : ""); // subject
        emailIntent.putExtra(Intent.EXTRA_TEXT, text != null ? text : ""); // body

        if (emailIntent.resolveActivity(act.getPackageManager()) != null) {
            act.startActivity(Intent.createChooser(emailIntent, intentChooseTitle));
            return true;
        } else {
            return false;
        }
    }

    @SuppressWarnings("deprecation")
    public static Drawable getDrawable(@NonNull Context ctx, @DrawableRes int id) {
        if (isApi21plus()) {
            return ctx.getResources().getDrawable(id, null);
        } else {
            return ctx.getResources().getDrawable(id);
        }
    }

    public static Drawable getVectorDrawable(@NonNull Context ctx, @DrawableRes int id) {
        Drawable icon;
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            icon = VectorDrawableCompat.create(ctx.getResources(), id, ctx.getTheme());
        } else {
            icon = ctx.getResources().getDrawable(id, ctx.getTheme());
        }
        Log.d("DEBUG", "getVectorDrawable: " + icon);
        return icon;
    }

    public static boolean openUriInIntent(@NonNull Activity act, @NonNull String uri) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        if (browserIntent.resolveActivity(act.getPackageManager()) != null) {
            act.startActivity(browserIntent);
            return true;
        } else {
            return false;
        }
    }

    public static boolean isPackageInstalled(@NonNull Context context, @NonNull String packagename) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean openApplicationPageInGooglePlay(@NonNull Activity activity, @NonNull String appId) {
        if (isPackageInstalled(activity.getBaseContext(), GOOGLE_PLAY_ID_NEW) ||
                isPackageInstalled(activity.getBaseContext(), GOOGLE_PLAY_ID_OLD)) {

            // openApplicationPageInGooglePlayByGPApp
            return openUriInIntent(activity, "market://details?id=" + appId);
        } else {
            // openApplicationPageInGooglePlayByBrowser
            return openUriInIntent(activity, "https://play.google.com/store/apps/details?id=" + appId);
        }
    }

    public static boolean checkSelfPermission(@NonNull Context ctx, @NonNull String permission) {
        return ActivityCompat.checkSelfPermission(ctx, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static Bitmap drawableToBitmap(@NonNull Drawable drawable) {
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    //ACCESS_NETWORK_STATE.
//    private boolean isNetworkAvailable(Context ctx) {
//        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
//        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//    }
}
