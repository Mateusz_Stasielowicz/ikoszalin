package com.globallogic.koszalin.shared;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

/**
 * SharedPreferenceUtils
 * <hr /> Created by damian.giedrys on 2016-05-19.
 */
public class SharedPreferenceUtils {

    private SharedPreferenceUtils() {
    }

    public static void setLong(@NonNull Context ctx, @NonNull String key, long value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(
                ctx.getApplicationContext());
        SharedPreferences.Editor edit = prefs.edit();
        edit.putLong(key, value);
        edit.apply();
    }

    public static long getLong(@NonNull Context ctx, @NonNull String key) {
        return getLong(ctx, key, 0L);
    }

    public static long getLong(@NonNull Context ctx, @NonNull String key, long defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(
                ctx.getApplicationContext()).getLong(key, defaultValue);
    }

}
