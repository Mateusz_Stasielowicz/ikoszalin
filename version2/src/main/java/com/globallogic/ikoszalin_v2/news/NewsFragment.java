package com.globallogic.ikoszalin_v2.news;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.globallogic.ikoszalin_v2.MainActivity;
import com.globallogic.ikoszalin_v2.R;


/**
 * Created by mateusz.stasielowicz on 2016-04-28.
 */
public class NewsFragment extends Fragment {


    private TabLayout tabLayout;
    private ViewPager viewPager;
    private NewsPageAdapter newsPageAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ((MainActivity)getActivity()).setTitle("Aktualności");


        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        newsPageAdapter = new NewsPageAdapter(getChildFragmentManager(), getContext());
        viewPager.setAdapter(newsPageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
//                selectedNavOption = tab.getPosition();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
