package com.globallogic.ikoszalin_v2.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by mateusz.stasielowicz on 2016-05-10.
 */
public class TextViewShadowed extends TextView {


    public TextViewShadowed(Context context) {
        super(context);
    }

    public TextViewShadowed(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextViewShadowed(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void draw(Canvas canvas) {
        for (int i = 0; i < 5; i++) {
            super.draw(canvas);
        }
    }
}
