package com.globallogic.ikoszalin_v2.view;

import android.support.v7.util.SortedList;

import java.io.Serializable;

/**
 * Created by mateusz.stasielowicz on 2016-04-28.
 */
public class CustomSortedList<T> extends SortedList<T> implements Serializable {


    public CustomSortedList(Class<T> klass, Callback<T> callback) {
        super(klass, callback);
    }

    public CustomSortedList(Class<T> klass) {

        super(klass, new Callback<T>() {
            @Override
            public int compare(T o1, T o2) {

                return 0;
            }

            @Override
            public void onInserted(int position, int count) {
            }

            @Override
            public void onRemoved(int position, int count) {
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
            }

            @Override
            public void onChanged(int position, int count) {
            }


            @Override
            public boolean areContentsTheSame(T oldItem, T newItem) {
                return false;
            }

            @Override
            public boolean areItemsTheSame(T item1, T item2) {
                return false;
            }
        });
    }

    public CustomSortedList(Class<T> klass, Callback<T> callback, int initialCapacity) {
        super(klass, callback, initialCapacity);
    }


//    public void addAll(Collection<T> items) {
//        if (items != null) {
//            this.beginBatchedUpdates();
//            for (T item : items) {
//                this.add(item);
//            }
//            this.endBatchedUpdates();
//        }
//    }

//    public void addAll(SortedList<T> items) {
//        if (items != null) {
//            this.beginBatchedUpdates();
//            for (int i = 0; i < items.size(); i++) {
//                this.add(items.get(i));
//            }
//            this.endBatchedUpdates();
//        }
//    }

}


