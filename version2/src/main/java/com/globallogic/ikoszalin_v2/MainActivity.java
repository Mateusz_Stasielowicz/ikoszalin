package com.globallogic.ikoszalin_v2;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


import com.android.volley.VolleyError;
import com.globallogic.googlesheetlib.query.Query;
import com.globallogic.googlesheetlib.query.request.ResponseListener;
import com.globallogic.googlesheetlib.query.response.SheetResponse;
import com.globallogic.ikoszalin_v2.mod.Things;
import com.globallogic.ikoszalin_v2.news.NewsContentFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String FRAGMENT_NEWS = "fragment_news";
    public static final String FRAGMENT_EVENTS = "fragment_events";
    public static final String FRAGMENT_MAIN = "fragment_main";
    public static final String FRAGMENT_PLACES = "fragment_places";
    public static final String FRAGMENT_PLACES_CONTENT = "fragment_places_content";

    private static final String SCRIPT_ID = "AKfycbzZv4eNdXWJuruu99p7OUPmC7n6-0WOtCoa0CmpuMQ";

    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.toolbar_transparent)));
//        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#550000ff")));


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
//        Utils.tintAllIcons(menu, ContextCompat.getColor(this,R.color.text_primary_inverse));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        selectFragment(id);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void selectFragment(int id){
        if(id == 0){
            replaceFragment(new NewsContentFragment(), FRAGMENT_NEWS);
            navigationView.setCheckedItem(R.id.nav_news);
        }else {
            navigationView.setCheckedItem(id);
            if (id == R.id.nav_news) {
                replaceFragment(new NewsContentFragment(), FRAGMENT_NEWS);
            }else if(id == R.id.nav_gallery){
                Query query = new Query(getApplicationContext(), SCRIPT_ID, "12345", null);
                Log.i("TEST","url to image "+query.getImageUrl("0B6eZECy7dDpXdm5mdllUYWUxNzQ",220));
//                query.getData(Things.Response.class, 0, new ResponseListener() {
//                    @Override
//                    public void onSuccess(SheetResponse response) {
//                        Log.i("TEST","thing  = "+response.toString());
//                    }
//
//                    @Override
//                    public void onError(VolleyError error) {
//                        Log.i("TEST","VolleyError = "+error.toString());
//                    }
//                });
            }
        }
        String a = "a";
    }

    public void replaceFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment newFragment = fragmentManager.findFragmentByTag(tag);
        if (newFragment == null) {
            newFragment = fragment;
        }
        fragmentTransaction.replace(R.id.main_layout, newFragment, tag);
        fragmentTransaction.commit();

        // after


    }
}
