package com.globallogic.ikoszalin_v2.mod;

import com.globallogic.googlesheetlib.query.response.GoogleSheetTable;
import com.globallogic.googlesheetlib.query.response.SheetResponse;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by mateusz.stasielowicz on 2016-05-16.
 */
public class Things {

    public int id;
    public String category;
    public String name;
    public int type;
    public Address address;
    public String telephone;
    public String telephone2;
    public String mail;
    public String descriptionShort;
    public String descriptionLong;
    public String www;
    public boolean indoor;
    @SerializedName("datetime_from")
    public Date datetimeFrom;
    @SerializedName("datetime_to")
    public Date datetimeTo;
    @SerializedName("opening_hours")
    public OpeningHours openingHours;


    @Override
    public String toString() {
        return "Things{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", address=" + address +
                ", telephone='" + telephone + '\'' +
                ", telephone2='" + telephone2 + '\'' +
                ", mail='" + mail + '\'' +
                ", descriptionShort='" + descriptionShort + '\'' +
                ", descriptionLong='" + descriptionLong + '\'' +
                ", www='" + www + '\'' +
                ", indoor=" + indoor +
                ", datetimeFrom=" + datetimeFrom +
                ", datetimeTo=" + datetimeTo +
                ", openingHours=" + openingHours +
                '}';
    }


    public class Address{
        public String localization;
        public String street;
        public String number;
        public String postCode;

        @Override
        public String toString() {
            return "Address{" +
                    "localization='" + localization + '\'' +
                    ", street='" + street + '\'' +
                    ", number='" + number + '\'' +
                    ", postCode='" + postCode + '\'' +
                    '}';
        }
    }
    public class OpeningHours{
        public String monday;
        public String teusday;
        public String wednesday;
        public String thursday;
        public String friday;
        public String saturday;
        public String sunday;

        @Override
        public String toString() {
            return "OpeningHours{" +
                    "monday='" + monday + '\'' +
                    ", teusday='" + teusday + '\'' +
                    ", wednesday='" + wednesday + '\'' +
                    ", thursday='" + thursday + '\'' +
                    ", friday='" + friday + '\'' +
                    ", saturday='" + saturday + '\'' +
                    ", sunday='" + sunday + '\'' +
                    '}';
        }
    }

    public class Response extends SheetResponse<Things>{

    }

}
