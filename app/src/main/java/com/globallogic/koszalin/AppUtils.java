package com.globallogic.koszalin;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Debug;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.widget.ImageView;

import com.globallogic.koszalin.content.data.ContentItem;
import com.globallogic.koszalin.content.data.ContentListItem;
import com.globallogic.koszalin.shared.AndroidUtils;
import com.globallogic.koszalin.shared.Utils;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Random;

/**
 * com.globallogic.ikoszalin.shared.AppUtils
 * <hr /> Created by damian.giedrys on 2016-05-04.
 */
public final class AppUtils {

    private static final String TAG = AppUtils.class.getSimpleName();

    public static boolean DEBUG = Debug.isDebuggerConnected() || BuildConfig.DEBUG;

    public static int[] COLORS_FOR_PROGRESSBAR = new int[]{R.color.koszalin_blue, R.color.koszalin_green, R.color.koszalin_red};

    private AppUtils() {
    }

    //    @SuppressWarnings("deprecation")
//    public static Drawable getDrawable(@NonNull Context ctx, @DrawableRes int id) {
//        if (Build.VERSION.SDK_INT >= 21) { // > 5.0 LOLLIPOP
//            return ctx.getResources().getDrawable(id, null);
//        } else {
//            return ctx.getResources().getDrawable(id);
//        }
//    }

//    // tmp
//    public static
//    @DrawableRes
//    int DEV_getImageresStub() {
//        Random r = new Random();
//        int rr;
//        switch (r.nextInt(4)) {
//            case 0:
//                rr = R.drawable.tmp1;
//                break;
//            case 1:
//                rr = R.drawable.tmp2;
//                break;
//            case 2:
//                rr = R.drawable.tmp3;
//                break;
//            case 3:
//                rr = R.drawable.tmp4;
//                break;
//            default:
//                rr = R.drawable.ic_menu_gallery;
//                break;
//        }
//        return rr;
//    }

    public static String[] DEV_getImageUrlStubM() {
        return new String[]{DEV_getImageUrlStub(), DEV_getImageUrlStub()};
    }

    public static String DEV_getImageUrlStub() {
        Random r = new Random();
        String rr;
        switch (r.nextInt(6)) {
            case 0:
                rr = "http://www.koszalin.pl/sites/default/files/styles/media_gallery_large/public/gallery_picture.2011-04-13.02006292_big.jpeg";
                break;
            case 1:
                rr = "http://www.koszalin.pl/sites/default/files/styles/media_gallery_large/public/aquapark-koszalin.jpg";
                break;
            case 2:
                rr = "http://www.koszalin.pl/sites/default/files/styles/media_gallery_large/public/gallery_picture.2011-04-13.01566705_big.jpeg";
                break;
            case 3:
                rr = "http://www.koszalin.pl/sites/default/files/styles/media_gallery_large/public/gallery_picture.2011-04-13.02013484_big.jpeg";
                break;
            case 4:
                rr = "http://www.koszalin.pl/sites/default/files/styles/media_gallery_large/public/gallery_picture.2011-04-15.1919_big.jpeg";
                break;
            case 5:
                rr = "http://www.koszalin.pl/sites/default/files/styles/media_gallery_large/public/gallery_picture.2011-04-15.3815_big.jpeg";
                break;
            default:
                rr = "http://www.koszalin.pl/sites/default/files/styles/media_gallery_large/public/gallery_picture.2011-04-15.1342_big.jpeg";
                break;
        }
        return rr;
    }

    //tmp
    public static double[] DEV_getLocationStub() {
        Random r = new Random();
        double lat = 54.195102;
        double lon = 16.195999;
        switch (r.nextInt(6)) {
            case 0:
                lat = 54.195102;
                lon = 16.195999;
                break;
            case 1:
                lat = 54.188932;
                lon = 16.191302;
                break;
            case 2:
                lat = 54.191617;
                lon = 16.179184;
                break;
            case 3:
                lat = 54.197159;
                lon = 16.183198;
                break;
            case 4:
                lat = 54.178927;
                lon = 16.196145;
                break;
            case 5:
                lat = 54.190945;
                lon = 16.211272;
                break;
        }
        return new double[]{lat, lon};
    }


    public static void loadDefaultImage(final @NonNull Context ctx, final @NonNull ImageView view) {
        // logo_koszalin_filled
        Picasso.with(ctx).load(R.drawable.logo_koszalin_tr_100alpha).into(view);
    }

    public static void loadEmptyImage(final @NonNull Context ctx, final @NonNull ImageView view) {
        // logo_koszalin_filled
        Picasso.with(ctx).load(R.drawable.empty_pixel).into(view);
    }

    public static void loadImage(final @NonNull Context ctx, final String url, final @NonNull ImageView view) {
        loadImage(ctx, url, view, -1);
    }

    public static void loadImage(final @NonNull Context ctx, final String url, final @NonNull ImageView view, @DrawableRes int drawableIfEmpty) {
        if (ctx == null || view == null) {
            return;
        }
        if (Utils.isNullOrEmpty(url)) {
            if (drawableIfEmpty > 0) {
                Picasso.with(ctx).load(drawableIfEmpty).into(view);
            }
            return;
        }

        //http://stackoverflow.com/questions/23978828/how-do-i-use-disk-caching-in-picasso
        // TODO cache

        // background2
        Picasso.with(ctx).
                load(url).
                networkPolicy(NetworkPolicy.OFFLINE).
                placeholder(R.drawable.logo_koszalin_tr_100alpha).
                error(R.drawable.logo_koszalin_tr_100alpha).
                into(view, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.v(TAG + "_Picasso", "onSuccess");
                    }

                    @Override
                    public void onError() {
                        Log.d(TAG + "_Picasso", "Could not fetch image (probably empty cache)");

                        Picasso.with(ctx).
                                load(url).
                                placeholder(R.drawable.logo_koszalin_tr_100alpha).
                                error(R.drawable.logo_koszalin_tr_100alpha).
                                into(view, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
                                        Log.d(TAG + "_Picasso", "Could not fetch image");
                                    }
                                });
                    }
                });
    }

    public static MarkerOptions asMarker(ContentItem item) {
        MarkerOptions ret = null;
        if (item != null && item.hasLocalisation()) {
//            Log.i("PlaceItem", item.getLatitude() + ", " + item.getLongitude());
            ret = new MarkerOptions()
                    .title(item.getName());
            ret.position(new LatLng(item.getAddress().localization.lat, item.getAddress().localization.lng));
        }
        return ret;
    }

    public static MarkerOptions asMarker(ContentListItem item) {
        MarkerOptions ret = null;
        if (item != null && item.lat != Double.MIN_VALUE && item.lng != Double.MIN_VALUE) {
//            Log.i("PlaceItem", item.getLatitude() + ", " + item.getLongitude());
            ret = new MarkerOptions()
                    .title(item.name);
            ret.position(new LatLng(item.lat, item.lng));
        }
        return ret;
    }

    public static final boolean isValidLocation(double[] latLng) {
        return latLng != null && latLng.length == 2 && isValidLocation(latLng[0], latLng[1]);
    }

    public static final boolean isValidLocation(double lat, double lng) {
        return lat != Double.MIN_VALUE && lng != Double.MIN_VALUE;
    }

    public static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);


//        Log.i("TEST", "1{" + lat1 + ", " + lon1 + "}, 2{" + lat2 + ", " + lon2 + "}");
        // 552.38 m , 0.005143993801614441
//        Log.i("TEST", "=" + dist);
        dist = dist * 60 * 1.1515;
//        if (unit == 'K') {
        dist = dist * 1.609344;
//        } else if (unit == 'N') {
//            dist = dist * 0.8684;
//        }
        return Utils.round(dist, 3); // do 1m
    }

    /**
     * converts decimal degrees to radians
     *
     * @param deg
     * @return
     */
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /**
     * converts radians to decimal degrees
     *
     * @param rad
     * @return
     */
    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public static double normalise(int min, int max, int value) {
        if (value >= max)
            return max;
        if (value <= min)
            return min;
        return value / (max - min);
    }

    public static double normalise(double min, double max, double value) {
        if (value >= max)
            return 1.0;
        if (value <= min)
            return 0.0;

        return (value - min) / (max - min);
    }

    public static BitmapDescriptor createBitmapDescriptor(@NonNull Context ctx, @DrawableRes int drRes) {

        try {
            // VectorDrawableCompat dr = VectorDrawableCompat.create(ctx.getResources(), drRes, null);
            Drawable dr = ResourcesCompat.getDrawable(ctx.getResources(), drRes, null);
            if (dr != null) {
                Bitmap b = AndroidUtils.drawableToBitmap(dr);
                if (b != null) {
                    return BitmapDescriptorFactory.fromBitmap(b);
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

}
