package com.globallogic.koszalin.about;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.globallogic.koszalin.R;
import com.globallogic.koszalin.base.AbstractActivity;
import com.globallogic.koszalin.shared.AndroidUtils;

/**
 * AboutActivity
 * <hr /> Created by damian.giedrys on 2016-05-13.
 */
public class AboutActivity extends AbstractActivity {

    private static final String TAG = AboutActivity.class.getName();

    private static final String PREF_ABOUT_APP_NAME = "pref_about_app_name";
    private static final String PREF_ABOUT_VERSION = "pref_about_version";
    private static final String PREF_ABOUT_FEEDBACK = "pref_about_feedback";
    private static final String PREF_ABOUT_REPORT_BUG = "pref_about_report_bug";
    private static final String PREF_ABOUT_SPONSOR_GL = "pref_about_sponsor_gl";

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
//    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener =
//            new Preference.OnPreferenceChangeListener() {
//
//                @Override
//                public boolean onPreferenceChange(Preference preference, Object value) {
//                    String stringValue = value != null ? value.toString() : "";
//
//                    if (preference instanceof ListPreference) {
//                        // For list preferences, look up the correct display value in
//                        // the preference's 'entries' list.
//                        ListPreference listPreference = (ListPreference) preference;
//                        int index = listPreference.findIndexOfValue(stringValue);
//
//                        // Set the summary to reflect the new value.
//                        preference.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);
//
//                    }
//                    // nieużywane tutaj
////            else if (preference instanceof RingtonePreference) {
////                // For ringtone preferences, look up the correct display value
////                // using RingtoneManager.
////                if (TextUtils.isEmpty(stringValue)) {
////                    // Empty values correspond to 'silent' (no ringtone).
////                    preference.setSummary(R.string.pref_ringtone_silent);
////                } else {
////                    Ringtone ringtone = RingtoneManager.getRingtone(
////                            preference.getContext(), Uri.parse(stringValue));
////                    if (ringtone == null) {
////                        // Clear the summary if there was a lookup error.
////                        preference.setSummary(null);
////                    } else {
////                        // Set the summary to reflect the new ringtone display
////                        // name.
////                        String name = ringtone.getTitle(preference.getContext());
////                        preference.setSummary(name);
////                    }
////                }
////            }
//                    else {
//                        // For all other preferences, set the summary to the value's
//                        // simple string representation.
//                        preference.setSummary(stringValue);
//                    }
//                    return true;
//                }
//            };

//    /**
//     * Binds a preference's summary to its value. More specifically, when the
//     * preference's value is changed, its summary (line of text below the
//     * preference title) is updated to reflect the value. The summary is also
//     * immediately updated upon calling this method. The exact display format is
//     * dependent on the type of preference.
//     *
//     * @see #sBindPreferenceSummaryToValueListener
//     */
//    private static void bindPreferenceSummaryToValue(Preference preference) {
//        // Set the listener to watch for value changes.
//        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);
//
//        // Trigger the listener immediately with the preference's
//        // current value.
//        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
//                PreferenceManager
//                        .getDefaultSharedPreferences(preference.getContext())
//                        .getString(preference.getKey(), ""));
//    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate, savedInstanceState: " + savedInstanceState);
        super.onCreate(savedInstanceState);


        // default values
        // PreferenceManager.setDefaultValues(this, R.xml.advanced_preferences, false);

        setContentView(R.layout.activity_about);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setDisplayShowHomeEnabled(true);

        initToolbar(R.id.toolbar, true);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.about_content_frame, new AboutFragment())
                .commit();


    }


    //    public static class AboutFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener {
    public static class AboutFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceClickListener {

        private static final String TAG2 = TAG + "_AboutFragment";

        private String _appNameCache;
        private String _appVersionCache;


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Log.d(TAG2, "onCreatePreferences, savedInstanceState:" + savedInstanceState);

        }

        @Override
        public void onCreatePreferences(Bundle bundle, String s) {
            Log.d(TAG2, "onCreatePreferences, bundle:" + bundle + ", s:" + s);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.pref_about);

            try {
                Activity ac = getActivity();
                PackageInfo pi = ac.getPackageManager().getPackageInfo(ac.getPackageName(), 0);
                _appNameCache = ac.getString(pi.applicationInfo.labelRes);
                _appVersionCache = pi.versionName + '#' + pi.versionCode;

                findPreference(PREF_ABOUT_APP_NAME).setTitle(_appNameCache);

                Preference pref = findPreference(PREF_ABOUT_VERSION);
                pref.setSummary(_appVersionCache);
                initPref(pref, R.drawable.ic_info_outline_black_24dp);

            } catch (PackageManager.NameNotFoundException e) {
                Log.e(TAG2, e.getMessage(), e);
            }

            initPref(PREF_ABOUT_FEEDBACK, R.drawable.ic_mail);
//            initPref(PREF_ABOUT_REPORT_BUG, R.drawable.ic_mail);
            initPref(PREF_ABOUT_SPONSOR_GL, R.drawable.ic_language_24dp);
        }

        @Override
        public boolean onPreferenceClick(Preference preference) {
            if (preference != null) {
                switch (preference.getKey()) {
                    case PREF_ABOUT_FEEDBACK:
                        if (!AndroidUtils.sendEmail(getActivity(),
                                getString(R.string.intent_send_email_via),
                                getString(R.string.support_email),
                                _appNameCache + " " + _appVersionCache + " Feedback", "")) {
                            Toast.makeText(getContext(), R.string.warn_app_for_intent_not_found, Toast.LENGTH_LONG).show();
                        }
                        return true;

                    case PREF_ABOUT_SPONSOR_GL:
                        if (!AndroidUtils.openUriInIntent(getActivity(), getString(R.string.sponsor_gl_url))) {
                            Toast.makeText(getContext(), R.string.warn_app_for_intent_not_found, Toast.LENGTH_LONG).show();
                        }
                        return true;
                }
            }

            return false;
        }

        private void initPref(String key, @DrawableRes int resId) {
            Preference pref = findPreference(key);
            initPref(pref, resId);
        }

        //TODO Błąd w appcompat-v7 23.3.0, nie można stosować drawable z vektorów bezpośrednio - obejście
        // spr. w póżniejszych wersjac czy dalej to samo
        private void initPref(Preference pref, @DrawableRes int resId) {
            pref.setOnPreferenceClickListener(this);
            if (resId > 0) {
//                VectorDrawableCompat dr = VectorDrawableCompat.create(getResources(), resId, null);
//                if (dr != null && iIconsTint != Integer.MIN_VALUE) {
//                    dr.setTint(iIconsTint);
//                }
//                pref.setIcon(dr);
            }
        }

    }
}
