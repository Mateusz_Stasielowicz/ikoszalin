package com.globallogic.koszalin.about;

import android.content.Context;
import android.support.v7.preference.Preference;
import android.util.AttributeSet;

import com.globallogic.koszalin.R;

/**
 * PreferenceLogo
 * <hr /> Created by damian.giedrys on 2016-05-30.
 */
public class PreferenceLogo extends Preference {

    public PreferenceLogo(Context context) {
        super(context);
        init();
    }

    public PreferenceLogo(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PreferenceLogo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public PreferenceLogo(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setLayoutResource(R.layout.preference_logo);
    }

}
