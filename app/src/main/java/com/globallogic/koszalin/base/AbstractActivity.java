package com.globallogic.koszalin.base;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.IdRes;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.globallogic.koszalin.R;
import com.globallogic.koszalin.shared.AndroidUtils;
import com.globallogic.koszalin.shared.Utils;
import com.globallogic.koszalin.shared.logic.BoolCallback;
import com.globallogic.koszalin.shared.view.AppCompatMsgDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * AbstractActivity
 * <hr /> Created by damian.giedrys on 2016-05-17.
 */
public abstract class AbstractActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String TAG = AbstractActivity.class.getSimpleName();

    protected Toolbar toolbar;
    protected ActionBar actionBar;

    protected void initToolbar (@NonNull @IdRes int toolbarId) {
        initToolbar(toolbarId, false);
    }

    protected void initToolbar (@NonNull @IdRes int toolbarId, boolean displayHomeAsUp) {

        toolbar = (Toolbar) findViewById(toolbarId);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
//        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.toolbar_transparent)));
//        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#550000ff")));
        if (displayHomeAsUp && actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) { // main activity
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
                overridePendingTransition(R.anim.launch_animation, R.anim.exit_animation);
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.launch_animation, R.anim.exit_animation);
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.launch_animation, R.anim.exit_animation);
    }

    @Override
    protected void onStop() {
        super.onStop();
        overridePendingTransition(R.anim.launch_animation, R.anim.exit_animation);
    }

    @Override
    public void setTitle(CharSequence title) {
        if (actionBar != null) {
            actionBar.setTitle(title);
        } else {
            super.setTitle(title);
        }
    }

    public void toast(@StringRes int strResource) {
        toast(getString(strResource));
    }

    public void toast(String str) {
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
    }

//    public abstract String getTag();

    public AppCompatMsgDialog showInfo(@StringRes int strRes) {
        return AppCompatMsgDialog.info(getSupportFragmentManager(), getString(strRes));
    }

    //<editor-fold desc="Android M Permissions requests">

    static class PermRequest {

        // private final int code;
        public final BoolCallback callback;
        public final String[] permissions;

        public PermRequest(BoolCallback callback, String[] permissions) { // int code,
//            this.code = code;
            this.callback = callback;
            this.permissions = permissions;
        }

        public boolean isMultiPerm() {
            return permissions.length > 1;
        }

    }

    private final Map<Integer, PermRequest> permissionsRequestsMap = new HashMap<>(4);


    public void runProtectedMethod(@IntRange(from = 0, to = 255) final int code,
                                   @NonNull String permission,
                                   @NonNull BoolCallback callback) {
        runProtectedMethod(code, callback, null, permission);
    }

    public void runProtectedMethod(@IntRange(from = 0, to = 255) final int code,
                                   @NonNull BoolCallback callback, @Nullable String explanations,
                                   @NonNull final String permission) {

        if (!AndroidUtils.checkSelfPermission(this, permission)) {

            permissionsRequestsMap.put(code, new PermRequest(callback, new String[]{permission}));

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {

                AppCompatMsgDialog.info(getSupportFragmentManager(), Utils.isNullOrEmpty(explanations) ?
                        "This application to run correctly requires some permissions." : explanations, new ResultReceiver(null) {

                    @Override
                    protected void onReceiveResult(int resultCode, Bundle resultData) {
                        // send request permission
                        ActivityCompat.requestPermissions(AbstractActivity.this, new String[]{permission}, code);
                    }

                });
            } else {
                // No explanation needed, we can request the permission.
                // send request permission
                ActivityCompat.requestPermissions(this, new String[]{permission}, code);
            }
        } else {
            callback.run(true);
        }
    }

    public void runProtectedMethods(@IntRange(from = 0, to = 255) final int code,
                                    @NonNull BoolCallback callback, @Nullable String explanations,
                                    @NonNull final String... permissions) {

        List<String> permissionsNeeded = new ArrayList<>();
        final List<String> permissionsList = new ArrayList<>();

        for (String perm : permissions) {
            if (maybeAddPermission(permissionsList, perm)) {
                permissionsNeeded.add(perm);
            }
        }
        if (permissionsList.size() > 0) {
            permissionsRequestsMap.put(code, new PermRequest(callback, permissions));

            if (permissionsNeeded.size() > 0) {
                String strExpl = explanations;
                for (String perm : permissionsNeeded) {
                    strExpl += perm; // TODO jakas nazwa 'user-friendly'
                    strExpl += "<br />";
                }

                AppCompatMsgDialog.info(getSupportFragmentManager(), strExpl, new ResultReceiver(null) {

                    @Override
                    protected void onReceiveResult(int resultCode, Bundle resultData) {
                        // send request permission
                        ActivityCompat.requestPermissions(AbstractActivity.this, permissions, code);
                    }

                });
            } else {
                // No explanation needed, we can request the permission.
                // send request permission
                ActivityCompat.requestPermissions(this, permissions, code);
            }
        } else {
            callback.run(true);
        }
    }

    private boolean maybeAddPermission(@NonNull List<String> permissionsList, @NonNull String permission) {
        if (!AndroidUtils.checkSelfPermission(this, permission)) {
            permissionsList.add(permission);
            // Maybe check for Rationale Option
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission))
                return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (permissionsRequestsMap.isEmpty() || !permissionsRequestsMap.containsKey(requestCode)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else {
            PermRequest request = permissionsRequestsMap.get(requestCode);
            if (request != null) {

                boolean isOk;
                if (!request.isMultiPerm()) { // SINGLE
                    isOk = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                } else { // MULTI
                    isOk = true;
                    for (int grantResult : grantResults) {
                        if (grantResult == PackageManager.PERMISSION_DENIED) {
                            isOk = false;
                            break;
                        }
                    }
                }

                if (isOk) {
                    permissionsRequestsMap.get(requestCode).callback.run(true);
                } else {
                    permissionsRequestsMap.get(requestCode).callback.run(false);

                    if (!request.isMultiPerm()) {
                        Log.i(TAG, "Access to permissions: " + permissions[0] + " DENIED");
                    } else {
                        Log.i(TAG, "Access to permission: " + Arrays.toString(permissions) + " DENIED");
                    }
                }
            } else {
                Log.w(TAG, "Couldn't find PermissionRequest for code: " + requestCode);
            }
            permissionsRequestsMap.remove(requestCode);
        }
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    //</editor-fold>
}
