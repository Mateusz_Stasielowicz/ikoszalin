package com.globallogic.koszalin.base;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;

import com.globallogic.koszalin.R;
import com.globallogic.koszalin.shared.view.AbstractV4Fragment;

/**
 * AbstractFragment
 * <hr /> Created by damian.giedrys on 2016-05-17.
 */
public class AbstractFragment extends AbstractV4Fragment {

    @Override
    public void startActivity(@NonNull Intent intent) {
        super.startActivity(intent);
        getActivity().overridePendingTransition(R.anim.launch_animation, R.anim.exit_animation);
    }

    public boolean startExtActivity(@NonNull Intent intent) {
        PackageManager pm = getActivity().getPackageManager();
        if (pm != null) {
            if (intent.resolveActivity(pm) != null) {
                startActivity(intent);
                return true;
            }
        }
        return false;
    }
}
