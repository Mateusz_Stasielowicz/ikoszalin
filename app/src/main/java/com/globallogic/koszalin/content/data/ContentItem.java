package com.globallogic.koszalin.content.data;

import com.globallogic.googlesheetlib.query.response.SheetResponse;
import com.globallogic.koszalin.shared.Utils;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

/**
 * ContentItem
 * <hr /> Created by damian.giedrys on 2016-05-19.
 */
public class ContentItem implements Serializable {

    private int id;
    private ECategory category;
    private ECategory category2;
    private String name;
    private EContentType type; // place, event
    private boolean recommended;
    private Address address;
    @SerializedName("phone")
    private String telephone;
    @SerializedName("phone2")
    private String telephone2;
    private String mail;
    @SerializedName("description_short")
    private String descriptionShort;
    @SerializedName("description_long")
    private String descriptionLong;
    @SerializedName("www_site")
    private String www;
    private boolean outdoor;
    @SerializedName("datetime_from")
    private Date datetimeFrom;
    @SerializedName("datetime_to")
    private Date datetimeTo;
    @SerializedName("opening_hours")
    private String[] openingHours;
    private String[] photo;
    private CountedData countedData;

    public ContentItem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ECategory getCategory() {
        return category;
    }

    public void setCategory(ECategory category) {
        this.category = category;
    }

    public ECategory getCategory2() {
        return category2;
    }

    public void setCategory2(ECategory category2) {
        this.category2 = category2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EContentType getType() {
        return type;
    }

    public void setType(EContentType type) {
        this.type = type;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone2() {
        return telephone2;
    }

    public void setTelephone2(String telephone2) {
        this.telephone2 = telephone2;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getDescriptionShort() {
        return descriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    public String getDescriptionLong() {
        return descriptionLong;
    }

    public void setDescriptionLong(String descriptionLong) {
        this.descriptionLong = descriptionLong;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public boolean isOutdoor() {
        return outdoor;
    }

    public void setOutdoor(boolean outdoor) {
        this.outdoor = outdoor;
    }

    public Date getDatetimeFrom() {
        return datetimeFrom;
    }

    public void setDatetimeFrom(Date datetimeFrom) {
        this.datetimeFrom = datetimeFrom;
    }

    public Date getDatetimeTo() {
        return datetimeTo;
    }

    public void setDatetimeTo(Date datetimeTo) {
        this.datetimeTo = datetimeTo;
    }

    public String[] getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String[] openingHours) {
        this.openingHours = openingHours;
    }

    public String[] getPhoto() {
        return photo;
    }

    public void setPhoto(String[] photo) {
        this.photo = photo;
    }

    public boolean isRecommended() {
        return recommended;
    }

    public void setRecommended(boolean recommended) {
        this.recommended = recommended;
    }

    public CountedData getCountedData() {
        return countedData;
    }

    public void setCountedData(CountedData countedData) {
        this.countedData = countedData;
    }

    @Override
    public String toString() {
        return "ContentItem {" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", category2='" + category2 + '\'' +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", address=" + address +
                ", telephone='" + telephone + '\'' +
                ", telephone2='" + telephone2 + '\'' +
                ", mail='" + mail + '\'' +
                ", descriptionShort='" + descriptionShort + '\'' +
                ", descriptionLong='" + descriptionLong + '\'' +
                ", www='" + www + '\'' +
                ", outdoor=" + outdoor +
                ", datetimeFrom=" + datetimeFrom +
                ", datetimeTo=" + datetimeTo +
                ", openingHours=" + Arrays.toString(openingHours) +
                ", photo=" + Arrays.toString(photo) +
                '}';
    }

    public boolean hasLocalisation() {
        return address != null && address.localization != null && address.localization.lat != 0.0 &&
                address.localization.lat != Double.MIN_VALUE && address.localization.lng != 0.0 &&
                address.localization.lng != Double.MIN_VALUE;
    }

//    public double[] getLocalisation() {
//        if (hasLocalisation()) {
//            int idx1 = address.localization.indexOf(",");
//            if (idx1 != -1) {
//                String str1 = address.localization.substring(0, idx1);
//                String str2 = address.localization.substring(idx1 + 1, address.localization.length());
//                return new double[]{Double.parseDouble(str1), Double.parseDouble(str2)};
//            }
//        }
//        return null;
//    }

    public boolean hasOpenHours() {
        switch (type) {
            case PLACE:
                return openingHours != null && openingHours.length == 7;
            case EVENT:
                return datetimeFrom != null; // && datetimeTo != null; // TODO ?
            default:
                return false;
        }
    }

    public String getAddressString() {
        String ret = "";
        if (address != null) {
            ret += address.street;
            if (address.number != null && address.number.length() > 0) {
                ret += " ";
                ret += address.number;
            }
            if (address.postCode != null && address.postCode.length() > 0) {
                ret += ", ";
                ret += address.postCode;
            }
        }
        return ret;
    }

    public String getFirstPhoto() {
        if (photo != null && photo.length > 0) {
            return Utils.isNullOrEmpty(photo[0]) ? null : photo[0];
        }
        return null;
    }

    public boolean hasMultiPhotos() {
        return photo != null && photo.length > 1;
    }

    public static class Address implements Serializable {
        //        public String localization;
        public Localization localization;
        public String street;
        public String number;
        @SerializedName("post_code")
        public String postCode;

        @Override
        public String toString() {
            return "Address{" +
                    "localization='" + localization + '\'' +
                    ", street='" + street + '\'' +
                    ", number='" + number + '\'' +
                    ", postCode='" + postCode + '\'' +
                    '}';
        }
    }


    public static class Localization implements Serializable {
        public double lat;
        public double lng;

        @Override
        public String toString() {
            return "Localization{" +
                    "lat=" + lat +
                    ", lng=" + lng +
                    '}';
        }
    }

    public static class CountedData implements Serializable {
        public double distance = Double.MIN_VALUE;

        @Override
        public String toString() {
            return "CountedData{" +
                    "distance=" + distance +
                    '}';
        }
    }

    public class Response extends SheetResponse<ContentItem> {

    }

}

