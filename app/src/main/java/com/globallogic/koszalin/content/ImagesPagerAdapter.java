package com.globallogic.koszalin.content;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * ImagesPagerAdapter
 * <hr /> Created by damian.giedrys on 2016-05-31.
 */
public class ImagesPagerAdapter extends FragmentStatePagerAdapter {

    private final String[] images;

    public ImagesPagerAdapter(FragmentManager fr, String[] images) {
        super(fr);
        this.images = images;
    }


    @Override
    public Fragment getItem(int position) {
        return ImageDetailFragment.newInstance(images[position]);
    }

    @Override
    public int getCount() {
        return images.length;
    }
}
