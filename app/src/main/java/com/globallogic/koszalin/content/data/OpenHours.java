//package com.globallogic.ikoszalin.content.data;
//
//import android.content.Context;
//import android.util.Log;
//
//import com.globallogic.ikoszalin.R;
//
//import java.io.Serializable;
//import java.util.Calendar;
//import java.util.Date;
//
///**
// * OpenHours
// * <hr /> Created by damian.giedrys on 2016-05-17.
// */
//public class OpenHours implements Serializable {
//
//    public String from;
//    public String to;
//
//    public OpenHours(String from, String to) {
//        this.from = from;
//        this.to = to;
//    }
//
//
//    public static String toHtml(Context ctx, OpenHours[] hours) {
//
//        StringBuilder sb = new StringBuilder();
//
//        String[] days = ctx.getResources().getStringArray(R.array.days);
//
//        sb.append("<table>");
//
//        Calendar calendar = Calendar.getInstance();
//        int today = calendar.get(Calendar.DAY_OF_WEEK);
//
//        for (int i=0 ; i<7 ; i++) {
//            sb.append("<tr>");
//
//            sb.append("<td>");
//            if (i == today) {
//                sb.append("<b>");
//                sb.append(days[i]);
//                sb.append("</b>");
//            } else {
//                sb.append(days[i]);
//            }
//            sb.append("</td>");
//
//
//            sb.append("<td>");
//            if (hours[i] != null) {
//                sb.append(hours[i].from + " : " + hours[i].to);
//            } else {
//                sb.append(ctx.getString(R.string.closed));
//            }
//            sb.append("</td>");
//
//            sb.append("</tr>");
//        }
//
//
//        sb.append("</table>");
//
//        Log.d("AA", sb.toString());
//
//        return sb.toString();
//    }
//
//}
