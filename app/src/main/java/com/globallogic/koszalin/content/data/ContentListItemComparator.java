package com.globallogic.koszalin.content.data;

import com.globallogic.koszalin.AppUtils;

import java.io.Serializable;
import java.util.Comparator;

/**
 * ContentListItemComparator
 * <hr /> Created by damian.giedrys on 2016-05-23.
 */
public class ContentListItemComparator implements Comparator<ContentListItem>, Serializable {

    private static final String TAG = ContentListItemComparator.class.getSimpleName();

    private static ContentListItemComparator _instance = null;

    public static ContentListItemComparator getInstance() {
        if (_instance == null) {
            _instance = new ContentListItemComparator();
        }
        return _instance;
    }

    private ContentListItemComparator() {
    }

    @Override
    public int compare(ContentListItem ci1, ContentListItem ci2) {
//        Log.d(TAG, "comparing " + ci1 + ", with " + ci2);

        if (ci1 == null && ci2 != null) {
//            Log.d(TAG, "-1");
            return -1;
        }
        if (ci1 != null && ci2 == null) {
//            Log.d(TAG, "1");
            return 1;
        }
        if ((ci1 == null && ci2 == null) || (ci1 != null && ci2 != null && ci1.equals(ci2))) {
//            Log.d(TAG, "0");
            return 0;
        }

        /*
        double rate1 = 0.0;
        double rate2 = 0.0;
        final int maxDistanceWeight = 30;
        final double maxDistance = 10.0;

        double d1 = maxDistance;
        if (ci1.distance != Double.MIN_VALUE) {
            d1 = ci1.distance;
        }
        d1 = AppUtils.normalise(0.0, maxDistance, d1);
        rate1 += calcDistWeight(d1) * maxDistanceWeight;
//        Log.d(TAG, "rate1: " + rate1);

        double d2 = maxDistance;
        if (ci2.distance != Double.MIN_VALUE) {
            d2 = ci2.distance;
        }
        d2 = AppUtils.normalise(0.0, maxDistance, d2);
        rate2 += calcDistWeight(d2) * maxDistanceWeight;
//        Log.d(TAG, "rate2: " + rate2);

        if (rate1 != 0.0 || rate2 != 0.0) {
            final int mul = 100;
            int iR = (int)(rate2 * mul - rate1 * mul);
//            Log.d(TAG, "iR: " + iR);
            return iR;
        }
        */

        // dommyślnie, po nazwie jedynie
        int i = ci1.name.compareTo(ci2.name);
        return i;
    }

    private double calcDistWeight(double dist) {
        return -Math.sqrt(1 - Math.pow(dist-1, 2)) + 1.0;
    }
}
