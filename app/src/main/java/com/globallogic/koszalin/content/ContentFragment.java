package com.globallogic.koszalin.content;

import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.globallogic.googlesheetlib.query.CustomErrorCodes;
import com.globallogic.koszalin.MainActivity;
import com.globallogic.koszalin.MyLocationManager;
import com.globallogic.koszalin.R;
import com.globallogic.koszalin.content.data.ContentListItem;
import com.globallogic.koszalin.content.data.EContentCategory;
import com.globallogic.koszalin.content.data.EContentFilterType;
import com.globallogic.koszalin.content.data.IContentFragment;
import com.globallogic.koszalin.logic.DefaultDataResponse;
import com.globallogic.koszalin.logic.i.CanChangeTabMode;
import com.globallogic.koszalin.shared.view.AbstractV4Fragment;
import com.globallogic.koszalin.shared.view.AppCompatMsgDialog;
import com.google.android.gms.maps.LocationSource;

import java.util.ArrayList;
import java.util.List;

/**
 * ContentFragment
 * <hr /> Created by damian.giedrys on 2016-05-13.
 */
public class ContentFragment extends AbstractV4Fragment implements CanChangeTabMode, LocationSource.OnLocationChangedListener { // HasDrawerTitle,

    public static final String BUNDLE_KEY_CATID = "bundle_key_catid";
    public static final String BUNDLE_KEY_ITEMS = "bundle_key_items";
    public static final String BUNDLE_NO_NET_INFO_SHOWED = "bundle_no_net_info_showed";
    public static final String BUNDLE_FILTER_TYPE = "bundle_filter_type";

    private static final String TAG = ContentFragment.class.getSimpleName();

    // ui
//    private ViewPager viewPager;
//    private SimpleFragmentPagerAdapter pageAdapter;
    private MenuItem miSwitch;
    private MenuItem miFilter;
    private MenuItem miFilterDistance;
    private MenuItem miFilterTime;
//    private ProgressBar progressBar;

    // vars
    private EContentCategory selectedCategory;
    private boolean isListView = true;
    private boolean locationIsSet = false;
    private boolean isNoInternetInfoShowed = false;
    private EContentFilterType filterType = EContentFilterType.DEFAULT;
    private ArrayList<ContentListItem> lastResponse;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            selectedCategory = EContentCategory.getById(bundle.getInt(BUNDLE_KEY_CATID));
            Log.i(TAG, "Got contentCat from bundle: " + selectedCategory);
        } else {
            Log.w(TAG, "Empty initial arguments");
        }

        if (savedInstanceState != null) {
            isNoInternetInfoShowed = savedInstanceState.getBoolean(BUNDLE_NO_NET_INFO_SHOWED);
            filterType = EContentFilterType.parse(savedInstanceState.getInt(BUNDLE_FILTER_TYPE, EContentFilterType.DEFAULT.id));
        }

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View ret = inflater.inflate(R.layout.fragment_content, container, false);
//        progressBar = initView(ret, R.id.progressBar);
        return ret;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated");

        ((MainActivity) getActivity()).clearAllTabs();

        locationIsSet = MyLocationManager.INSTANCE.getLastLocation() != null;

        switchToMode(isListView, null);

        if (!ContentDatabase.INSTANCE.isInited()) {

            ContentDatabase.INSTANCE.initDataset(new DefaultDataResponse() { // progressBar

                @Override
                public void onSuccessImpl(Object response) {
                    updateItems(false);
                }

                @Override
                public void onErrorImpl(int errorCode) {
                    AppCompatMsgDialog.warning(getFragmentManager(), getString(R.string.error_handle_message, errorCode));
                }
            });
        } else {
//            getContentData();
            updateItems(false);
        }

        updateMenuVisibility();
        MyLocationManager.INSTANCE.addLocationChangeListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(BUNDLE_NO_NET_INFO_SHOWED, isNoInternetInfoShowed);
        outState.putInt(BUNDLE_FILTER_TYPE, filterType.id);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        MyLocationManager.INSTANCE.removeLocationListener(this);
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.i(TAG, "onCreateOptionsMenu: " + menu);
        inflater.inflate(R.menu.menu_content, menu);
        miSwitch = menu.findItem(R.id.action_switch_map_list);

        miFilter = menu.findItem(R.id.action_filter);
        miFilter.setVisible(false);

        menu.findItem(R.id.action_filter_default).setVisible(false);

        miFilterDistance = menu.findItem(R.id.action_filter_distance);
        miFilterDistance.setVisible(false);

        miFilterTime = menu.findItem(R.id.action_filter_time);
        miFilterTime.setVisible(false);

        menu.findItem(R.id.action_filter_popularity).setVisible(false); // TODO disabled right now
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected: " + item);

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_switch_map_list:

                Log.d(TAG, "onOptionsItemSelected: action_switch_map_list");

                isListView = !isListView;
                switchToMode(isListView, lastResponse);

//                ContentDatabase.INSTANCE.getItemsFor(selectedCategory, false, new DefaultDataResponse<ArrayList<ContentListItem>>() {
//
//                    @Override
//                    public void onSuccessImpl(ArrayList<ContentListItem> response) {
//                        Log.d(TAG, "onOptionsItemSelected: action_switch_map_list onSuccessImpl");
//                        isListView = !isListView;
//                        switchToMode(isListView, response);
//                    }
//
//                    @Override
//                    public void onErrorImpl(int errorCode) {
//                        if (errorCode != CustomErrorCodes.NO_CONNECTION_ERROR) {
//                            AppCompatMsgDialog.warning(getFragmentManager(), getString(R.string.error_handle_message, errorCode));
//                        } else {
//                            isListView = !isListView;
//                            switchToMode(isListView, null);
//                        }
//                    }
//                });

                updateItems(false);

                return true;
            case R.id.action_filter_default:
                return inFilterChecked(item, EContentFilterType.DEFAULT);
            case R.id.action_filter_name:
                return inFilterChecked(item, EContentFilterType.BY_NAME);
            case R.id.action_filter_distance:
                return inFilterChecked(item, EContentFilterType.BY_DISTANCE);
            case R.id.action_filter_time:
                return inFilterChecked(item, EContentFilterType.BY_TIME);
            case R.id.action_filter_popularity:
                return inFilterChecked(item, EContentFilterType.BY_POPULARITY);
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean inFilterChecked(@NonNull MenuItem mi, EContentFilterType ft) {
        mi.setChecked(true);
        changeFilterType(ft);
        return true;
    }

    @Override
    public int getTabMode() {
        return TabLayout.MODE_FIXED;
    }

    @Override
    public boolean isTabVisible() {
        return false;
    }

    private void updateMenuVisibility() {
        if (miFilter != null) {
            miFilter.setVisible(isListView && selectedCategory != EContentCategory.THINGS_TODO); // list

            miFilterTime.setVisible(selectedCategory == EContentCategory.EVENTS);
        }
    }

    public void setSelectedCategory(EContentCategory selectedCategory) {
        Log.i(TAG, "setSelectedCategory: " + selectedCategory);
        if (selectedCategory != this.selectedCategory) {
            announceModelClear();
            this.selectedCategory = selectedCategory;
            updateMenuVisibility();
            updateItems(false);
        }
    }

    private void switchToMode(boolean mode, ArrayList<ContentListItem> list) { // isListView
        Log.d(TAG, "switchToMode: " + mode);

        FragmentManager fragmentManager = getChildFragmentManager();
        String tag = mode ? ContentListFragment.TAG : ContentMapFragment.TAG;
        Fragment newFragment = fragmentManager.findFragmentByTag(tag);
        if (newFragment == null) {
            if (mode) {
                newFragment = new ContentListFragment();
            } else {
                newFragment = new ContentMapFragment();
            }
        }

        Bundle bundle = new Bundle();
        if (selectedCategory != null) {
            bundle.putInt(BUNDLE_KEY_CATID, selectedCategory.id);
        }
        bundle.putInt(BUNDLE_FILTER_TYPE, filterType.id);
        bundle.putSerializable(BUNDLE_KEY_ITEMS, list);
//        newFragment.setArguments(bundle);

        if (newFragment.getArguments() == null) {
            newFragment.setArguments(bundle);
        } else {
            newFragment.getArguments().clear();
            newFragment.getArguments().putAll(bundle);
        }

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_layout, newFragment, tag);
        fragmentTransaction.commit();

        if (miSwitch != null) {
            miSwitch.setTitle(getString(mode ? R.string.action_switch_to_map : R.string.action_switch_to_list));
            Drawable dr = ResourcesCompat.getDrawable(getResources(), mode ? R.drawable.ic_place_white : R.drawable.ic_list_white_24dp, null);
            miSwitch.setIcon(dr);
        }

        if (miFilter != null) {
            miFilter.setVisible(mode && selectedCategory != EContentCategory.THINGS_TODO); // list
        }
    }

//    private void getContentData() {
//        Log.d(TAG, "getContentData");
//
//        ContentDatabase.INSTANCE.getItemsFor(selectedCategory, false, new DefaultDataResponse<ArrayList<ContentListItem>>() {
//            @Override
//            public void onSuccessImpl(ArrayList<ContentListItem> response) {
//                switchToMode(isListView, response);
////                announceModelChange();
//            }
//
//            @Override
//            public void onErrorImpl(int errorCode) {
//                switchToMode(isListView, null);
//                handleError(errorCode);
//            }
//        });
//    }

    private Fragment getCurrentFragment() {
        FragmentManager fragmentManager = getChildFragmentManager();
        return fragmentManager.findFragmentById(R.id.content_layout);
    }

    private void changeFilterType(EContentFilterType newFilter) {
        if (newFilter != filterType) {
            filterType = newFilter;
            announceFilterChange(filterType);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (!locationIsSet) {
            // model list/map zmieniamy tylko za 1 razem...?
            if (location != null) {
                locationIsSet = true;
                miFilterDistance.setVisible(true);
                updateItems(true);
            }
        }
        announceLocationChange(location);
    }

    public void updateItems(boolean force) {
        final int tmpSelectedCategory = selectedCategory.id;

        announceModelRefreshing(true);

        ContentDatabase.INSTANCE.getItemsFor(selectedCategory, force, new DefaultDataResponse<ArrayList<ContentListItem>>() {
            @Override
            public void onSuccessImpl(ArrayList<ContentListItem> response) {
                if (tmpSelectedCategory == selectedCategory.id) {
                    lastResponse = response;
                    announceModelChange(response);
                } else {
                    Log.d(TAG, "canceled response for cat " + tmpSelectedCategory);
                }
            }

            @Override
            public void onErrorImpl(int errorCode) {
                handleError(errorCode);
            }
        });
    }

    private void handleError(int errorCode) {

        if (errorCode != CustomErrorCodes.NO_CONNECTION_ERROR) {
            AppCompatMsgDialog.warning(getFragmentManager(), getString(R.string.error_handle_message, errorCode));
        } else if (selectedCategory == EContentCategory.THINGS_TODO && !isNoInternetInfoShowed) {
            AppCompatMsgDialog.warning(getFragmentManager(), getString(R.string.info_internet_required_for_functionality), new ResultReceiver(null) {
                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData) {
                    isNoInternetInfoShowed = true;
                }
            });
        }
        announceModelRefreshing(false);
    }

    //<editor-fold desc="Announces">
    private void announceModelClear() {
        Log.d(TAG, "announceModelClear");

        FragmentManager fragmentManager = getChildFragmentManager();
        if (fragmentManager.getFragments() != null) {
            for (Fragment fr : fragmentManager.getFragments()) {
                if (fr != null && fr instanceof IContentFragment) {
                    ((IContentFragment) fr).onCategorySwitch(selectedCategory, null);
                }
            }
        }
    }

    private void announceModelChange(List<ContentListItem> newModel) {
        Log.d(TAG, "announceModelChange");

        FragmentManager fragmentManager = getChildFragmentManager();
        if (fragmentManager.getFragments() != null) {
            for (Fragment fr : fragmentManager.getFragments()) {
                if (fr != null && fr instanceof IContentFragment) {
                    ((IContentFragment) fr).onCategorySwitch(selectedCategory, newModel);
                }
            }
        } else {
            Log.w(TAG, "No fragments!?");
        }
    }

    private void announceFilterChange(EContentFilterType type) {
        Log.d(TAG, "announceFilterChange");

        FragmentManager fragmentManager = getChildFragmentManager();
        if (fragmentManager.getFragments() != null) {
            for (Fragment fr : fragmentManager.getFragments()) {
                if (fr != null && fr instanceof ContentListFragment) {
                    ((ContentListFragment) fr).onFilterChange(type);
                }
            }
        } else {
            Log.w(TAG, "No fragments!?");
        }
    }

    private void announceLocationChange(Location location) {
        Log.d(TAG, "announceModelChange");
        FragmentManager fragmentManager = getChildFragmentManager();
        if (fragmentManager.getFragments() != null) {
            for (Fragment fr : fragmentManager.getFragments()) {
                if (fr != null && fr instanceof LocationSource.OnLocationChangedListener) {
                    ((LocationSource.OnLocationChangedListener) fr).onLocationChanged(location);
                }
            }
        }
    }

    private void announceModelRefreshing(boolean value) {
        Log.d(TAG, "announceModelRefreshing");
        Fragment currentFr = getCurrentFragment();
        if (currentFr != null && currentFr instanceof IContentFragment) {
            ((IContentFragment)currentFr).setModelRefreshing(value);
        }
    }
    //</editor-fold>
}
