package com.globallogic.koszalin.content.data;

import java.util.List;

/**
 * IContentFragment
 * <hr /> Created by damian.giedrys on 2016-05-16.
 */
public interface IContentFragment {

    void onCategorySwitch (EContentCategory cat, List<ContentListItem> items);

    void setModelRefreshing(boolean value);

}
