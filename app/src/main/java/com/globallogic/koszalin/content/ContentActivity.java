package com.globallogic.koszalin.content;

import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.globallogic.koszalin.R;
import com.globallogic.koszalin.base.AbstractActivity;
import com.globallogic.koszalin.content.data.ContentItem;
import com.globallogic.koszalin.shared.Utils;
import com.globallogic.koszalin.shared.view.AppCompatMsgDialog;
import com.globallogic.koszalin.shared.view.SimpleFragmentPagerAdapter;
import com.globallogic.koszalin.shared.view.ViewPagerBubbleIndicatorListener;

/**
 * ContentActivity
 * <hr /> Created by damian.giedrys on 2016-05-17.
 */
public class ContentActivity extends AbstractActivity {

    private static final String TAG = ContentActivity.class.getSimpleName();
    public static final String BUNDLE_KEY_ITEM = "bundle_key_content";

    // ui
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    //    private ImageView toolabrImage;
    private AppBarLayout appBarLayout;
    private ViewPager imagesViewPager;

    // vars
    private ContentItem model;


    public ContentActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        // init UI
        setContentView(R.layout.app_bar_content);

        initToolbar(R.id.toolbar, true);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
//        toolabrImage = (ImageView)findViewById(R.id.toolbar_image);
        appBarLayout = (AppBarLayout) findViewById(R.id.appbarlayout);
        imagesViewPager = (ViewPager) findViewById(R.id.images_view_pager);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        if (tabLayout != null) {
            tabLayout.setVisibility(View.GONE);
//            tabLayout.setTabMode(TabLayout.MODE_FIXED);
//            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//            tabLayout.setupWithViewPager(viewPager);
//            tabLayout.getTabAt(0).setIcon(R.drawable.ic_my_location_24dp_white);
//            tabLayout.getTabAt(1).setIcon(R.drawable.ic_location_searching_24dp_white);
            //tabLayout.getTabAt(0).setCustomView(createCustomTabView(R.string.fragment_copy, R.drawable.ic_my_location_24dp_white));
            //tabLayout.getTabAt(1).setCustomView(createCustomTabView(R.string.fragment_paste, R.drawable.ic_location_searching_24dp_white));
        }

        // init
        model = null;
        Intent i = getIntent();
        if (i != null) {
            model = (ContentItem) i.getSerializableExtra(BUNDLE_KEY_ITEM);
        }

        if (model == null) {
            Log.w(TAG, "Empty initial arguments!");
        }

        // adapter
        SimpleFragmentPagerAdapter adapter = new SimpleFragmentPagerAdapter(getSupportFragmentManager());


        if (model != null) {
            ContentInfoFragment fr1 = new ContentInfoFragment();

            Bundle args = new Bundle();
            args.putSerializable(BUNDLE_KEY_ITEM, model);
            fr1.setArguments(args);

            setTitle(Utils.isNullOrEmpty(model.getName()) ? " " : model.getName());
//            toolbar.setTitle(model.getName());

            // TODO po wyłączeniu scrolla (wrap_content height na appBarLayout) nie widać title ?
            // collapsingToolbarLayout
            if (model.getFirstPhoto() != null) {
//                toolabrImage.setVisibility(View.VISIBLE);
                imagesViewPager.setVisibility(View.VISIBLE);
//                toolbar.setCollapsible(true);

//                ViewGroup.LayoutParams lp = appBarLayout.getLayoutParams();
//                lp.height = (int)getResources().getDimension(R.dimen.appbarlayout_scrollable_height);
//                appBarLayout.setLayoutParams(lp);

//                AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) collapsingToolbarLayout.getLayoutParams();
//                params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL|AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED); // list other flags here by |
//                collapsingToolbarLayout.setLayoutParams(params);

//                AppUtils.loadImage(getBaseContext(), model.getFirstPhoto(), toolabrImage);

                ImagesPagerAdapter imagesAdapter = new ImagesPagerAdapter(getSupportFragmentManager(), model.getPhoto());
                imagesViewPager.setAdapter(imagesAdapter);

                LinearLayout imagesPagerIndicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
                imagesViewPager.setOnPageChangeListener(new ViewPagerBubbleIndicatorListener(this.getBaseContext(),
                        imagesPagerIndicator, R.drawable.image_pager_dot_selected, R.drawable.image_pager_dot_notselected, imagesAdapter.getCount()));

            } else { // nie ma zdjęć
                // wyłącza scrollowanie i zdjęcie
//                toolabrImage.setVisibility(View.GONE);
                imagesViewPager.setVisibility(View.GONE);

//                toolbar.setCollapsible(false);

//                ViewGroup.LayoutParams lp = appBarLayout.getLayoutParams();
//                lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
//                appBarLayout.setLayoutParams(lp);

//                AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) collapsingToolbarLayout.getLayoutParams();
//                params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP); // list other flags here by |
//                collapsingToolbarLayout.setLayoutParams(params);

//                collapsingToolbarLayout.setTitle(model.getName());
//                toolbar.setTitle(model.getName());

                appBarLayout.setExpanded(false, false);
            }

            adapter.addFragment(fr1, model != null ? model.getName() : "fr1");

        } else {
            setTitle(" "); // "" jest ignorowane

            AppCompatMsgDialog.warning(getSupportFragmentManager(),
                    getString(R.string.error_handle_message, getString(R.string.empty_data)),
                    new ResultReceiver(null) {

                        @Override
                        protected void onReceiveResult(int resultCode, Bundle resultData) {
                            super.onReceiveResult(resultCode, resultData);
                            finish();
                        }
                    });
        }


//        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) collapsingToolbarLayout.getLayoutParams();
//        params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP); // list other flags here by |
//        collapsingToolbarLayout.setLayoutParams(params);

        if (viewPager != null)
            viewPager.setAdapter(adapter);

    }

    @Override
    public void setTitle(CharSequence title) {
        if (collapsingToolbarLayout != null) {
            collapsingToolbarLayout.setTitle(title);
        } else {
            super.setTitle(title);
        }
    }

}
