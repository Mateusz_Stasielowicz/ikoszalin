package com.globallogic.koszalin.content;

import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.SparseArray;

import com.globallogic.googlesheetlib.query.CustomErrorCodes;
import com.globallogic.koszalin.AppUtils;
import com.globallogic.koszalin.IKApplication;
import com.globallogic.koszalin.MyLocationManager;
import com.globallogic.koszalin.content.data.ContentItem;
import com.globallogic.koszalin.content.data.ContentListItem;
import com.globallogic.koszalin.content.data.ECategory;
import com.globallogic.koszalin.content.data.EContentCategory;
import com.globallogic.koszalin.content.data.EContentType;
import com.globallogic.koszalin.logic.DataProviderImpl;
import com.globallogic.koszalin.logic.DataResponse;
import com.globallogic.koszalin.shared.SharedPreferenceUtils;
import com.globallogic.koszalin.shared.logic.BoolCallback;
import com.google.android.gms.maps.LocationSource;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ContentDatabase
 * <hr /> Created by damian.giedrys on 2016-05-19.
 */
public enum ContentDatabase implements LocationSource.OnLocationChangedListener{

    INSTANCE;


    private static final boolean DEV_DELETE_LOCAL_DB = false; // default: false,
    private static final boolean DEV_DONT_USE_CACHE = false; // default: false,
//    private static final boolean DEV_OFFLINE = false;
    private static final String TAG = ContentDatabase.class.getSimpleName();
    private static final String KEY_LOCAL_DB_UPDATE_TIME = "key_local_db_update_time";
    private static final String DB_FILENAME = "db.txt";

    private HashMap<Integer, ContentItem> allItems;
//    private ArrayList<ContentItem> allItems;
    //    private SerializableSparseArray<ContentItem> allItems;
    private SparseArray<ArrayList<ContentListItem>> menuCache;

    private boolean inited;
    private Date localUpdateDate;
    private double[] lastUserLocation;

    public void initDataset(final DataResponse callback) {
        inited = false;

        long l = SharedPreferenceUtils.getLong(IKApplication.getCtx(), KEY_LOCAL_DB_UPDATE_TIME, 0L);
        localUpdateDate = new Date(l);
        Log.i(TAG, "initDataset, localUpdateDate: " + localUpdateDate);

        if (allItems != null) {
            allItems.clear();
            menuCache.clear();
        } else {
            allItems = new HashMap<>(128);
//            allItems = new ArrayList<>(128);
//            allItems = new SerializableSparseArray<>(128);
            menuCache = new SparseArray<>(EContentCategory.values().length);
        }

        if (DEV_DELETE_LOCAL_DB) {
            File fileDb = getDBFile();
            if (fileDb.exists()) {
                Log.d(TAG, "DEV_DELETE_LOCAL_DB, delete:" + fileDb.delete());
            }
        }

        MyLocationManager.INSTANCE.removeLocationListener(this);
        MyLocationManager.INSTANCE.addLocationChangeListener(this);

//        shouldUpdateDB(new BoolCallback() {
//            @Override
//            public void run(boolean value) {
//
//                if (value) {
//                    // update from SS
//                    DataProviderImpl.INSTANCE.getCategoryItems(IKApplication.getCtx(), EContentCategory.THINGS_TODO, new DefaultDataResponse<List<ContentItem>>() {
//                        @Override
//                        public void onSuccessImpl(List<ContentItem> response) {
//                            Log.i(TAG, "Obtaining " + (response == null ? "NONE" : response.size()) + " items from 'server'");
//
//                            if (response != null)
//                                allItems.addAll(response);
//
//                            saveDbToLocal(); // zapis na dysk
//
//                            localUpdateDate = new Date();
//                            SharedPreferenceUtils.setLong(IKApplication.getCtx(), KEY_LOCAL_DB_UPDATE_TIME, localUpdateDate.getTime());
//                            inited = true;
//                            callback.run();
//                        }
//
//                        @Override
//                        public void onErrorImpl(int errorCode) {
//
//                            // błąd z serwera, probuje działać na cachu
//                            readDbFromLocal();
//                            inited = true;
//                            callback.run();
//
//                            callback.run();
//                        }
//                    });
//
//                } else {
//                    readDbFromLocal(); // update from disk
//
//                    inited = true;
//                    callback.run();
//                }
//            }
//        });

        if (DEV_DONT_USE_CACHE) {
            inited = true;
            callback.onSuccess(null);
        } else {
            readDbFromLocal();
            inited = true;
            callback.onSuccess(null);
        }

    }

    public boolean isInited() {
        return inited;
    }

    public ContentItem getContentItem(int id) {
        checkIfInited();

        // array
//        for (int i = 0; i < allItems.size(); i++) {
//            if (allItems.get(i).getId() == id) {
//                return allItems.get(i);
//            }
//        }

        // sparse
//        ContentItem ret = allItems.get(id);

        ContentItem ret = allItems.get(id);

        if (ret == null)
            Log.w(TAG, "Empty item for id: " + id);
        return ret;
    }

    public void getItemsFor(@NonNull final EContentCategory cat, boolean force, @NonNull final DataResponse<ArrayList<ContentListItem>> callback) {

        checkIfInited();

        final int catId = cat.id;
//        boolean shouldSort = false;
//
//        // latLng changed ?
//        Log.d(TAG, "lastUserLocation: " + Utils.toString(lastUserLocation));
//        double[] latLng = MyLocationManager.INSTANCE.getLastLocation();
//        Log.d(TAG, "latLng: " + Utils.toString(latLng));
//        boolean latLngChanged = false;
//        if (!(latLng != null && lastUserLocation != null && latLng[0] == lastUserLocation[0] && latLng[1] == lastUserLocation[1])) {
//            latLngChanged = true;
//            lastUserLocation = latLng;
//            shouldSort = true;
//        }
//
//        if (latLngChanged) {
//            // measure distance
//            maybeUpdateDistance();
//        }
//
//        if (shouldSort) {
//            Log.d(TAG, "shouldSort menuCache.size(): " + menuCache.size());
//            for (int i = 0 ; i<menuCache.size() ; i++) {
//                Collections.sort(menuCache.valueAt(i), ContentListItemComparator.getInstance());
//            }
//        }

        if (!force && menuCache.get(catId) != null) {
            Log.d(TAG, "getting " + menuCache.get(catId).size() + " items for " + cat + " category");
//            CustomSortedList<ContentListItem> newList2 = new CustomSortedList<>(ContentListItem.class);
//            newList2.addAll(menuCache.get(catId));

            callback.onSuccess(menuCache.get(catId));
//            return menuCache.get(catId);
            return;
        }

        // create new
//        CustomSortedList<ContentListItem> newList = new CustomSortedList<>(ContentListItem.class);

        if (!cat.virtual) {
            DataProviderImpl.INSTANCE.getCategoryItems(IKApplication.getCtx(), ECategory.parse(cat.id),

                    new ContentItemListDataResponse(cat, callback));


        } else if (cat == EContentCategory.THINGS_TODO) {
            DataProviderImpl.INSTANCE.getThingsToDo(IKApplication.getCtx(),
                    new ContentItemListDataResponse(cat, callback));
        } else if (cat == EContentCategory.EVENTS) {
            DataProviderImpl.INSTANCE.getEvents(IKApplication.getCtx(),
                    new ContentItemListDataResponse(cat, callback));
        } else if (cat == EContentCategory.OUTSIDE) {
            DataProviderImpl.INSTANCE.getOutdoors(IKApplication.getCtx(),
                    new ContentItemListDataResponse(cat, callback));
        }

//        ArrayList<ContentListItem> newList = new ArrayList<>(16);
//
//        if (cat != EContentCategory.THINGS_TODO) {
//            for (int i = 0; i < allItems.size(); i++) {
//                ContentItem ci = allItems.get(i);
//                if (ci != null && ci.getCategory() == catId) {
//                    newList.add(ContentListItem.fromItem(ci, lastUserLocation));
//                }
//            }
//        } else {
//            for (int i = 0; i < allItems.size(); i++) {
//                newList.add(ContentListItem.fromItem(allItems.get(i), lastUserLocation));
//            }
//        }
//
//        Collections.sort(newList, ContentListItemComparator.getInstance());
////        CustomSortedList<ContentListItem> newList2 = new CustomSortedList<>(ContentListItem.class);
////        newList2.addAll(newList);
//        menuCache.put(catId, newList);
//        Log.d(TAG, "getting2 " + menuCache.get(catId).size() + " items for " + cat + " category");
////        return menuCache.get(catId);
//        return newList;
    }


    private void shouldUpdateDB(@NonNull BoolCallback callback) {
        Log.d(TAG, "shouldUpdateDB ?");

        // TODO zapytanie o czas do SS

        Date d1 = new Date(localUpdateDate.getTime() + (24 * 60 * 60 * 1000));
        Date now = new Date();

        boolean ret = !isLocalDbFilePresent() || (d1.before(now)); // serverTime.after(localUpdateDate)
        Log.d(TAG, "shouldUpdateDB " + ret);
        callback.run(ret);
    }

    private boolean isLocalDbFilePresent() {
        File fileDb = getDBFile();
        return fileDb.exists() && fileDb.isFile() && fileDb.length() > 0;
    }

    private void saveDbToLocal() {

        File fileDb = getDBFile();
        Log.d(TAG, "saveDbToLocal, fileDb: " + fileDb);

        if (fileDb.exists()) {
            Log.d(TAG, "saveDbToLocal, fileDb exist, deleted: " + fileDb.delete());
        }

        PrintStream ps = null;
        try {
            ps = new PrintStream(getDBFile());
            Gson gson = com.globallogic.googlesheetlib.query.Utils.getGson();

            // save ArrayList
//            List<ContentItem> tmpList = new ArrayList<>();
//            for (int i = 0; i < allItems.size(); i++) {
//                tmpList.add(allItems.get(i));
//            }
//            gson.toJson(tmpList, ps);

            gson.toJson(allItems, ps);

            Log.d(TAG, "saveDbToLocal, saved " + allItems.size() + " items!");

        } catch (IOException ioe) {
            Log.w(TAG, "Error during db creating", ioe);
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception ioe) {
                    Log.w(TAG, "Error during file closing", ioe);
                }
            }
        }
    }

    private void readDbFromLocal() {
        allItems.clear();

        File fileDb = getDBFile();
        Log.d(TAG, "readDbFromLocal, fileDb: " + fileDb);

        if (!fileDb.exists()) {
            Log.d(TAG, "readDbFromLocal, fileDb don't exist");
            return;
        }

        BufferedReader r = null;
        try {
            Gson gson = com.globallogic.googlesheetlib.query.Utils.getGson();
            r = new BufferedReader(new FileReader(getDBFile()));
            // save ArrayList

            HashMap<Integer, ContentItem> tmpArray = gson.fromJson(r, new TypeToken<HashMap<Integer, ContentItem>>() {
            }.getType());
//            allItems = gson.fromJson(r, new TypeToken<SerializableSparseArray<ContentItem>>() {
//            }.getType());



            Log.d(TAG, "readDbFromLocal, fileDb contains " + (tmpArray != null ? tmpArray.size() : "NULL") + " items");

            // removing old Event
            Date now = new Date();
            Date dTmp;
            for (Integer key : tmpArray.keySet()) {
                ContentItem ci = tmpArray.get(key);
                if (ci != null) {
                    if (ci.getType() == EContentType.EVENT) {
                        dTmp = ci.getDatetimeFrom();
                        if (dTmp == null)
                            dTmp = ci.getDatetimeFrom();
                        if (dTmp != null && dTmp.after(now)) {
                            allItems.put(key, ci);
                        }
                    } else {
                        allItems.put(key, ci);
                    }
                }
            }

            Log.d(TAG, "readDbFromLocal, fileDb allItems: " + allItems.size() + ", removed " +
                    (tmpArray.size() - allItems.size()) + " old EVENT's");

        } catch (IOException ioe) {
            Log.w(TAG, "Error during db creating", ioe);
        } finally {
            if (r != null) {
                try {
                    r.close();
                } catch (Exception ioe) {
                    Log.w(TAG, "Error during file closing", ioe);
                }
            }
        }
    }

    private File getDBFile() {
        return IKApplication.getCtx().getFileStreamPath(DB_FILENAME);
    }

    private void maybeUpdateDistance() {

        for (int i = 0; i < menuCache.size(); i++) {
            List<ContentListItem> items = menuCache.valueAt(i);
            for (ContentListItem item : items) {

                if (item.hasLocation() && AppUtils.isValidLocation(lastUserLocation)) {
                    item.distance = AppUtils.distance(lastUserLocation[0], lastUserLocation[1], item.lat, item.lng);
                } else {
                    item.distance = Double.MIN_VALUE;
                }
            }
        }
    }

    private void checkIfInited() {
        assert inited : "Database not inited!";
    }

    private boolean updateDataSet(List<ContentItem> serverData) {

        boolean updated = false;
        if (serverData != null && serverData.size() > 0) {

            // for (final Iterator<ContentItem> iter = serverData.iterator() ; iter.hasNext();) {
            for (ContentItem ci : serverData) {
                if (allItems.get(ci.getId()) == null) {
                    allItems.put(ci.getId(), ci);
                    updated = true;
                }
            }
        }
        return updated;
    }

    private void putToCache(EContentCategory contCat, List<ContentItem> response) {

        if (response != null && response.size() > 0) {
            ArrayList<ContentListItem> newList = new ArrayList<>(response.size());
            for (int i = 0; i < response.size(); i++) {
                newList.add(ContentListItem.fromItem(response.get(i), contCat, MyLocationManager.INSTANCE.getLastLocation()));
            }
            menuCache.put(contCat.id, newList);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged, location: " + location);

        // update cached items distance info
        if (menuCache != null && location != null) {
            double[] myLoc = MyLocationManager.INSTANCE.getLastLocation();
            for (int i = 0 ; i<menuCache.size() ; i++) {
                ArrayList<ContentListItem>  arr = menuCache.get(menuCache.keyAt(i));
                if (arr != null && arr.size() > 0) {
                    for (ContentListItem cli : arr) {
                        ContentListItem.maybeUpdateDistane(cli, myLoc);
                    }
                }
            }
        }

    }

    private class ContentItemListDataResponse implements DataResponse<List<ContentItem>> {

        private final EContentCategory requestCat;
        private final DataResponse<ArrayList<ContentListItem>> clientCallback;

        ContentItemListDataResponse(EContentCategory requestCat, final DataResponse<ArrayList<ContentListItem>> callback) {
            this.requestCat = requestCat;
            this.clientCallback = callback;
        }

        @Override
        public void onSuccess(List<ContentItem> response) {
            boolean updated = updateDataSet(response);
            putToCache(requestCat, response);
            if (updated && !DEV_DONT_USE_CACHE) {
                saveDbToLocal();
            }

            clientCallback.onSuccess(menuCache.get(requestCat.id));
        }

        @Override
        public void onError(int errorCode) {
            if (errorCode == CustomErrorCodes.NO_CONNECTION_ERROR) {
                Log.d(TAG, "NO_CONNECTION_ERROR");
                // no internet access? trying to get data from cache..
                ArrayList<ContentListItem> ret = menuCache.get(requestCat.id);
                if (ret != null && ret.size() > 0) {
                    Log.d(TAG, "getting from old cache");
                    clientCallback.onSuccess(ret);
                } else {
                    if (requestCat == EContentCategory.THINGS_TODO) {
                        clientCallback.onError(errorCode);
                    } else {
                        // populate from localDB
                        ret = new ArrayList<>();
                        double[] myLoc = MyLocationManager.INSTANCE.getLastLocation();

                        for (Integer id : allItems.keySet()) {
                            ContentItem ci = allItems.get(id);
                            if (ci != null) {
                                if (EContentCategory.haveCategory(ci, requestCat)) {
                                    ret.add(ContentListItem.fromItem(ci, requestCat, myLoc));
                                }
                            }
                        }
                        menuCache.put(requestCat.id, ret);
                        Log.d(TAG, "populating " + ret.size() + " item from old cache");
                        clientCallback.onSuccess(ret);
                    }
                }

//                clientCallback.onError(errorCode);
            } else {
                clientCallback.onError(errorCode);
            }
        }
    }
}
