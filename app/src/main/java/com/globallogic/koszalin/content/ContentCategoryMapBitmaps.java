package com.globallogic.koszalin.content;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.SparseArray;

import com.globallogic.koszalin.AppUtils;
import com.globallogic.koszalin.content.data.EContentCategory;
import com.google.android.gms.maps.model.BitmapDescriptor;

/**
 * ContentCategoryMapBitmaps
 * <hr /> Created by damian.giedrys on 2016-05-16.
 */
public class ContentCategoryMapBitmaps extends SparseArray <BitmapDescriptor> {

    //    private final Map<EContentCategory, BitmapDescriptor> map;
    private static final SparseArray<BitmapDescriptor> map = new SparseArray<>(EContentCategory.values().length);

    ContentCategoryMapBitmaps() {
//        map = new HashMap<>(EContentCategory.values().length + 1);
//        map = new SparseArray<>(EContentCategory.values().length);
    }


    public static BitmapDescriptor getMarkerBitmap(@NonNull Context ctx, EContentCategory cat) {
        if (cat == null) {
            return null;
        }

        if (map.indexOfKey(cat.id) >= 0) {
            return map.get(cat.id);
        }

        try {
            BitmapDescriptor bd = AppUtils.createBitmapDescriptor(ctx, cat.iconResMapMarker);
            map.put(cat.id, bd);
            return bd;
        } catch (Exception e) {
            return null;
        }
    }
}
