package com.globallogic.koszalin.content;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.provider.CalendarContract;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.globallogic.koszalin.AppUtils;
import com.globallogic.koszalin.DateUtils;
import com.globallogic.koszalin.R;
import com.globallogic.koszalin.base.AbstractFragment;
import com.globallogic.koszalin.content.data.ContentItem;
import com.globallogic.koszalin.shared.AndroidUtils;
import com.globallogic.koszalin.shared.Utils;
import com.globallogic.koszalin.shared.view.AppCompatMsgDialog;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * PlaceContentFragment
 * <hr /> Created by damian.giedrys on 2016-05-17.
 */
public class ContentInfoFragment extends AbstractFragment implements View.OnClickListener {

    private static final String TAG = ContentInfoFragment.class.getSimpleName();
    private static final String FORMAT_GOOGLE_NAV = "google.navigation:q=%.6f,%.6f";
    // private static final String FORMAT_GEO_LOC = "geo:%.6f,%.6f?z=15&q=%s";
    private static final String FORMAT_GEO_LOC = "geo:%.6f,%.6f?z=15&q=%.6f,%.6f(%s)";
    private static final String STR_OPEN_ALL_DAY = "00:00 - 23:59";


    // ui
    private TextView viewOpenHoursSmall;
    private View viewOpenHoursBig;
    private View viewopenHoursEvent;

    // vars
    private ContentItem model;
    private boolean openHoursMode;
    private String strClosed;
    private String strOpen;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreate, savedInstanceState=" + savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            model = (ContentItem) bundle.getSerializable(ContentActivity.BUNDLE_KEY_ITEM);
            Log.d(TAG, "Got item: " + model);
        } else {
            Log.w(TAG, "Empty initial arguments!");
        }

        strClosed = getString(R.string.closed);
        strOpen = getString(R.string.open);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView, savedInstanceState=" + savedInstanceState);
        return inflater.inflate(R.layout.fragment_content_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onViewCreated, savedInstanceState=" + savedInstanceState);
        super.onViewCreated(view, savedInstanceState);

        // init
        if (model != null) {

            // photo
            if (model.getFirstPhoto() != null) {
                AppUtils.loadImage(getContext(), model.getFirstPhoto(), (ImageView) initView(view, (R.id.content_item_image)));
            }

            // Buttons
            AppCompatButton btnCall = initView(view, R.id.btn_call, this);
//            btnCall.setCompoundDrawables(null, null,
//                    AndroidUtils.getDrawable(getContext(), R.drawable.ic_call_normal_24dp), null);
//            btnCall.setCompoundDrawables(null, null,
//                    AndroidUtils.getVectorDrawable(getContext(), R.drawable.ic_call_normal_24dp), null);
            btnCall.setVisibility(Utils.isNullOrEmpty(model.getTelephone()) ? View.GONE : View.VISIBLE);

            Button btnDirections = initView(view, R.id.btn_directions, this);
            btnDirections.setVisibility(model.hasLocalisation() ? View.VISIBLE : View.GONE);

            Button btnWebsite = initView(view, R.id.btn_website, this);
            btnWebsite.setVisibility(Utils.isNullOrEmpty(model.getWww()) ? View.GONE : View.VISIBLE);

            // Description
            String desc = model.getDescriptionLong();
            if (Utils.isNullOrEmpty(desc)) {
                desc = model.getDescriptionShort();
            }
            if (Utils.isNullOrEmpty(desc)) {
                desc = "";
            }
            initTxtView(view, R.id.content_item_desc, Html.fromHtml(desc));

            // Address
            initTxtView(view, R.id.content_item_address_txt, model.getAddressString());
            initView(view, R.id.content_item_address_row, this).setVisibility(
                    Utils.isNullOrEmpty(model.getAddressString()) ? View.GONE : View.VISIBLE);

            // Tel 1
            initTxtView(view, R.id.content_item_tel_txt, model.getTelephone());
            initView(view, R.id.content_item_tel_row, this).setVisibility(
                    Utils.isNullOrEmpty(model.getTelephone()) ? View.GONE : View.VISIBLE);

            // Tel 2
            initTxtView(view, R.id.content_item_tel2_txt, model.getTelephone2());
            initView(view, R.id.content_item_tel2_row, this).setVisibility(
                    Utils.isNullOrEmpty(model.getTelephone2()) ? View.GONE : View.VISIBLE);

            // eMail
            initTxtView(view, R.id.content_item_mail_txt, model.getMail());
            initView(view, R.id.content_item_mail_row, this).setVisibility(
                    Utils.isNullOrEmpty(model.getMail()) ? View.GONE : View.VISIBLE);

            // open hours
            View viewOpenHours = initView(view, R.id.content_item_openhours_row);
            viewopenHoursEvent = initView(view, R.id.content_item_openhours_event, this);
            viewOpenHoursBig = initView(view, R.id.content_item_openhours_txt_big, this);
            viewOpenHoursSmall = initView(view, R.id.content_item_openhours_txt_small, this);
            if (model.hasOpenHours()) {

                switch (model.getType()) {
                    case PLACE:
                        viewopenHoursEvent.setVisibility(View.GONE);

                        boolean isAllClosed = true;
                        for (int i = 0; i < 7; i++) {
                            if (!Utils.isNullOrEmpty(model.getOpeningHours()[i])) {
                                isAllClosed = false;
                                break;
                            }
                        }
                        if (isAllClosed) {
                            viewOpenHours.setVisibility(View.GONE);
                        } else {
                            viewOpenHours.setVisibility(View.VISIBLE);
                            viewOpenHoursSmall.setVisibility(View.VISIBLE);
                            viewOpenHoursBig.setVisibility(View.GONE);

                            Calendar calendar = Calendar.getInstance(new Locale("pl"));
                            int today = calendar.get(Calendar.DAY_OF_WEEK);
                            String strTodayOpen = model.getOpeningHours()[calendarDayToTable(today)];
                            Log.d(TAG, "today: " + today);

                            // big
                            initOpenHour(view, R.id.open_hour_0_lbl, R.id.open_hour_0, model.getOpeningHours()[0], today == Calendar.MONDAY);
                            initOpenHour(view, R.id.open_hour_1_lbl, R.id.open_hour_1, model.getOpeningHours()[1], today == Calendar.TUESDAY);
                            initOpenHour(view, R.id.open_hour_2_lbl, R.id.open_hour_2, model.getOpeningHours()[2], today == Calendar.WEDNESDAY);
                            initOpenHour(view, R.id.open_hour_3_lbl, R.id.open_hour_3, model.getOpeningHours()[3], today == Calendar.THURSDAY);
                            initOpenHour(view, R.id.open_hour_4_lbl, R.id.open_hour_4, model.getOpeningHours()[4], today == Calendar.FRIDAY);
                            initOpenHour(view, R.id.open_hour_5_lbl, R.id.open_hour_5, model.getOpeningHours()[5], today == Calendar.SATURDAY);
                            initOpenHour(view, R.id.open_hour_6_lbl, R.id.open_hour_6, model.getOpeningHours()[6], today == Calendar.SUNDAY);

                            // small
                            int h = calendar.get(Calendar.HOUR_OF_DAY);
                            int m = calendar.get(Calendar.MINUTE);
                            int nowMinutes = m + h * 60;

                            if (Utils.isNullOrEmpty(strTodayOpen)) {
                                viewOpenHoursSmall.setText(strClosed);
                            } else {
                                if (strTodayOpen.equals(STR_OPEN_ALL_DAY)) {
                                    viewOpenHoursSmall.setText(strOpen);
                                } else {
                                    int[] todayOpenH = openMinutesFromStr(strTodayOpen);
                                    if (todayOpenH != null) {
                                        boolean isOpened = nowMinutes > todayOpenH[0] && nowMinutes < todayOpenH[1];

                                        String str;
                                        int idx1 = strTodayOpen.indexOf('-');
                                        if (isOpened) {
                                            str = getString(R.string.info_open_closed_at, strTodayOpen.substring(idx1 + 1, strTodayOpen.length()).trim());
                                        } else {
                                            str = getString(R.string.info_closed_open_at, strTodayOpen.substring(0, idx1).trim());
                                        }
                                        viewOpenHoursSmall.setText(Html.fromHtml(str));
                                    } else {
                                        viewOpenHoursSmall.setText(strClosed);
                                    }
                                }
                            }
                        }

                        break;
                    case EVENT:
                        viewOpenHoursBig.setVisibility(View.GONE);
                        viewOpenHoursSmall.setVisibility(View.GONE);
                        viewopenHoursEvent.setVisibility(View.VISIBLE);
                        initOpenHour(view, R.id.open_hour_from, model.getDatetimeFrom());
                        initOpenHour(view, R.id.open_hour_to, model.getDatetimeTo());

                        break;
                    default:
                        viewopenHoursEvent.setVisibility(View.GONE);
                        viewOpenHoursBig.setVisibility(View.GONE);
                        viewOpenHoursSmall.setVisibility(View.GONE);
                        break;
                }
            } else {
                viewOpenHours.setVisibility(View.GONE);
            }
        } else {
            // empty model, for unknown reason: close view
            view.findViewById(R.id.content_info_main_container).setVisibility(View.INVISIBLE);


        }
    }

    private int calendarDayToTable(int calendarToday) {
        switch (calendarToday) {
            case Calendar.MONDAY:
                return 0;
            case Calendar.TUESDAY:
                return 1;
            case Calendar.WEDNESDAY:
                return 2;
            case Calendar.THURSDAY:
                return 3;
            case Calendar.FRIDAY:
                return 4;
            case Calendar.SATURDAY:
                return 5;
            case Calendar.SUNDAY:
                return 6;
        }
        return -1;
    }


//    private void initTxtView(View root, @IdRes int id, String str) {
//        TextView txt = initView(root, id);
//        txt.setText(str);
//    }


    private TextView initTxtView(View root, @IdRes int id, CharSequence str) {
        TextView txt = initView(root, id);
        if (Utils.isNullOrEmpty(str)) {
            txt.setText("");
            txt.setVisibility(View.GONE);
        } else {
            txt.setText(str);
            txt.setVisibility(View.VISIBLE);
        }
        return txt;
    }

    private void initOpenHour(View root, @IdRes int lblId, @IdRes int id, String hour, boolean bold) {
        String str = Utils.isNullOrEmpty(hour) ? strClosed : hour.equals(STR_OPEN_ALL_DAY) ? strOpen : hour;
        if (bold) {
            str = "<b>" + str + "</b>";
        }
        initTxtView(root, id, Html.fromHtml(str));

        ((TextView) root.findViewById(lblId)).setTypeface(null, bold ? Typeface.BOLD : Typeface.NORMAL);
    }

    private void initOpenHour(View root, @IdRes int id, Date date) {
        if (date != null) {
            initTxtView(root, id, DateUtils.formatEventDate(date));
        } else {
            initTxtView(root, id, null);
        }
    }

    @Override
    public void onClick(View v) {
        if (v != null) {
            switch (v.getId()) {
                case R.id.btn_call: // dial
                    maybeDial(this, model.getTelephone());
                    break;
                case R.id.btn_directions: // get direction by GMaps
                    if (model.hasLocalisation()) {
                        String strUri = String.format(Locale.US, FORMAT_GOOGLE_NAV,
                                model.getAddress().localization.lat, model.getAddress().localization.lng);

                        maybeOpenGM(this, strUri);
                    }
                    break;
                case R.id.btn_website: // open www in browser
                    maybeOpenBrowser(this, model.getWww());
                    break;
                case R.id.content_item_mail_row:

                    AndroidUtils.sendEmail(getActivity(), getString(R.string.intent_send_email_via), model.getMail(), null, null);
                    break;
                case R.id.content_item_openhours_txt_small:
                case R.id.content_item_openhours_txt_big:
                    doSwitchOpenHours();
                    break;
                case R.id.content_item_address_row:

                    if (model.hasLocalisation()) {

//                        String strUri = String.format(Locale.US, FORMAT_GEO_LOC,
//                                model.getAddress().localization.lat, model.getAddress().localization.lng, Uri.encode(model.getName()));

                        ContentItem.Localization loc = model.getAddress().localization;
                        String strUri = String.format(Locale.US, FORMAT_GEO_LOC,
                                loc.lat, loc.lng, loc.lat, loc.lng, Uri.encode(model.getName()));

                        Log.i(TAG, strUri);

                        maybeOpenGM(this, strUri);
                    }
                    break;
                case R.id.content_item_tel_row:
                    maybeDial(this, model.getTelephone());
                    break;
                case R.id.content_item_tel2_row:
                    maybeDial(this, model.getTelephone2());
                    break;
                case R.id.content_item_openhours_event:
                    maybeAddCalendarEvent(this, model);
                    break;
            }
        }
    }

    private void doSwitchOpenHours() {
        openHoursMode = !openHoursMode;
        viewOpenHoursBig.setVisibility(openHoursMode ? View.VISIBLE : View.GONE);
        viewOpenHoursSmall.setVisibility(openHoursMode ? View.GONE : View.VISIBLE);
    }

    private static void maybeDial(@NonNull AbstractFragment fr, String phone) {
        try {
            if (!Utils.isNullOrEmpty(phone)) { // just in case
                // ACTION_CALL - wymaga osobnego prawa
                Intent callIntent = new Intent(Intent.ACTION_DIAL,
                        Uri.fromParts("tel", phone, null));
                fr.startActivity(callIntent);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
    }

//    private static void maybeMail(AbstractFragment fr, String email) {
//        try {
//            if (!Utils.isNullOrEmpty(email)) { // just in case
//                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null));
//                //emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject != null ? subject : ""); // subject
//                //emailIntent.putExtra(Intent.EXTRA_TEXT, text != null ? text : ""); // body
//                fr.startExtActivity(emailIntent);
//            }
//        } catch (Exception e) {
//            Log.e(TAG, e.getLocalizedMessage(), e);
//        }
//    }

    private static void maybeOpenBrowser(@NonNull AbstractFragment fr, String www) {
        try {
            if (!Utils.isNullOrEmpty(www)) {
                String strL = www.toLowerCase();
                String str = www;
                if (!strL.startsWith("http://") && !strL.startsWith("https://")) {
                    str = ("http://" + www);
                }
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(str));
                fr.startExtActivity(browserIntent);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
    }

    private static void maybeOpenGM(final @NonNull AbstractFragment fr, String strUri) {
        try {
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
            mapIntent.setPackage(AndroidUtils.GOOGLE_MAPS_PACKAGE_NAME); // preferred

            boolean b = fr.startExtActivity(mapIntent);
            if (!b) {
                AppCompatMsgDialog.question(fr.getFragmentManager(), fr.getString(R.string.info_no_activity_to_handle_intent), new ResultReceiver(null) {
                    @Override
                    protected void onReceiveResult(int resultCode, Bundle resultData) {
                        if (resultCode == DialogInterface.BUTTON_POSITIVE) {
                            AndroidUtils.openApplicationPageInGooglePlay(fr.getActivity(), AndroidUtils.GOOGLE_MAPS_PACKAGE_NAME);
                        }
                    }
                });
            }

        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
    }

    private static void maybeAddCalendarEvent(final @NonNull AbstractFragment fr, ContentItem model) {
        if (model != null && model.getDatetimeFrom() != null && model.getDatetimeTo() != null) {
            Intent intent = new Intent(Intent.ACTION_INSERT)
                    .setData(CalendarContract.Events.CONTENT_URI)
                    .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, model.getDatetimeFrom().getTime())
                    .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, model.getDatetimeTo().getTime())
                    .putExtra(CalendarContract.Events.TITLE, model.getName())
//                    .putExtra(CalendarContract.Events.DESCRIPTION, "Group class")
                    .putExtra(CalendarContract.Events.EVENT_LOCATION, model.getAddressString())
                    .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
//                    .putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com")
                    ;
            fr.startExtActivity(intent);
        }

//        Intent intent = new Intent(Intent.ACTION_INSERT)
//                .setData(Events.CONTENT_URI)
//                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
//                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
//                .putExtra(Events.TITLE, "Yoga")
//                .putExtra(Events.DESCRIPTION, "Group class")
//                .putExtra(Events.EVENT_LOCATION, "The gym")
//                .putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY)
//                .putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com");
//        startActivity(intent);

    }

    private static int[] openMinutesFromStr(String str) {

        if (!Utils.isNullOrEmpty(str)) {
            int i = str.indexOf('-');
            if (i != -1) {
                String str1 = str.substring(0, i).trim();
                String str2 = str.substring(i + 1, str.length()).trim();

                int i2 = str1.indexOf(':');
                int i3 = str2.indexOf(':');
                if (i2 != -1 && i3 != -1) {
                    try {
                        int h1 = Integer.parseInt(str1.substring(0, i2));
                        int m1 = Integer.parseInt(str1.substring(i2 + 1, str1.length()));
                        int h2 = Integer.parseInt(str2.substring(0, i3));
                        int m2 = Integer.parseInt(str2.substring(i3 + 1, str2.length()));
//                        Log.i("TEST", "h1: '" + h1 + "', m1: '" + m1 + "', h2: '" + h2 + "', m2: '" + m2 + "'");
                        return new int[]{h1 * 60 + m1, h2 * 60 + m2};
                    } catch (NumberFormatException nfe) {
                        Log.w(TAG, nfe);
                    }
                }
            }
        }
        return null;
    }

}
