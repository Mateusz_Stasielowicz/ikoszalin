package com.globallogic.koszalin.content.data;

import android.util.Log;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * ContentListItemComparator
 * <hr /> Created by damian.giedrys on 2016-05-23.
 */
public class ContentListItemComparator2 implements Comparator<ContentListItem> {

    private static final String TAG = ContentListItemComparator2.class.getSimpleName();

    private static Map<EContentFilterType, ContentListItemComparator2> _instance = null;

//    private static ContentListItemComparator2 _instance = null;

    public static ContentListItemComparator2 getInstance(EContentFilterType type) {
        if (_instance == null) {
            _instance = new HashMap<>();
        }
        if (!_instance.containsKey(type)) {
            _instance.put(type, new ContentListItemComparator2(type));
        }
        return _instance.get(type);
    }

    private final EContentFilterType type;

    private ContentListItemComparator2(EContentFilterType type) {
        this.type = type;
    }

    @Override
    public int compare(ContentListItem ci1, ContentListItem ci2) {
//        Log.d(TAG, "comparing " + ci1 + ", with " + ci2);

        if (ci1 == null && ci2 != null) {
            return -1;
        }
        if (ci1 != null && ci2 == null) {
            return 1;
        }
        if ((ci1 == null && ci2 == null) || (ci1 != null && ci2 != null && ci1.equals(ci2))) {
            return 0;
        }

        switch (type) {
            case BY_DISTANCE:
                if (ci1.distance == Double.MIN_VALUE && ci2.distance != Double.MIN_VALUE) {
                    return 1;
                }
                if (ci1.distance != Double.MIN_VALUE && ci2.distance == Double.MIN_VALUE) {
                    return -1;
                }
                double diff = ci1.distance - ci2.distance;
                if (diff < 0.0) {
                    return -1;
                } else if (diff > 0.0) {
                    return 1;
                }
                return 0;
            case BY_TIME:
                if (ci1.dateFrom == null && ci2.dateFrom != null) {
                    return 1;
                }
                if (ci1.dateFrom != null && ci2.dateFrom == null) {
                    return -1;
                }
                if ((ci1.dateFrom == null && ci2.dateFrom == null) ||
                        (ci1.dateFrom != null && ci2.dateFrom != null && ci1.dateFrom.getTime() == ci2.dateFrom.getTime())) {
                    return 0;
                }
                return ci1.dateFrom.after(ci2.dateFrom) ? 1 : -1;
            case BY_POPULARITY:
            default:
                int i = ci1.name.compareToIgnoreCase(ci2.name);
                return i;
        }
    }
}
