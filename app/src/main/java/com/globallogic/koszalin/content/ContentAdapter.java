package com.globallogic.koszalin.content;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.globallogic.koszalin.AppUtils;
import com.globallogic.koszalin.R;
import com.globallogic.koszalin.content.data.ContentListItem;
import com.globallogic.koszalin.shared.Utils;
import com.globallogic.koszalin.shared.view.AbstractRecyclerViewAdapter;
import com.globallogic.koszalin.shared.view.CustomSortedList;

/**
 * ContentFragment
 * <hr /> Created by damian.giedrys on 2016-05-13.
 */
public class ContentAdapter extends AbstractRecyclerViewAdapter<ContentListItem> {


    public ContentAdapter(CustomSortedList<ContentListItem> mDataset, Context mContext) {
        super(mDataset, mContext);
    }

    public ContentAdapter(CustomSortedList<ContentListItem> mDataset, Context mContext, AdapterView.OnItemClickListener onClickListener) {
        super(mDataset, mContext, onClickListener);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View mainView;
        protected ImageView icon;
        protected TextView title;
        protected TextView title2;
        protected TextView distance;
//        protected TextView stars;

        public ViewHolder(View v) {
            super(v);
            this.mainView = v;
            this.icon = (ImageView) v.findViewById(R.id.content_item_image);
            this.title = (TextView) v.findViewById(R.id.content_item_title);
            this.title2 = (TextView) v.findViewById(R.id.content_item_title2);
            this.distance = (TextView) v.findViewById(R.id.content_item_distance);
//            this.stars = (TextView) v.findViewById(R.id.content_item_star);
        }
    }

    @Override
    public ContentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {

//        View v = LayoutInflater.from(parent.getContext())
        View v = LayoutInflater.from(mContext)
                .inflate(R.layout.adapter_item_content, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder h, int position) {
        ContentListItem item = getItem(position);
        ContentAdapter.ViewHolder holder = (ViewHolder) h;
        if (mOnClickListener != null) {
            holder.mainView.setTag(holder);
            holder.mainView.setOnClickListener(this);
        }
        if (item == null) {
            return;
        }

        if (!Utils.isNullOrEmpty(item.photoUrl)) {
            AppUtils.loadImage(mContext, item.photoUrl, holder.icon);
        } else {
            AppUtils.loadDefaultImage(mContext, holder.icon);
        }

//        if(item.getImageRes() > 0) {
//            holder.icon.setImageResource(item.getImageRes());
//        }else if(item.getImageUrl() != null){
//
//        }

        holder.title.setText(item.name);
        holder.title2.setText(item.name);

        if (item.distance == Double.MIN_VALUE) {
            holder.distance.setVisibility(View.GONE);
        } else {
            holder.distance.setText(mContext.getString(R.string.format_distance, item.distance));
            holder.distance.setVisibility(View.VISIBLE);
        }

//        holder.distance.setText(String.valueOf(item.getDistance()) + "km");
//        holder.stars.setText(String.valueOf(item.getStars()));

    }
}
