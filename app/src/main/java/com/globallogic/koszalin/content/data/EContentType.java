package com.globallogic.koszalin.content.data;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

/**
 * EContentType
 * <hr /> Created by damian.giedrys on 2016-05-19.
 */
public enum EContentType {

    @SerializedName("1")
    PLACE(1),
    @SerializedName("2")
    EVENT(2);

    EContentType(int id) {
        this.id = id;
    }

    public final int id;


    public static EContentType parse(int id) {
        for (EContentType t : EContentType.values()) {
            if (t.id == id) {
                return t;
            }
        }
        Log.w("EContentType", "Unknown id for parsing: " + id);
        return null;
    }

}
