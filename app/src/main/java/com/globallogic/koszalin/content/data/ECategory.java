package com.globallogic.koszalin.content.data;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

/**
 * ECategory
 * <hr /> Created by damian.giedrys on 2016-06-01.
 */
public enum ECategory {

    @SerializedName("1")
    TO_SEE(1),

    @SerializedName("2")
    FOR_KIDS(2),

    @SerializedName("3")
    FOOD(3),

    @SerializedName("4")
    CAFE(4),

    @SerializedName("5")
    FUN(5),

    @SerializedName("6")
    SLEEP(6),

    @SerializedName("7")
    HEALTH(7);

    ECategory(int id) {
        this.id = id;
    }

    public final int id;


    public static ECategory parse(int id) {
        for (ECategory t : ECategory.values()) {
            if (t.id == id) {
                return t;
            }
        }
        Log.w("ECategory", "Unknown id for parsing: " + id);
        return null;
    }
}
