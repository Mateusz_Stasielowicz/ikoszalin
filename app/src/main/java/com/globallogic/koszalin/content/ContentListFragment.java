package com.globallogic.koszalin.content;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.globallogic.koszalin.AppUtils;
import com.globallogic.koszalin.R;
import com.globallogic.koszalin.base.AbstractFragment;
import com.globallogic.koszalin.content.data.ContentListItem;
import com.globallogic.koszalin.content.data.ContentListItemComparator2;
import com.globallogic.koszalin.content.data.EContentCategory;
import com.globallogic.koszalin.content.data.EContentFilterType;
import com.globallogic.koszalin.content.data.IContentFragment;
import com.globallogic.koszalin.shared.view.EmptyRecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * ContentFragment
 * <hr /> Created by damian.giedrys on 2016-05-13.
 */
public class ContentListFragment extends AbstractFragment implements AdapterView.OnItemClickListener,
        IContentFragment, SwipeRefreshLayout.OnRefreshListener {

    static final String TAG = ContentListFragment.class.getSimpleName();

    // ui
    private ContentAdapter2 adapter;
    private EmptyRecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    // vars
    private EContentCategory selectedCategory;
//    private ContentListItemSortedList dataset;
    private List<ContentListItem> dataset;
    private EContentFilterType filterType;
    private boolean showRefreshOnStart = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        dataset = new ContentListItemSortedList();
        dataset = new ArrayList<>();
        Bundle bundle = getArguments();
        if (bundle != null) {
            selectedCategory = EContentCategory.getById(bundle.getInt(ContentFragment.BUNDLE_KEY_CATID));
            Log.d(TAG, "Got contentCat from bundle: " + selectedCategory);
            filterType = EContentFilterType.DEFAULT;
//            filterType = EContentFilterType.parse(savedInstanceState.getInt(ContentFragment.BUNDLE_FILTER_TYPE, EContentFilterType.DEFAULT.id));
            Log.d(TAG, "Got filterType from bundle: " + filterType);

//            CustomSortedList<ContentListItem> items = (CustomSortedList<ContentListItem>)bundle.getSerializable(ContentFragment.BUNDLE_KEY_ITEMS);
            ArrayList<ContentListItem> items = (ArrayList<ContentListItem>)bundle.getSerializable(ContentFragment.BUNDLE_KEY_ITEMS);

            populateDataSet(items, false);
            showRefreshOnStart = (items == null && savedInstanceState == null);

            Log.d(TAG, "Got items from bundle: " + dataset);
        } else {
            Log.w(TAG, "Empty initial arguments");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_content_list,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (EmptyRecyclerView) view.findViewById(R.id.content_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
//        recyclerView.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.view_empty_recyclerview, null, false));
        recyclerView.setEmptyView(view.findViewById(R.id.view_emptyrecyclerview));

        adapter = new ContentAdapter2(dataset, getContext(),this);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        swipeRefreshLayout = initView(view, R.id.content_swipe_refresh_view);
        swipeRefreshLayout.setColorSchemeResources(AppUtils.COLORS_FOR_PROGRESSBAR);
        swipeRefreshLayout.setOnRefreshListener(this);

        adapter.notifyDataSetChanged();

        if (showRefreshOnStart) {

            /**
             * Showing Swipe Refresh animation on activity create
             * As animation won't start on onCreate, post runnable is used
             */
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            });

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ContentListItem item = dataset.get(position);
        Log.i(TAG, "Clicked: position=" + position + ", id=" + id + ", item=" + item);

        Log.i(TAG, "Starting ContentActivity...");
        Intent intent = new Intent(getActivity(), ContentActivity.class);
        intent.putExtra(ContentActivity.BUNDLE_KEY_ITEM, ContentDatabase.INSTANCE.getContentItem(item.id));
        startActivity(intent);
    }

    @Override
    public void setModelRefreshing(boolean value) {
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(value);
    }

    @Override
    public void onCategorySwitch(EContentCategory cat, List<ContentListItem> items) {
        Log.d(TAG, "onCategorySwitch, contentCat:" + cat + ", items:" + (items == null ? "NULL" : items.size()));
        this.selectedCategory = cat;

        populateDataSet(items, true);
    }

    public void onFilterChange(EContentFilterType type) {
        this.filterType = type;
        doFilter(true);
    }

    private void populateDataSet(List<ContentListItem> newItems, boolean notify) {
        dataset.clear();
        if (newItems != null) {
            dataset.addAll(newItems);
            doFilter(false);
        }

        if (notify) {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            } else {
                Log.w(TAG, "adapter is null!!");
            }
        }
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
    }

    private void doFilter(boolean notify) {
        Log.d(TAG, "doFilter, filterType: " + filterType);

        if (selectedCategory != EContentCategory.THINGS_TODO) {
            Collections.sort(dataset, ContentListItemComparator2.getInstance(filterType));
            if (notify && adapter != null) {
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                } else {
                    Log.w(TAG, "adapter is null!!");
                }
            }
        }
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        Fragment parent = getParentFragment();
        if (parent != null && parent instanceof ContentFragment) {
            ((ContentFragment)parent).updateItems(true);
        } else {
            Log.w(TAG, "Empty parent");
            swipeRefreshLayout.setRefreshing(false);
        }
    }

}
