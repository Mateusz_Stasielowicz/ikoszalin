package com.globallogic.koszalin.content.data;

import com.globallogic.koszalin.AppUtils;
import com.globallogic.koszalin.shared.Utils;

import java.io.Serializable;
import java.util.Date;

/**
 * ContentItem for lists/recycle view
 * <hr /> Created by damian.giedrys on 2016-05-19.
 */
public class ContentListItem implements Serializable {

    public final int id;
    public final int type;
    public final int contentCat;
    public final String name;
    //    public final int type;
    public final String photoUrl;
    public final double lat;
    public final double lng;
    public final String desc_short;

    public double distance = Double.MIN_VALUE;
    public Date dateFrom;
    public Date dateTo;


    private ContentListItem(int id, int type, int cat, String name, String photoUrl, double lat, double lng, String shortDesc) {
        this.id = id;
        this.type = type;
        this.contentCat = cat;
        this.name = name;
        this.photoUrl = photoUrl;
        this.lat = lat;
        this.lng = lng;
        this.desc_short = shortDesc;
    }

    @Override
    public String toString() {
        return "ContentListItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", contentCat=" + contentCat +
                ", lat=" + lat +
                ", lng=" + lng +
                ", dist=" + (distance == Double.MIN_VALUE ? "-" : distance) +
                ", dateFrom=" + dateFrom +
                '}';
    }

    public boolean hasLocation() {
        return lat != Double.MIN_VALUE && lng != Double.MIN_VALUE;
    }

    public static ContentListItem fromItem(ContentItem item, EContentCategory contCat, double[] myLoc) {
        if (item == null)
            return null;

        double lat = Double.MIN_VALUE;
        double lng = Double.MIN_VALUE;
        if (item.hasLocalisation()) {
            lat = item.getAddress().localization.lat;
            lng = item.getAddress().localization.lng;
        }

        int iContCat = contCat.id;
        if (contCat == EContentCategory.THINGS_TODO) {
            EContentCategory[] ccs = EContentCategory.getCatForItem(item);
            if (ccs != null) {
                for (EContentCategory cc : ccs) {
                    if (cc != null) {
                        iContCat = cc.id;
                        break;
                    }
                }

            }
        }

        ContentListItem ret = new ContentListItem(item.getId(), item.getType().id, iContCat,
                item.getName(), item.getFirstPhoto(), lat, lng, item.getDescriptionShort());

//        Log.d("TEST", "item: "+item);
//        Log.d("TEST", "item.getDistance(): "+item.getCountedData());
//        Log.d("TEST", "item.hasLocalisation(): "+item.hasLocalisation());
//        Log.d("TEST", "AppUtils.isValidLocation(myLoc): "+AppUtils.isValidLocation(myLoc));
//        Log.d("TEST", "myLoc: "+ Arrays.toString(myLoc));
//        if (myLoc != null)
//        Log.d("TEST", ""+AppUtils.distance(myLoc[0], myLoc[1], lat, lng));

        if (item.getType() == EContentType.EVENT) {
            ret.dateFrom = item.getDatetimeFrom();
            ret.dateTo = item.getDatetimeTo();
        }

        if (item.getCountedData() != null && item.getCountedData().distance != Double.MIN_VALUE) {
            ret.distance = Utils.round(item.getCountedData().distance / 1000, 3);
        } else if (item.hasLocalisation() && AppUtils.isValidLocation(myLoc)) {
            ret.distance = AppUtils.distance(myLoc[0], myLoc[1], lat, lng);
        }

        return ret;
    }

    public static void maybeUpdateDistane(ContentListItem item, double[] myLoc) {
        if (item != null && item.hasLocation() && AppUtils.isValidLocation(myLoc)) {
            item.distance = AppUtils.distance(myLoc[0], myLoc[1], item.lat, item.lng);
        } else {
            item.distance = Double.MIN_VALUE;
        }
    }
}

