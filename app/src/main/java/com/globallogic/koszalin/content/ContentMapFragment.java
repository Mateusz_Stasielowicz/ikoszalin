package com.globallogic.koszalin.content;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.globallogic.koszalin.AppUtils;
import com.globallogic.koszalin.MyLocationManager;
import com.globallogic.koszalin.R;
import com.globallogic.koszalin.base.AbstractFragment;
import com.globallogic.koszalin.content.data.ContentListItem;
import com.globallogic.koszalin.content.data.ContentListItemSortedList;
import com.globallogic.koszalin.content.data.EContentCategory;
import com.globallogic.koszalin.content.data.IContentFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import android.support.v4.app.Fragment;

/**
 * ContentMapFragment
 * <hr /> Created by damian.giedrys on 2016-05-16.
 */
public class ContentMapFragment extends AbstractFragment implements OnMapReadyCallback, IContentFragment,
        LocationSource.OnLocationChangedListener,SwipeRefreshLayout.OnRefreshListener  {

    static final String TAG = ContentMapFragment.class.getSimpleName();

    // ui
    private SupportMapFragment mapFragment;
    private View rootView;
    private SwipeRefreshLayout swipeRefreshLayout;

    // vars
    private GoogleMap map;
    private EContentCategory selectedCategory;
    private ContentListItemSortedList dataset;
//    private double[] myLocation;

    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;
    private Map<Integer, Marker> markerMap;
    private Map<Marker, Integer> markers;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        Log.d(TAG, "onCreate, savedInstanceState:" + savedInstanceState);

        dataset = new ContentListItemSortedList();
        pagerAdapter = new ScreenSlidePagerAdapter(getChildFragmentManager());
        Bundle bundle = getArguments();
        if (bundle != null) {
//            model = (PlaceItem[])bundle.getParcelableArray(PlacesActivity.BUNDLE_KEY_PLACES);
//            Log.i(TAG, "Got parcel: " + Utils.toString(model));

            selectedCategory = EContentCategory.getById(bundle.getInt(ContentFragment.BUNDLE_KEY_CATID));

//            CustomSortedList<ContentListItem> items = (CustomSortedList<ContentListItem>)bundle.getSerializable(ContentFragment.BUNDLE_KEY_ITEMS);
            ArrayList<ContentListItem> items = (ArrayList<ContentListItem>) bundle.getSerializable(ContentFragment.BUNDLE_KEY_ITEMS);
//            if (items != null) {
//                for (int i = 0 ; i< items.size() ; i++) {
//                    dataset.add(items.get(i));
//                }
//            }
            if (items != null)
                dataset.addAll(items);

        } else {
            Log.w(TAG, "Empty initial arguments");
        }


        super.onCreate(savedInstanceState);
    }

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            updateMarkerMessageWindow(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void updateMarkerMessageWindow(int position) {
        if (dataset != null && map != null && viewPager != null && dataset.size() > position) {
            Marker marker = markerMap.get(dataset.get(position).id);
            if (marker != null) {
                map.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
                marker.showInfoWindow();
            } else {
                Log.w(TAG, "Empty marker for pos: " + position);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.i(TAG, "onCreateView, savedInstanceState=" + savedInstanceState);

        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }

        try {
            rootView = inflater.inflate(R.layout.fragment_content_map, container, false);
        } catch (InflateException e) {
            /* map is already there, just return view as it is */
        }

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        } else {
            Log.i(TAG, "mapFragment is empty");
        }
//        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
//        SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
        viewPager = (ViewPager) rootView.findViewById(R.id.map_viewpager);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(onPageChangeListener);
        viewPager.setClipToPadding(false);
        viewPager.setPadding(50, 0, 50, 0);
        viewPager.setPageMargin(20);

        swipeRefreshLayout = initView(rootView, R.id.content_swipe_refresh_view);
        swipeRefreshLayout.setColorSchemeResources(AppUtils.COLORS_FOR_PROGRESSBAR);
        swipeRefreshLayout.setOnRefreshListener(this);
//        swipeRefreshLayout.requestDisallowInterceptTouchEvent(true);
//        swipeRefreshLayout.setNestedScrollingEnabled(false);
        swipeRefreshLayout.setEnabled(false);
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.map = googleMap;
        showObjects();
    }

    @Override
    public void setModelRefreshing(boolean value) {
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(value);
    }

    @Override
    public void onCategorySwitch(EContentCategory cat, List<ContentListItem> items) {
        Log.d(TAG, "onCategorySwitch, contentCat:" + cat + ", items:" + (items == null ? "NULL" : items.size()));

        this.selectedCategory = cat;
        this.dataset.clear();
        if (items != null) {
//            for (int i = 0 ; i< items.size() ; i++) {
//                dataset.add(items.get(i));
//            }
            dataset.addAll(items);
        }
        pagerAdapter = new ScreenSlidePagerAdapter(getChildFragmentManager());
        showObjects();
        if (viewPager != null) {
            viewPager.setAdapter(pagerAdapter);
            if (dataset != null && dataset.size() > 0) {
                updateMarkerMessageWindow(0);
            }
        }

        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
    }



    private void showObjects() {
        if (map != null) { // TODO spr. po wejsciu z innej activity (null)
            map.clear(); // TODO
            markers = new HashMap<>();
            markerMap = new HashMap<>();
            map.setOnMarkerClickListener(onMarkerClickListener);
            if (dataset != null) {
                for (int i = 0; i < dataset.size(); i++) {
                    ContentListItem ci = dataset.get(i);
                    if (ci != null) {
                        MarkerOptions mo = AppUtils.asMarker(ci);

                        if (mo != null) {
                            BitmapDescriptor bd = ContentCategoryMapBitmaps.getMarkerBitmap(getContext(),
                                    EContentCategory.getById(ci.contentCat));
                            if (bd != null) {
                                mo.icon(bd);
                            }
                        }

                        Log.i("TEST", "map: " + map);
                        Log.i("TEST", "mo: " + mo);

                        if (mo != null) {
                            Marker marker = map.addMarker(mo);
                            if (marker != null) {
                                markers.put(marker, i);
                                markerMap.put(ci.id, marker);
                            }
                        } else {
                            Log.w(TAG, "Empty marker for item: " + ci);
                        }
                    }
                }
            }

            double[] myLocation = MyLocationManager.INSTANCE.getLastLocation();
            if (AppUtils.isValidLocation(myLocation)) {

                MarkerOptions myMarker = new MarkerOptions()
                        .title(getString(R.string.my_location));
                myMarker.position(new LatLng(myLocation[0], myLocation[1]));

                BitmapDescriptor ret = AppUtils.createBitmapDescriptor(getContext(), R.drawable.marker_mylocation);
                if (ret != null) {
                    myMarker.icon(ret);
                }

                map.addMarker(myMarker);
            }
            updateMarkerMessageWindow(0);
        }
    }

    private GoogleMap.OnMarkerClickListener onMarkerClickListener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker) {
            if (markers != null && viewPager != null) {
                Integer index = markers.get(marker);
                if(index != null) {
                    viewPager.setCurrentItem(index, true);
                }
            }
            return false;
        }
    };

    @Override
    public void onLocationChanged(Location location) {
        showObjects();
    }


    // TODO FragmentStatePagerAdapter powoduje
    // java.lang.IllegalStateException: Fragment no longer exists for key f0: index 3
    // podczas szybkiego przełączania widoków
    //private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
    private class ScreenSlidePagerAdapter extends FragmentPagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (dataset != null && dataset.size() > position) {
                return ContentMapSlidePageFragment.newInstance(dataset.get(position));
            }
            return null;
        }

        @Override
        public int getCount() {
            if (dataset != null) {
                return dataset.size();
            }
            return 0;
        }
    }

    @Override
    public void onRefresh() {
        Log.d(TAG, "onRefresh()");
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(true);
    }
}
