package com.globallogic.koszalin.content;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.globallogic.koszalin.AppUtils;
import com.globallogic.koszalin.R;
import com.globallogic.koszalin.shared.Utils;

/**
 * ImageDetailFragment
 * <hr /> Created by damian.giedrys on 2016-05-31.
 */
public class ImageDetailFragment extends Fragment {

    private static final String IMAGE_DATA_EXTRA = "resUrl";
    private String mImageUrl;
    private ImageView mImageView;

    static ImageDetailFragment newInstance(String imageUrl) {
        final ImageDetailFragment f = new ImageDetailFragment();
        final Bundle args = new Bundle();
        args.putString(IMAGE_DATA_EXTRA, imageUrl);
        f.setArguments(args);
        return f;
    }

    // Empty constructor, required as per Fragment docs
    public ImageDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageUrl = getArguments() != null ? getArguments().getString(IMAGE_DATA_EXTRA) : null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // image_detail_fragment.xml contains just an ImageView
        final View v = inflater.inflate(R.layout.fragment_image_details, container, false);
        mImageView = (ImageView) v.findViewById(R.id.imageView);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        final int resId = ImageDetailActivity.imageResIds[mImageUrl];
//        mImageView.setImageResource(resId); // Load image into ImageView

        if (!Utils.isNullOrEmpty(mImageUrl)) {
            AppUtils.loadImage(getContext(), mImageUrl, mImageView);
        }
    }


}
