package com.globallogic.koszalin.content.data;

/**
 * EContentFilterType
 * <hr /> Created by damian.giedrys on 2016-06-03.
 */
public enum EContentFilterType {

    DEFAULT(0),
    BY_NAME(1),
    BY_DISTANCE(2),
    BY_POPULARITY(3),
    BY_TIME(4);

    public final int id;

    EContentFilterType (int id) {
        this.id = id;
    }

    public static EContentFilterType parse(int id) {
        for (EContentFilterType value : EContentFilterType.values()) {
            if (id == value.id) {
                return value;
            }
        }
        return EContentFilterType.DEFAULT;
    }

}
