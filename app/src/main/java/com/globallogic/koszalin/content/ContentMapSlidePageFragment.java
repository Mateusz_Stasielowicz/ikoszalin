package com.globallogic.koszalin.content;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.globallogic.koszalin.AppUtils;
import com.globallogic.koszalin.R;
import com.globallogic.koszalin.content.data.ContentListItem;
import com.globallogic.koszalin.content.data.EContentCategory;
import com.globallogic.koszalin.shared.Utils;

/**
 * Created by mateusz.stasielowicz on 2016-05-24.
 */
public class ContentMapSlidePageFragment extends Fragment {

    private static final String KEY_CONTENT_LIST_ITEM = "KEY_CONTENT_LIST_ITEM";

    private TextView txtDescription;

    private ContentListItem item;



    public static ContentMapSlidePageFragment newInstance(ContentListItem item) {

        Bundle args = new Bundle();
        args.putSerializable(KEY_CONTENT_LIST_ITEM,item);
        ContentMapSlidePageFragment fragment = new ContentMapSlidePageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            if(getArguments().containsKey(KEY_CONTENT_LIST_ITEM)){
                this.item = (ContentListItem) getArguments().getSerializable(KEY_CONTENT_LIST_ITEM);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map_slide_page,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ContentActivity.class);
                intent.putExtra(ContentActivity.BUNDLE_KEY_ITEM, ContentDatabase.INSTANCE.getContentItem(item.id));
                startActivity(intent);
            }
        });

        TextView title = (TextView) view.findViewById(R.id.map_viewpager_title);
        ImageView image = (ImageView) view.findViewById(R.id.map_viewpager_image);
        TextView content = (TextView) view.findViewById(R.id.map_viewpager_category);
        TextView txtDesc = (TextView) view.findViewById(R.id.map_viewpager_desc);

        title.setText(item.name);
        EContentCategory cc = EContentCategory.getById(item.contentCat);
        if (cc != null && cc.titleRes > 0) {
            content.setText(cc.titleRes);
        } else {
            content.setText("");
        }
        txtDesc.setText(Utils.isNullOrEmpty(item.desc_short) ? "" : item.desc_short);

        Log.d("TEST", item + "item.desc_short: " + item.desc_short);

        if (Utils.isNullOrEmpty(item.photoUrl)) {
            image.setVisibility(View.GONE);
        } else {
            image.setVisibility(View.VISIBLE);
            AppUtils.loadImage(getContext(), item.photoUrl, image);
        }



    }
}
