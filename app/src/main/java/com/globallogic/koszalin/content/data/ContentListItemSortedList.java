package com.globallogic.koszalin.content.data;

import android.util.Log;

import com.globallogic.koszalin.shared.view.CustomSortedList;

/**
 * ContentListItemSortedList
 * <hr /> Created by damian.giedrys on 2016-05-23.
 */
public class ContentListItemSortedList extends CustomSortedList<ContentListItem> {

    public ContentListItemSortedList() {
        super(ContentListItem.class, ContentListItemSortedListCallback);
    }

    private static final Callback ContentListItemSortedListCallback = new Callback() {
        @Override
        public int compare(Object o1, Object o2) {
            return ContentListItemComparator.getInstance().compare((ContentListItem)o1, (ContentListItem)o2);
        }

        @Override
        public void onInserted(int position, int count) {

        }

        @Override
        public void onRemoved(int position, int count) {

        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {

        }

        @Override
        public void onChanged(int position, int count) {

        }

        @Override
        public boolean areContentsTheSame(Object oldItem, Object newItem) {
            return false;
        }

        @Override
        public boolean areItemsTheSame(Object item1, Object item2) {
            return false;
        }
    };
}
