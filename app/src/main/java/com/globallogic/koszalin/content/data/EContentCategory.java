package com.globallogic.koszalin.content.data;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.util.Log;

import com.globallogic.koszalin.R;
import com.google.gson.annotations.SerializedName;

import static com.globallogic.koszalin.content.data.EContentType.*;

/**
 * EContentCategory
 * <hr /> Created by damian.giedrys on 2016-05-13.
 */
public enum EContentCategory { // EMenuCatergory

    // From category's
    TO_SEE(ECategory.TO_SEE.id, 3, R.drawable.ic_menu_tosee, R.drawable.cat_marker_tosee, R.string.content_cat_tosee),

    FOR_KIDS(ECategory.FOR_KIDS.id, 4, R.drawable.ic_menu_childfriendly, R.drawable.cat_marker_forkids, R.string.content_cat_forkids),

    FOOD(ECategory.FOOD.id, 5, R.drawable.ic_menu_food, R.drawable.cat_marker_food, R.string.content_cat_food),

    CAFE(ECategory.CAFE.id, 6, R.drawable.ic_menu_cafes, R.drawable.cat_marker_cafe, R.string.content_cat_cafe),

    FUN(ECategory.FUN.id, 7, R.drawable.ic_menu_fun, R.drawable.cat_marker_fun, R.string.content_cat_fun),

    SLEEP(ECategory.SLEEP.id, 8, R.drawable.ic_menu_accommodations, R.drawable.cat_marker_sleep, R.string.content_cat_sleep),

    HEALTH(ECategory.HEALTH.id, 9, R.drawable.ic_menu_health, R.drawable.cat_marker_health, R.string.content_cat_health),


    THINGS_TODO(128, 1, R.drawable.ic_menu_thingstodo, R.drawable.cat_marker_thingstodo, R.string.content_cat_things_todo, true),

    EVENTS(129, 2, R.drawable.ic_menu_events, R.drawable.cat_marker_events, R.string.content_cat_events, true),

    OUTSIDE(130, 10, R.drawable.ic_menu_gallery, R.drawable.cat_marker_outside, R.string.content_cat_outside, true),

    UNKNOWN(256, -1, R.drawable.ic_menu_thingstodo, R.drawable.cat_marker_thingstodo, R.string.UNKNOWN, true);

    EContentCategory(int id, int order, @DrawableRes int iconResMenu,
                     @DrawableRes int iconResMapMarker, @StringRes int titleRes) {
        this(id, order, iconResMenu, iconResMapMarker, titleRes, false);
    }

    EContentCategory(int id, int order, @DrawableRes int iconResMenu,
                     @DrawableRes int iconResMapMarker, @StringRes int titleRes, boolean virtual) {
        this.order = order;
        this.id = id;
        this.iconResMenu = iconResMenu;
        this.iconResMapMarker = iconResMapMarker;
        this.titleRes = titleRes;
        this.virtual = virtual;
    }

    public final int id;
    public final int iconResMenu;
    public final int iconResMapMarker;
    public final int titleRes;
    public final int order;
    public final boolean virtual;

    public static EContentCategory getById(int id) {
        for (EContentCategory cat : EContentCategory.values()) {
            if (cat.id == id) {
                return cat;
            }
        }
        Log.w("EContentCategory", "Unknown id for parsing: " + id);
        return null;
    }

    // nie powinien zwracac THINGS_TODO
    public static EContentCategory[] getCatForItem(ContentItem ci) {
        if (ci != null) {

            switch (ci.getType()) {
                case EVENT:
                    return new EContentCategory[]{EContentCategory.EVENTS};
                default: // PLACES
                    if (ci.getCategory() == null && ci.getCategory2() == null) {
                        // jak serwer zwrócił item bez kategorii
                        Log.w("EContentCategory", "Unknown category for item: " + ci);
                        return new EContentCategory[]{EContentCategory.UNKNOWN};
                    }
                    EContentCategory ret1 = ci.getCategory() != null ? getById(ci.getCategory().id) : null;
                    EContentCategory ret2 = ci.getCategory2() != null ? getById(ci.getCategory2().id) : null;
                    EContentCategory ret3 = ci.isOutdoor() ? EContentCategory.OUTSIDE : null;
                    return new EContentCategory[] {ret1, ret2, ret3};
            }
        }
        return null;
    }

    public static boolean haveCategory(ContentItem ci, EContentCategory cc) {
        if (ci == null)
            return false;
        EContentCategory[] ccs = getCatForItem(ci);
        if (ccs != null) {
            for (EContentCategory c : ccs) {
                if (c != null && c == cc) {
                    return true;
                }
            }
        }
        return false;
    }

}
