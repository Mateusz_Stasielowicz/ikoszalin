package com.globallogic.koszalin;

import android.support.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * DateUtils
 * <hr /> Created by damian.giedrys on 2016-06-03.
 */
public class DateUtils {

    public static final SimpleDateFormat DF_EVENT_FROM_TO = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static final SimpleDateFormat DF_YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat DF_HH_mm = new SimpleDateFormat("HH:mm");

    public static boolean isSameDay(@NonNull Date d1, @NonNull Date d2) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTime(d1);
        c2.setTime(d2);
        return c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) &&
                c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR);
    }

    public static boolean isTomorrow(@NonNull Date d1, @NonNull Date d2) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTime(d1);
        c2.setTime(d2);
        return c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) &&
                (c1.get(Calendar.DAY_OF_YEAR) + 1) == c2.get(Calendar.DAY_OF_YEAR);
    }

    public static String formatEventDate(@NonNull Date date) {
        String ret = DF_EVENT_FROM_TO.format(date);
        if (ret.endsWith(" 00:00")) {
            ret = DF_YYYY_MM_DD.format(date);
        }
        return ret;
    }
}
