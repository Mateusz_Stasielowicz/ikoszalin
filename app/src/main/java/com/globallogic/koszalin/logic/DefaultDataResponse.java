package com.globallogic.koszalin.logic;

import android.view.View;
import android.widget.ProgressBar;

import com.globallogic.koszalin.R;

/**
 * DefaultDataResponse
 * <hr /> Created by damian.giedrys on 2016-05-19.
 */
public abstract class DefaultDataResponse <T> implements DataResponse<T> {

    private ProgressBar progressBar;

    public DefaultDataResponse() {
        this (null);
    }

    public DefaultDataResponse(View rootView) {
        if (rootView != null) {
            progressBar = (ProgressBar)rootView.findViewById(R.id.progressBar);
            if (progressBar != null) {
                progressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    public DefaultDataResponse(ProgressBar progressBar) {
        this.progressBar = progressBar;
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSuccess(T response) {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
        onSuccessImpl(response);
    }

    @Override
    public void onError(int errorCode) {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
        onErrorImpl(errorCode);
    }

    public abstract void onSuccessImpl(T response) ;

    public abstract void onErrorImpl(int errorCode) ;
}
