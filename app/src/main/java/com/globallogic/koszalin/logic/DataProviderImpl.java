package com.globallogic.koszalin.logic;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;

import com.globallogic.koszalin.MyLocationManager;
import com.globallogic.koszalin.content.data.ContentItem;
import com.globallogic.koszalin.content.data.ECategory;
import com.globallogic.koszalin.content.data.EContentCategory;


import java.util.List;

/**
 * DataProviderImpl
 * <hr /> Created by damian.giedrys on 2016-05-19.
 */
public enum DataProviderImpl implements IDataProvider {

    INSTANCE;

    private IDataProvider dataProvider;

    DataProviderImpl() {
        dataProvider = new GoogleSheetDataProvider();
//        dataProvider = new TestDataProvider();

    }

//    @Override
//    public void getData(@NonNull Context ctx, @NonNull DataResponse<List<ContentItem>> callback) {
//        dataProvider.getData(ctx, callback);
//    }
//    @Override
//    public void getData(@NonNull Context ctx, EContentCategory contentCat, @NonNull DataResponse<List<ContentItem>> callback) {
//        dataProvider.getData(ctx, contentCat, callback);
//    }

    @Override
    public void getCategoryItems(@NonNull Context ctx, ECategory cat, @NonNull DataResponse<List<ContentItem>> callback) {
        dataProvider.getCategoryItems(ctx, cat, callback);
    }

    @Override
    public void getThingsToDo(@NonNull Context ctx, @NonNull DataResponse<List<ContentItem>> callback) {
        dataProvider.getThingsToDo(ctx, callback);
    }

    @Override
    public void getEvents(@NonNull Context ctx, @NonNull DataResponse<List<ContentItem>> callback) {
        dataProvider.getEvents(ctx, callback);
    }

    @Override
    public void getOutdoors(@NonNull Context ctx, @NonNull DataResponse<List<ContentItem>> callback) {
        dataProvider.getOutdoors(ctx, callback);
    }

    @Override
    public void getRecords(@NonNull Context ctx, int[] ids, @NonNull DataResponse<List<ContentItem>> callback) {
        dataProvider.getRecords(ctx, ids, callback);
    }

}
