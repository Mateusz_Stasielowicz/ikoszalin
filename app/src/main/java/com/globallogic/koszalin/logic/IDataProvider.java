package com.globallogic.koszalin.logic;

import android.content.Context;
import android.support.annotation.NonNull;

import com.globallogic.koszalin.content.data.ContentItem;
import com.globallogic.koszalin.content.data.ECategory;
import com.globallogic.koszalin.content.data.EContentCategory;
import com.google.android.gms.maps.LocationSource;

import java.io.Serializable;
import java.util.List;

/**
 * IDataProvider
 * <hr /> Created by damian.giedrys on 2016-05-19.
 */
public interface IDataProvider {

    void getCategoryItems(@NonNull Context ctx, ECategory cat, @NonNull DataResponse<List<ContentItem>> callback);

    void getThingsToDo(@NonNull Context ctx, @NonNull DataResponse<List<ContentItem>> callback);

    void getEvents(@NonNull Context ctx, @NonNull DataResponse<List<ContentItem>> callback);

    void getOutdoors(@NonNull Context ctx, @NonNull DataResponse<List<ContentItem>> callback);

    void getRecords(@NonNull Context ctx, int[] ids, @NonNull DataResponse<List<ContentItem>> callback);

    // getCategories

}
