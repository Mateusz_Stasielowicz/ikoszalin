package com.globallogic.koszalin.logic;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.globallogic.koszalin.AppUtils;
import com.globallogic.koszalin.content.data.ContentItem;
import com.globallogic.koszalin.content.data.ECategory;
import com.globallogic.koszalin.content.data.EContentType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * TestDataProvider
 * <hr /> Created by damian.giedrys on 2016-05-19.
 */
public class TestDataProvider implements IDataProvider {

    private static final long LAG_TIME = 2000;

    @Override
    public void getCategoryItems(@NonNull Context ctx, final ECategory cat,
                                 @NonNull final DataResponse<List<ContentItem>> callback) {
        doLag(new Runnable() {
            @Override
            public void run() {
                List<ContentItem> list = new ArrayList<>();
                // populate
                for (int i = 0; i < 20; i++) {
                    list.add(createStub(i, cat));
                }
                callback.onSuccess(list);
            }
        });
    }

    @Override
    public void getThingsToDo(@NonNull Context ctx, @NonNull final DataResponse<List<ContentItem>> callback) {
        doLag(new Runnable() {
            @Override
            public void run() {
                List<ContentItem> list = new ArrayList<>();
                // populate
                for (int i = 0; i < 25; i++) {
                    list.add(createStub(i));
                }
                callback.onSuccess(list);
            }
        });
    }

    @Override
    public void getEvents(@NonNull Context ctx, final @NonNull DataResponse<List<ContentItem>> callback) {
        doLag(new Runnable() {
            @Override
            public void run() {
                List<ContentItem> list = new ArrayList<>();
                // populate
                for (int i = 0; i < 20; i++) {
                    list.add(createStub(i, null, EContentType.EVENT));
                }
                callback.onSuccess(list);
            }
        });
    }

    @Override
    public void getOutdoors(@NonNull Context ctx, final @NonNull DataResponse<List<ContentItem>> callback) {
        doLag(new Runnable() {
            @Override
            public void run() {
                List<ContentItem> list = new ArrayList<>();
                // populate
                for (int i = 0; i < 20; i++) {
                    EContentType t = createRandomType();
                    if (t == EContentType.EVENT) {
                        list.add(createStub(i, null, EContentType.EVENT, true));
                    } else if (t == EContentType.PLACE) {
                        list.add(createStub(i, createRandomCategory(), EContentType.PLACE, true));
                    }
                }
                callback.onSuccess(list);
            }
        });
    }

    @Override
    public void getRecords(@NonNull Context ctx, int[] ids, final @NonNull DataResponse<List<ContentItem>> callback) {
        throw new IllegalStateException("Not supported yet");
    }

    private void doLag(Runnable runner) {
        Handler h = new Handler();
        h.postDelayed(runner, LAG_TIME);
    }


    private static ContentItem createStub(int no) {
        return createStub(no, createRandomCategory());
    }

    private static ContentItem createStub(int no, ECategory c) {
        return createStub(no, c, createRandomType());
    }

    private static ContentItem createStub(int no, ECategory c, EContentType t) {
        Random r = new Random();
        return createStub(no, c, t, r.nextBoolean());
    }

    private static ContentItem createStub(int no, ECategory c, EContentType t, boolean outdoors) {

        Random r = new Random();

//        ContentItem ret = new ContentItem(AppUtils.DEV_getImageresStub(), "Miejsce jakieś dłuższe " + c.ordinal() + "," + no,
//                "Category" + no, "Lorem ipsum dolor sit amet, viris iudico timeam vix ea, cu mei commodo albucius. Dicant efficiendi no usu, ei est sapientem gubergren, iusto paulo scripserit eum et. Est munere fabulas fastidii no. Vis malis fierent ne, at probo aliquando vim. Ex cum placerat tractatos iracundia, est in ubique interesset.",
//                "A short description of a place", "+48 111 222 " + String.format("%03d", no),
//                "Address can be a long string..., yes indeed " + no, lanLon[0], lanLon[1], "http://www.google.com");

        ContentItem ret = new ContentItem();

        ret.setMail("mail@example.com");

        ContentItem.Address adr = new ContentItem.Address();
        ContentItem.Localization loc = new ContentItem.Localization();
        double[] lanLon = AppUtils.DEV_getLocationStub();
        loc.lat = lanLon[0];
        loc.lng = lanLon[1];
//        adr.localization = "54.195102, 16.195999";
        adr.localization = loc;
        adr.number = "66";
        adr.postCode = "75-449";
        adr.street = "Partyzantów";
        ret.setAddress(adr);
        ret.setCategory(c);

        Date now = new Date();
        Date d1 = new Date(now.getTime() + (1 * 60 * 60 * 1000));
        Date d2 = new Date(now.getTime() + (2 * 60 * 60 * 1000));

        ret.setDatetimeFrom(d1);
        ret.setDatetimeFrom(d2);
        ret.setDescriptionLong("Lorem ipsum dolor sit amet, viris iudico timeam vix ea, cu mei commodo albucius. Dicant efficiendi no usu, ei est sapientem gubergren, iusto paulo scripserit eum et. Est munere fabulas fastidii no. Vis malis fierent ne, at probo aliquando vim. Ex cum placerat tractatos iracundia, est in ubique interesset.");
        ret.setDescriptionShort("Lorem ipsum dolor sit amet, viris iudico timeam vix ea");
        ret.setId(no);
//        ret.setPhoto("http://www.koszalin.pl/sites/default/files/styles/media_gallery_large/public/gallery_picture.2011-04-13.02006292_big.jpeg");
        ret.setPhoto(AppUtils.DEV_getImageUrlStubM());
        if (t == EContentType.EVENT) {
            ret.setName("Wydarzenie jakieś dłuższe" + no);
        } else {
            ret.setName("Miejsce jakieś dłuższe " + c.ordinal() + "," + no);
        }

        ret.setTelephone("+48 666 666 666");
        ret.setTelephone2("+48 666 666 666");
        ret.setType(t);

//        ret.setType(EContentType.EVENT);
        ret.setWww("http://www.google.com");

//        ret.setDistance(r.nextFloat() * 10);

        String[] op = new String[7];
        op[0] = "11:00 - 18:00";
        op[1] = "11:00 - 18:00";
        op[2] = "11:00 - 18:00";
        op[3] = "11:00 - 18:00";
        op[4] = "11:00 - 18:00";
        op[5] = "12:00 - 22:00";
        op[6] = null;
        ret.setOpeningHours(op);

        ret.setMail("test@test.com");

        ret.setOutdoor(outdoors);

        return ret;
    }

    private static ECategory createRandomCategory() {
        Random r = new Random();
        return ECategory.parse(r.nextInt(ECategory.values().length) + 1);
    }

    private static EContentType createRandomType() {
        Random r = new Random();
        return EContentType.parse(r.nextInt(EContentType.values().length) + 1);
    }

}
