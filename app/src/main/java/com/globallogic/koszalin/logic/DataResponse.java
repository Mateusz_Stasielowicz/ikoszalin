package com.globallogic.koszalin.logic;

/**
 * DataResponse
 * <hr /> Created by damian.giedrys on 2016-05-19.
 */
public interface DataResponse <T> {

    void onSuccess(T response);

    void onError(int errorCode);

}
