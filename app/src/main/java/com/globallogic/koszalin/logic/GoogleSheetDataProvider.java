package com.globallogic.koszalin.logic;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;
import com.globallogic.googlesheetlib.query.CustomErrorCodes;
import com.globallogic.googlesheetlib.query.Query;
import com.globallogic.googlesheetlib.query.request.ResponseListener;
import com.globallogic.googlesheetlib.query.response.SheetResponse;
import com.globallogic.koszalin.AppUtils;
import com.globallogic.koszalin.MyLocationManager;
import com.globallogic.koszalin.content.data.ContentItem;
import com.globallogic.koszalin.content.data.ECategory;
import com.globallogic.koszalin.content.data.EContentCategory;
import com.google.android.gms.maps.LocationSource;

import java.util.List;

/**
 * GoogleSheetDataProvider
 * <hr /> Created by damian.giedrys on 2016-05-19.
 */
public class GoogleSheetDataProvider implements IDataProvider {

    private static final String TAG = GoogleSheetDataProvider.class.getSimpleName();

    public static final String SCRIPT_ID_TEST = "AKfycbzZv4eNdXWJuruu99p7OUPmC7n6-0WOtCoa0CmpuMQ";
    public static final String SCRIPT_ID_RELEASE = "AKfycbw6VVuIUYjNs-8LpZ78QeSAZI-9k8LgEdSt3w2F28HOM-cMcyI";

    public static final String TOKEN = "12345";

    @Override
    public void getCategoryItems(@NonNull Context ctx, ECategory cat, final @NonNull DataResponse<List<ContentItem>> callback) {
        Query query = createQuery(ctx);

        query.getCategoryItems(ContentItem.Response.class, cat.id, new ResponseListener() {
            @Override
            public void onSuccess(SheetResponse response) {
                handleResponse(response);
                List<ContentItem> list = response.getResponse();
                callback.onSuccess(list);
            }

            @Override
            public void onError(VolleyError error) {
                handleError(error, callback);
            }
        });
    }

    @Override
    public void getThingsToDo(@NonNull Context ctx, final @NonNull DataResponse<List<ContentItem>> callback) {
        Query query = createQuery(ctx);

        query.getThingsTodo(ContentItem.Response.class, new ResponseListener() {
            @Override
            public void onSuccess(SheetResponse response) {
                handleResponse(response);
                List<ContentItem> list = response.getResponse();
                callback.onSuccess(list);
            }

            @Override
            public void onError(VolleyError error) {
                handleError(error, callback);
            }
        });
    }

    @Override
    public void getEvents(@NonNull Context ctx, final @NonNull DataResponse<List<ContentItem>> callback) {
        Query query = createQuery(ctx);

        query.getEvents(ContentItem.Response.class, new ResponseListener() {
            @Override
            public void onSuccess(SheetResponse response) {
                handleResponse(response);
                List<ContentItem> list = response.getResponse();
                callback.onSuccess(list);
            }

            @Override
            public void onError(VolleyError error) {
                handleError(error, callback);
            }
        });
    }

    @Override
    public void getOutdoors(@NonNull Context ctx, final @NonNull DataResponse<List<ContentItem>> callback) {
        Query query = createQuery(ctx);

        query.getOutdoors(ContentItem.Response.class, new ResponseListener() {
            @Override
            public void onSuccess(SheetResponse response) {
                handleResponse(response);
                List<ContentItem> list = response.getResponse();
                callback.onSuccess(list);
            }

            @Override
            public void onError(VolleyError error) {
                handleError(error, callback);
            }
        });
    }

    @Override
    public void getRecords(@NonNull Context ctx, int[] ids, final @NonNull DataResponse<List<ContentItem>> callback) {
        Query query = createQuery(ctx);

        query.getRecords(ContentItem.Response.class, ids, new ResponseListener() {
            @Override
            public void onSuccess(SheetResponse response) {
                handleResponse(response);

                List<ContentItem> list = response.getResponse();
                callback.onSuccess(list);
            }

            @Override
            public void onError(VolleyError error) {
                handleError(error, callback);
            }
        });
    }

    private String getScriptKey() {
        return SCRIPT_ID_RELEASE;
    }

    private Query createQuery(@NonNull Context ctx) {
        return new Query(ctx, getScriptKey(), TOKEN, MyLocationManager.INSTANCE.getLastLocation());
    }

    private void handleResponse(SheetResponse response) {
        if (AppUtils.DEBUG) {
            Log.d(TAG, "onSuccess, response: " + response);
        }
    }

    private void handleError(VolleyError error, DataResponse<?> callback) {
        Log.w(TAG, "onError, error: " + error);
        int code = -1;
        if (error.networkResponse != null) {
            code = error.networkResponse.statusCode;
        } else if (error instanceof NoConnectionError) {
            // VolleyError: com.android.volley.NoConnectionError: java.net.UnknownHostException:
            // Unable to resolve host "script.google.com": No address associated with hostname
            code = CustomErrorCodes.NO_CONNECTION_ERROR;
        }
        callback.onError(code);
    }

}
