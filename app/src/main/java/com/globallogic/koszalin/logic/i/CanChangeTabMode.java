package com.globallogic.koszalin.logic.i;

import android.support.design.widget.TabLayout;

/**
 * CanChangeTabMode
 * <hr /> Created by damian.giedrys on 2016-04-29.
 */
public interface CanChangeTabMode {

    @TabLayout.Mode int getTabMode();

    boolean isTabVisible();

}
