package com.globallogic.koszalin;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.globallogic.googlesheetlib.query.Query;
import com.globallogic.koszalin.gcm.RegistrationIntentService;
import com.globallogic.koszalin.logic.GoogleSheetDataProvider;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;

/**
 * IKApplication
 * <hr /> Created by damian.giedrys on 2016-05-19.
 */
public class IKApplication extends Application {

    private static IKApplication INSTANCE;
    private static Query query;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i("IKApplication3", "-------------------- create --------------------");
        INSTANCE = this;

        query = new Query(this, GoogleSheetDataProvider.TOKEN,GoogleSheetDataProvider.SCRIPT_ID_RELEASE,null,false);

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.networkInterceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response originalResponse = chain.proceed(chain.request());
                Log.i("networkInterceptors","cache-control original "+originalResponse.header("Cache-Control"));
                Response response = originalResponse.newBuilder().header("Cache-Control", "public, max-age=" + (60 * 60 * 24 * 365)).build();
                Log.i("networkInterceptors","cache-control "+response.header("Cache-Control"));
                return response;
            }
        });

        okHttpClient.setCache(new Cache(this.getCacheDir(), Integer.MAX_VALUE));
        OkHttpDownloader okHttpDownloader = new OkHttpDownloader(okHttpClient);
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(okHttpDownloader);


        Picasso built = builder.build();
        if (AppUtils.DEBUG) {
//            RED color indicates that image is fetched from NETWORK.
//            GREEN color indicates that image is fetched from CACHE MEMORY.
//            BLUE color indicates that image is fetched from DISK MEMORY.
            built.setIndicatorsEnabled(true);
//            built.setLoggingEnabled(true);
        }
        Picasso.setSingletonInstance(built);



        startService(new Intent(this, RegistrationIntentService.class));


//        // part
//        double lat1 = 54.195256;
//        double lng1 = 16.1959589;
//
//        // irish
//        double lat2 = 54.190331;
//        double lng2 = 16.198497;
//        // 552.38 m , 0.005143993801614441
//
//        // 0.5719584946863718
//        double d = AppUtils.distance(lat1, lng1, lat2, lng2);
//        Log.i("TEST", "d: " + d);

    }

    public static Context getCtx() {
        return INSTANCE.getBaseContext();
    }
    public static Query getQuery(){
        return query;
    }
}
