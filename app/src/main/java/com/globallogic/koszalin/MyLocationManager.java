package com.globallogic.koszalin;

import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.globallogic.koszalin.shared.AndroidUtils;
import com.google.android.gms.maps.LocationSource;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * MyLocationManager
 * <hr /> Created by damian.giedrys on 2016-05-18.
 */
public enum MyLocationManager implements LocationListener {

    INSTANCE;

    private static final String TAG = MyLocationManager.class.getSimpleName();
    private static final int T = 120000; // 2 * 60 * 1000 , [2 min]

    private List<LocationSource.OnLocationChangedListener> _listeners;
    private boolean _isAccessFineLocationGranted = false;
    private boolean _isLocationListenerAdded = false;
    private Location _lastLocation;

    public void init() {
        _isAccessFineLocationGranted = false;
        _isLocationListenerAdded = false;
        _lastLocation = null;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged, location:" + location);
        _lastLocation = location;
        maybeNotifyListeners();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(TAG, "onStatusChanged, provider:" + provider + ", status:" + status);
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d(TAG, "onProviderEnabled, provider:" + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d(TAG, "onProviderDisabled, provider:" + provider);
    }

    public void setAccessGranted(boolean accessGranted) {
        this._isAccessFineLocationGranted = accessGranted;
    }

    public boolean isAccessGranted() {
        return _isAccessFineLocationGranted;
    }

    public double[] getLastLocation() {
//        Log.d(TAG, "getLastLocation: " + _lastLocation);
        if (_lastLocation != null) {
            return new double[]{_lastLocation.getLatitude(), _lastLocation.getLongitude()};
        }
        return null;
    }

    public void requestLocationUpdates(Context ctx) {
        Log.d(TAG, "requestLocationUpdates");

        LocationManager locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);

        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, T, 0, this);
            _isLocationListenerAdded = true;
        } catch (SecurityException se) {
            Log.w(TAG, "SecurityException during requestLocationUpdates", se);
            se.printStackTrace(); // TODO
        }
    }

    public void removeLocationUpdates(Context ctx) {
        Log.d(TAG, "removeLocationUpdates");
        if (_isLocationListenerAdded && AndroidUtils.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Log.d(TAG, "removeLocationUpdates2");
            // Acquire a reference to the system Location Manager
            LocationManager locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
            if (locationManager != null) {
                try {
                    Log.d(TAG, "removing location updates listener");
                    locationManager.removeUpdates(this);
                } catch (SecurityException se) {
                    Log.w(TAG, "Error during removing location updates listener", se);
                    se.printStackTrace(); // TODO
                }
            }
        }
    }

    public void addLocationChangeListener(@NonNull LocationSource.OnLocationChangedListener listener) {
        if (_listeners == null) {
            _listeners = new ArrayList<>();
        }
        _listeners.add(listener);
    }

    public boolean removeLocationListener(@NonNull LocationSource.OnLocationChangedListener listener) {
        if (_listeners != null && _listeners.contains(listener)) {
            return _listeners.remove(listener);
        }
        return false;
    }

    public static boolean isGPSProviderEnabled(Context ctx) {
        boolean ret = false;
        try {
            LocationManager locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
            ret = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ignore) {
        }
        return ret;
    }

    private void maybeNotifyListeners() {
        if (_listeners != null && _listeners.size() > 0) {
            for (LocationSource.OnLocationChangedListener l : _listeners) {
                l.onLocationChanged(_lastLocation);
            }
        }
    }

}
