package com.globallogic.koszalin.version2;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.globallogic.koszalin.MainFragment;
import com.globallogic.koszalin.R;
import com.globallogic.koszalin.base.AbstractActivity;
import com.globallogic.koszalin.logic.i.CanChangeTabMode;
import com.globallogic.koszalin.shared.logic.HasDrawerTitle;
import com.globallogic.koszalin.news.NewsFragment;

public class MainActivity extends AbstractActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String FRAGMENT_NEWS = "fragment_news";
    public static final String FRAGMENT_EVENTS = "fragment_events";
    public static final String FRAGMENT_MAIN = "fragment_main";
    public static final String FRAGMENT_PLACES = "fragment_places";
    public static final String FRAGMENT_PLACES_CONTENT = "fragment_places_content";

    private static final String KEY_SELECTED_NAV_OPTION = "KEY_SELECTED_NAV_OPTION";

    private static final String TAG = "MainActivity";

    private static int selectedNavOption = 0;

    public TabLayout tabLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.toolbar_transparent)));
//        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#550000ff")));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerview = navigationView.getHeaderView(0);
        headerview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(new MainFragment(),FRAGMENT_MAIN);
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });


        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        Log.i(TAG,"onCreate");
        if(savedInstanceState != null){
            if(savedInstanceState.containsKey(KEY_SELECTED_NAV_OPTION)){
                selectedNavOption = savedInstanceState.getInt(KEY_SELECTED_NAV_OPTION);
                Log.i(TAG,"onCreate selectedNavOption = "+selectedNavOption);
            }
        }
        selectFragment(selectedNavOption);

    }

//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//            overridePendingTransition(R.anim.launch_animation, R.anim.exit_animation);
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        selectedNavOption = id;

        selectFragment(id);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void selectFragment(int id){
        if(id == 0){
            replaceFragment(new NewsFragment(), FRAGMENT_NEWS);
            navigationView.setCheckedItem(R.id.nav_news);
        }else {
            navigationView.setCheckedItem(id);
            if (id == R.id.nav_news) {
                replaceFragment(new NewsFragment(), FRAGMENT_NEWS);
            }
//            else if (id == R.id.nav_events) {
//                tabLayout.removeAllTabs();
//                replaceFragment(new EventsFragment(), FRAGMENT_EVENTS);
//            } else if (id == R.id.nav_places) {
//                replaceFragment(new PlacesFragment(), FRAGMENT_PLACES);
//            }
            else if (id == R.id.nav_header) {
                replaceFragment(new MainFragment(), FRAGMENT_PLACES);
            } else {
                tabLayout.removeAllTabs();
                replaceFragment(new MainFragment(), FRAGMENT_MAIN);
            }
        }
    }

    public void replaceFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment newFragment = fragmentManager.findFragmentByTag(tag);
        if (newFragment == null) {
            newFragment = fragment;
        }
        fragmentTransaction.replace(R.id.main_layout, newFragment, tag);
        fragmentTransaction.commit();

        // after
        if (newFragment instanceof HasDrawerTitle) {
            setTitle(((HasDrawerTitle)newFragment).getDrawerTitle(getBaseContext()));
        }
        if (newFragment instanceof CanChangeTabMode) {
            //noinspection ResourceType
            tabLayout.setTabMode(((CanChangeTabMode)newFragment).getTabMode());
        } else {
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE); // change to default
        }

    }

    public void clearAllTabs() {
        tabLayout.removeAllTabs();
    }

    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_SELECTED_NAV_OPTION,selectedNavOption);
        Log.i(TAG,"onSaveInstanceState selectedNavOption = "+selectedNavOption);
    }
}
