package com.globallogic.koszalin;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.globallogic.koszalin.about.AboutActivity;
import com.globallogic.koszalin.base.AbstractActivity;
import com.globallogic.koszalin.content.ContentFragment;
import com.globallogic.koszalin.content.data.EContentCategory;
import com.globallogic.koszalin.logic.i.CanChangeTabMode;
import com.globallogic.koszalin.news.NewsFragment;
import com.globallogic.koszalin.places.PlacesFragment;
import com.globallogic.koszalin.shared.logic.BoolCallback;
import com.globallogic.koszalin.shared.logic.HasDrawerTitle;
import com.globallogic.koszalin.shared.view.AppCompatMsgDialog;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AbstractActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int REQ_PERM_ACCESS_FINE_LOCATIN = 128;
    private static final int REQ_CODE_LOCATION_SOURCE_SETTINGS = 66;

    public static final String FRAGMENT_NEWS = "fragment_news";
    public static final String FRAGMENT_EVENTS = "fragment_events";
    public static final String FRAGMENT_MAIN = "fragment_main";
    public static final String FRAGMENT_PLACES = "fragment_places";
    public static final String FRAGMENT_PLACES_CONTENT = "fragment_places_content";
    public static final String FRAGMENT_CONTENT = "fragment_content";

    private static final String KEY_SELECTED_NAV_OPTION = "KEY_SELECTED_NAV_OPTION";
    private static final String KEY_SELECTED_NAV_OPTION_2 = "KEY_SELECTED_NAV_OPTION_2";
    private static final int CONTENT_SUBMENU_GROUP_ID = 16; // CONTENT_SUBMENU_GROUP_ID
    private static final String TAG = "MainActivity";

    private static int selectedNavOption = 0;
    private int lastSelectedSubMenuItem = -1;
    private String _lastDialogTag;

    public TabLayout tabLayout;
    private NavigationView navigationView;
    private SubMenu subMenu;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate, savedInstanceState:" + savedInstanceState);

        // init Fabric's Crashlytics
        if (!AppUtils.DEBUG)
            Fabric.with(this, new Crashlytics());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolbar(R.id.toolbar);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.toolbar_transparent)));
////        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#550000ff")));

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerview = navigationView.getHeaderView(0);
        if (headerview != null) {
            headerview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    replaceFragment(new MainFragment(), FRAGMENT_MAIN);
                    closeDrawer();
                }
            });
        }
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        lastSelectedSubMenuItem = EContentCategory.THINGS_TODO.id;
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_SELECTED_NAV_OPTION)) {
                selectedNavOption = savedInstanceState.getInt(KEY_SELECTED_NAV_OPTION);
                Log.i(TAG, "onCreate selectedNavOption = " + selectedNavOption);
            }
            if (savedInstanceState.containsKey(KEY_SELECTED_NAV_OPTION_2)) {
                lastSelectedSubMenuItem = savedInstanceState.getInt(KEY_SELECTED_NAV_OPTION_2);
                Log.i(TAG, "onCreate selectedNavOption2 = " + lastSelectedSubMenuItem);
            }
        }

//        selectFragment(selectedNavOption);
        onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_content));


        // add location listener
        MyLocationManager.INSTANCE.init();
        runProtectedMethod(REQ_PERM_ACCESS_FINE_LOCATIN, Manifest.permission.ACCESS_FINE_LOCATION, new BoolCallback() {
            @Override
            public void run(boolean param) {
                MyLocationManager.INSTANCE.setAccessGranted(param);

                if (param) {

                    try {
                        MyLocationManager.INSTANCE.requestLocationUpdates(getBaseContext());

                        if (MyLocationManager.isGPSProviderEnabled(getBaseContext())) {
                            toast(R.string.requesting_current_location);
                        } else {

                            Log.d(TAG, "searching for fragment tag: " + _lastDialogTag);
                            if (_lastDialogTag != null && getFragmentManager().findFragmentByTag(_lastDialogTag) != null) {
                                Fragment fr = getSupportFragmentManager().findFragmentByTag(_lastDialogTag);
                                ((AppCompatMsgDialog) fr).dismiss();
                                Log.d(TAG, "Fragment with tag: " + _lastDialogTag + " dismissed!");
                            }

//                            Log.d(TAG, "start to show dialog");
                            _lastDialogTag = AppCompatMsgDialog.info(getSupportFragmentManager(), getString(R.string.info_gps_not_enabled_open_location_settings),
                                    new ResultReceiver(null) {
                                        @Override
                                        protected void onReceiveResult(int resultCode, Bundle resultData) {
                                            if (resultCode == DialogInterface.BUTTON_POSITIVE) {
                                                startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                                        REQ_CODE_LOCATION_SOURCE_SETTINGS);
                                            }
                                        }
                                    }).getDialogTag();

                            Log.d(TAG, "lastTag: " + _lastDialogTag);
                        }

                    } catch (SecurityException se) {
                        Log.w(TAG, "SecurityException during requestLocationUpdates", se);
                    }
                } else {
                    Log.i(TAG, "Permission to ACCESS_FINE_LOCATIN was NOT granted!");
                    toast(R.string.info_gps_permission_denied);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {

        MyLocationManager.INSTANCE.removeLocationUpdates(this.getBaseContext());
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult , requestCode=" + requestCode + ", resultCode=" + resultCode);
        if (requestCode == REQ_CODE_LOCATION_SOURCE_SETTINGS) {
            if (MyLocationManager.isGPSProviderEnabled(this)) {
                Log.d(TAG, "onActivityResult gps_enabled");
                toast(R.string.requesting_current_location);
            } else {
                Log.d(TAG, "onActivityResult gps_disabled");
//                toast(R.string.info_location_not_enabled_by_user);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Log.i(TAG, "onNavigationItemSelected, item:" + item);
        Log.i(TAG, "getOrder:" + item.getOrder() + ", getGroupId:" + item.getGroupId() + ", getItemId:" + id);

        selectedNavOption = id;

        if (id == R.id.nav_about_app) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            closeDrawer();
            return false;
        } else if (id == R.id.nav_about_city) {
            showInfo(R.string.info_functionality_not_available);

        } else if (id == R.id.nav_content) {
            maybeCreateSubMenu();
            maybeSetSubMenuVisible(true);
            maybeSetLastSelectedContentCat();
            selectFragment(id);
            EContentCategory cat = EContentCategory.getById(lastSelectedSubMenuItem);
            if (cat != null)
                setTitle(getString(cat.titleRes));
            closeDrawer();
            return false;
        } else if (item.getGroupId() == CONTENT_SUBMENU_GROUP_ID) {
            lastSelectedSubMenuItem = item.getItemId();
            EContentCategory cat = EContentCategory.getById(lastSelectedSubMenuItem);
            if (cat != null)
                setTitle(getString(cat.titleRes));

            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment newFragment = fragmentManager.findFragmentByTag(FRAGMENT_CONTENT);
            if (newFragment != null && newFragment instanceof ContentFragment) {
                ((ContentFragment) newFragment).setSelectedCategory(cat);
            }
        } else {
            maybeSetSubMenuVisible(false);
        }

        selectFragment(id);
        closeDrawer();
        return true;
    }

    private void selectFragment(int id) {

        switch (id) {
            case R.id.nav_news:
                replaceFragment(new NewsFragment(), FRAGMENT_NEWS);
                break;
//            case R.id.nav_events:
//                replaceFragment(new EventsFragment(), FRAGMENT_EVENTS);
//            break;
            case R.id.nav_places:
                replaceFragment(new PlacesFragment(), FRAGMENT_PLACES);
                break;
            case R.id.nav_content:
                ContentFragment fr = new ContentFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(ContentFragment.BUNDLE_KEY_CATID, lastSelectedSubMenuItem);
                fr.setArguments(bundle);
                replaceFragment(fr, FRAGMENT_CONTENT);
                break;
            case R.id.nav_header:
                replaceFragment(new MainFragment(), FRAGMENT_MAIN);
                break;
            default:
                break;
        }

        /*
        if (id == 0) {
            replaceFragment(new NewsFragment(), FRAGMENT_NEWS);
            navigationView.setCheckedItem(R.id.nav_news);
        } else {
            navigationView.setCheckedItem(id);
            if (id == R.id.nav_news) {
                replaceFragment(new NewsFragment(), FRAGMENT_NEWS);
            }
//            else if (id == R.id.nav_events) {
//                tabLayout.removeAllTabs();
//                replaceFragment(new EventsFragment(), FRAGMENT_EVENTS);
//            } else if (id == R.id.nav_places) {
//                replaceFragment(new PlacesFragment(), FRAGMENT_PLACES);
//            }
            else if (id == R.id.nav_header) {
                replaceFragment(new MainFragment(), FRAGMENT_PLACES);
            } else {
                tabLayout.removeAllTabs();
                replaceFragment(new MainFragment(), FRAGMENT_MAIN);
            }
        }
        */
    }

    public void replaceFragment(Fragment fragment, String tag) {
        Log.d(TAG, "replaceFragment: " + fragment);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment newFragment = fragmentManager.findFragmentByTag(tag);
        if (newFragment == null) {
            newFragment = fragment;
        }
        fragmentTransaction.replace(R.id.main_layout, newFragment, tag);
        fragmentTransaction.commit();

        // after
        if (newFragment instanceof HasDrawerTitle) {
            setTitle(((HasDrawerTitle) newFragment).getDrawerTitle(getBaseContext()));
        }
        if (newFragment instanceof CanChangeTabMode) {
            Log.d(TAG, "replaceFragment instance of CanChangeTabMode");
            //noinspection ResourceType
            CanChangeTabMode i = (CanChangeTabMode) newFragment;

            //noinspection ResourceType
            tabLayout.setTabMode(i.getTabMode());
            tabLayout.setVisibility(i.isTabVisible() ? View.VISIBLE : View.GONE);
        } else {
            Log.d(TAG, "replaceFragment instance of CanChangeTabMode NOT!");
            // default
            tabLayout.setVisibility(View.VISIBLE);
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE); // change to default
        }
    }

    public void clearAllTabs() {
        tabLayout.removeAllTabs();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_SELECTED_NAV_OPTION, selectedNavOption);
        Log.i(TAG, "onSaveInstanceState selectedNavOption = " + selectedNavOption);
        outState.putInt(KEY_SELECTED_NAV_OPTION_2, lastSelectedSubMenuItem);
        Log.i(TAG, "onSaveInstanceState lastSelectedSubMenuItem = " + lastSelectedSubMenuItem);
    }

    private void closeDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null)
            drawer.closeDrawer(GravityCompat.START);
    }

    private void maybeCreateSubMenu() {
        if (subMenu == null) {
            MenuItem mi = navigationView.getMenu().findItem(R.id.nav_content);

            if (mi != null) {
                int order = mi.getOrder();
                Menu menu = navigationView.getMenu();

                // adding a section and items into it
                subMenu = menu.addSubMenu(CONTENT_SUBMENU_GROUP_ID, 1, order, getString(R.string.fragment_content));
                for (EContentCategory subCat : EContentCategory.values()) {
                    if (subCat != EContentCategory.UNKNOWN) {
                        MenuItem subCatMI = subMenu.add(CONTENT_SUBMENU_GROUP_ID, subCat.id, subCat.order, getString(subCat.titleRes));
                        if (subCat.iconResMenu > 0) {
                            subCatMI.setIcon(subCat.iconResMenu);
                        }
                    }
                }

                menu.setGroupCheckable(CONTENT_SUBMENU_GROUP_ID, true, true);
                subMenu.setGroupCheckable(CONTENT_SUBMENU_GROUP_ID, true, true);
            }
            maybeSetSubMenuVisible(false);
        }
    }

    private void maybeSetSubMenuVisible(boolean visible) {
        if (subMenu != null) {
            navigationView.getMenu().setGroupVisible(CONTENT_SUBMENU_GROUP_ID, visible);
        }
    }

    private void maybeSetLastSelectedContentCat() {
        if (subMenu != null) {
            MenuItem mi = subMenu.findItem(lastSelectedSubMenuItem);
            if (mi != null) {
                if (!mi.isChecked()) {
                    navigationView.setCheckedItem(lastSelectedSubMenuItem);
//                    navigationView.invalidate();
                }
            }
        }
    }

}
