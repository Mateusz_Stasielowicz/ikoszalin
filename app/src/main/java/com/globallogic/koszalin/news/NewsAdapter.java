package com.globallogic.koszalin.news;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.globallogic.koszalin.R;
import com.globallogic.koszalin.shared.view.AbstractRecyclerViewAdapter;
import com.globallogic.koszalin.shared.view.CustomSortedList;

/**
 * Created by mateusz.stasielowicz on 2016-04-28.
 */
public class NewsAdapter extends AbstractRecyclerViewAdapter<NewsItem> {



    public NewsAdapter(CustomSortedList<NewsItem> mDataset, Context mContext) {
        super(mDataset, mContext);
    }

    public NewsAdapter(CustomSortedList<NewsItem> mDataset, Context mContext, AdapterView.OnItemClickListener onClickListener) {
        super(mDataset, mContext, onClickListener);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View mainView;
        protected ImageView icon;
        protected TextView title;
        protected TextView content;


        public ViewHolder(View v) {
            super(v);
            this.mainView = v;
            this.icon = (ImageView) v.findViewById(R.id.news_item_image);
            this.title = (TextView) v.findViewById(R.id.news_item_title);
            this.content = (TextView) v.findViewById(R.id.news_item_content);

        }
    }

    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_item_news, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder h, int position) {
        NewsItem item = getItem(position);
        NewsAdapter.ViewHolder holder = (ViewHolder) h;
        if (mOnClickListener != null) {
            holder.mainView.setTag(holder);
            holder.mainView.setOnClickListener(this);
        }
        if (item == null) {
            return;
        }

        if(item.getImageRes() > 0) {
            holder.icon.setImageResource(item.getImageRes());
        }else if(item.getImageUrl() != null){

        }

        holder.title.setText(item.getTitle());
        holder.content.setText(item.getContent());



    }
}
