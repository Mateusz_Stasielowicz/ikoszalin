package com.globallogic.koszalin.news;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.globallogic.koszalin.AppUtils;
import com.globallogic.koszalin.R;
import com.globallogic.koszalin.shared.view.CustomSortedList;

/**
 * Created by mateusz.stasielowicz on 2016-04-28.
 */
public class NewsContentFragment extends Fragment implements AdapterView.OnItemClickListener{

    private NewsAdapter newsAdapter;
    private CustomSortedList<NewsItem> dataset;
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news_content,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.news_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        dataset = new CustomSortedList<>(NewsItem.class);

        newsAdapter = new NewsAdapter(dataset,getContext(),this);
        recyclerView.setAdapter(newsAdapter);
        recyclerView.setHasFixedSize(true);

        initDataset();
    }

    private void initDataset(){


        dataset.clear();
        dataset.add(new NewsItem(R.drawable.logo_koszalin_tr_100alpha,"Tytuł aktualności 1","Lorem ipsum dolor sit amet, viris iudico timeam vix ea, cu mei commodo albucius. Dicant efficiendi no usu, ei est sapientem gubergren, iusto paulo scripserit eum et. Est munere fabulas fastidii no. Vis malis fierent ne, at probo aliquando vim. Ex cum placerat tractatos iracundia, est in ubique interesset."));
        dataset.add(new NewsItem(R.drawable.logo_koszalin_tr_100alpha,"Tytuł aktualności 2","Lorem ipsum dolor sit amet, viris iudico timeam vix ea, cu mei commodo albucius. Dicant efficiendi no usu, ei est sapientem gubergren, iusto paulo scripserit eum et. Est munere fabulas fastidii no. Vis malis fierent ne, at probo aliquando vim. Ex cum placerat tractatos iracundia, est in ubique interesset."));
        dataset.add(new NewsItem(R.drawable.logo_koszalin_tr_100alpha,"Tytuł aktualności 3","Lorem ipsum dolor sit amet, viris iudico timeam vix ea, cu mei commodo albucius. Dicant efficiendi no usu, ei est sapientem gubergren, iusto paulo scripserit eum et. Est munere fabulas fastidii no. Vis malis fierent ne, at probo aliquando vim. Ex cum placerat tractatos iracundia, est in ubique interesset."));
        dataset.add(new NewsItem(R.drawable.logo_koszalin_tr_100alpha,"Tytuł aktualności 4","Lorem ipsum dolor sit amet, viris iudico timeam vix ea, cu mei commodo albucius. Dicant efficiendi no usu, ei est sapientem gubergren, iusto paulo scripserit eum et. Est munere fabulas fastidii no. Vis malis fierent ne, at probo aliquando vim. Ex cum placerat tractatos iracundia, est in ubique interesset."));
        dataset.add(new NewsItem(R.drawable.logo_koszalin_tr_100alpha,"Tytuł aktualności 5","Lorem ipsum dolor sit amet, viris iudico timeam vix ea, cu mei commodo albucius. Dicant efficiendi no usu, ei est sapientem gubergren, iusto paulo scripserit eum et. Est munere fabulas fastidii no. Vis malis fierent ne, at probo aliquando vim. Ex cum placerat tractatos iracundia, est in ubique interesset."));

        newsAdapter.notifyDataSetChanged();
        Log.i("TEST","dataset size = "+dataset.size());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
