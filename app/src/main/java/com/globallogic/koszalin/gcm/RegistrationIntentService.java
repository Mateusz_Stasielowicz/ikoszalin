package com.globallogic.koszalin.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.VolleyError;
import com.globallogic.googlesheetlib.query.Query;
import com.globallogic.googlesheetlib.query.request.RequestItemGcm;
import com.globallogic.googlesheetlib.query.request.ResponseListener;
import com.globallogic.googlesheetlib.query.response.SheetResponse;
import com.globallogic.koszalin.R;
import com.globallogic.koszalin.logic.GoogleSheetDataProvider;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by mateusz.stasielowicz on 2016-06-02.
 */
public class RegistrationIntentService extends IntentService {


    private static final String SENT_TOKEN_TO_SERVER = "SENT_TOKEN_TO_SERVER";
    private static final String REGISTRATION_COMPLETE = "REGISTRATION_COMPLETE";
    private static final String APPLICATION_ID = "APPLICATION_ID";

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};
    private SharedPreferences sharedPreferences;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            boolean forceRefresh = false;
            if(intent.hasExtra(GcmIdListenerService.FORCE_TOKEN_REFRESH)){
                forceRefresh = intent.getBooleanExtra(GcmIdListenerService.FORCE_TOKEN_REFRESH,false);

            }
            // [START register_for_gcm]
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // R.string.gcm_defaultSenderId (the Sender ID) is typically derived from google-services.json.
            // See https://developers.google.com/cloud-messaging/android/start for details on this file.
            // [START get_token]
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            // [END get_token]
            Log.i(TAG, "GCM Registration Token: " + token);

            String applicationId = sharedPreferences.getString(APPLICATION_ID,null);
            if(applicationId == null){
                applicationId = UUID.randomUUID().toString();
                sharedPreferences.edit().putString(APPLICATION_ID,applicationId).apply();
            }
            // TODO: Implement this method to send any registration to your app's servers.
            if(!sharedPreferences.getBoolean(SENT_TOKEN_TO_SERVER,false) || forceRefresh) {
                sendRegistrationToServer(applicationId, token);
            }

            // Subscribe to topic channels
            subscribeTopics(token);

            // You should store a boolean that indicates whether the generated token has been
            // sent to your server. If the boolean is false, send the token to your server,
            // otherwise your server should have already received the token.

            // [END register_for_gcm]
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            sharedPreferences.edit().putBoolean(SENT_TOKEN_TO_SERVER, false).apply();
        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    /**
     * Persist registration to third-party servers.
     *
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String applicationId, String token) {
        Query query = new Query(this, GoogleSheetDataProvider.SCRIPT_ID_TEST,GoogleSheetDataProvider.TOKEN,null,true);

        query.sendGcmReqistrationToken(GcmResponseItem.Response.class, new RequestItemGcm(applicationId, token), new ResponseListener() {
            @Override
            public void onSuccess(SheetResponse response) {
                Log.i(TAG,"sendRegistrationToServer success");
                sharedPreferences.edit().putBoolean(SENT_TOKEN_TO_SERVER, true).apply();
            }

            @Override
            public void onError(VolleyError error) {
                sharedPreferences.edit().putBoolean(SENT_TOKEN_TO_SERVER, false).apply();
                Log.i(TAG,"sendRegistrationToServer error "+error);
            }
        });
        // Add custom implementation, as needed.
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
            Log.i(TAG,"subscribe for topic "+topic);
        }
    }
    // [END subscribe_topics]

}
