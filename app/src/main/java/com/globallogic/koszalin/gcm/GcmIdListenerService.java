package com.globallogic.koszalin.gcm;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.*;
import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by mateusz.stasielowicz on 2016-06-02.
 */
public class GcmIdListenerService extends InstanceIDListenerService {

    public static final String FORCE_TOKEN_REFRESH = "FORCE_TOKEN_REFRESH";

    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        Intent intent = new Intent(this, RegistrationIntentService.class);
        intent.putExtra(FORCE_TOKEN_REFRESH,true);
        startService(intent);
    }

}
