package com.globallogic.koszalin.gcm;

import com.globallogic.googlesheetlib.query.response.SheetResponse;

/**
 * Created by mateusz.stasielowicz on 2016-06-02.
 */
public class GcmResponseItem {

    private String status;

    public class Response extends SheetResponse<GcmResponseItem> {

    }
}
