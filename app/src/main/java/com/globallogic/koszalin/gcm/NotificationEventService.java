package com.globallogic.koszalin.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

/**
 * Created by mateusz.stasielowicz on 2016-06-03.
 */
public class NotificationEventService extends IntentService {

    private static final String TAG = NotificationEventService.class.getSimpleName();
    public static final String EXTRA_NOTIDICATION_ID = "EXTRA_NOTIDICATION_ID";

    public NotificationEventService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG,"onHandleIntent "+intent.toString());
        GcmListenerService.notificationCount--;
        if(GcmListenerService.notificationCount < 0){
            GcmListenerService.notificationCount = 0;
        }
        if( GcmListenerService.notificationCount == 0){
            GcmListenerService.notificationId = 0;
        }

        if(intent.hasExtra(EXTRA_NOTIDICATION_ID)){
            int notificationId = intent.getIntExtra(EXTRA_NOTIDICATION_ID,-1);
            Log.i(TAG,"Notification Id = "+notificationId);
        }
        Log.i(TAG,"notification id count = "+GcmListenerService.notificationId);
    }
}
