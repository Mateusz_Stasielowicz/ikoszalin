package com.globallogic.koszalin.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.android.volley.VolleyError;
import com.globallogic.googlesheetlib.query.Query;
import com.globallogic.googlesheetlib.query.request.ResponseListener;
import com.globallogic.googlesheetlib.query.response.SheetResponse;
import com.globallogic.koszalin.R;
import com.globallogic.koszalin.content.ContentActivity;
import com.globallogic.koszalin.content.data.ContentItem;
import com.globallogic.koszalin.logic.GoogleSheetDataProvider;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.DateFormat;
import java.util.List;

/**
 * Created by mateusz.stasielowicz on 2016-06-02.
 */
public class GcmListenerService extends com.google.android.gms.gcm.GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    private static final String KEY_NOTIFICATION_GROUP_SPECIAL_EVENT = "KEY_NOTIFICATION_GROUP_SPECIAL_EVENT";
    public static int notificationId = 0;
    public static int notificationCount = 0;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d(TAG, "Data: " + data.toString());

        if (data.containsKey("payload")) {
            String payload = data.getString("payload");

            DataItem item = null;
            try {
                item = new Gson().fromJson(payload, DataItem.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
            Log.i(TAG, "item = " + item);
            if (item != null && item.getType() != null) {
                switch (item.getType()) {
                    case UPDATED_RECORDS:

                        break;
                    case SPECIAL_EVENT:
                        if (item.getIds() != null && item.getIds().length > 0) {
                            int specialEventId = item.getIds()[0];
                            downloadContentAndShowNotification(specialEventId);
                        }
                        break;
                }

            }
        }

    }

    private void downloadContentAndShowNotification(int id) {

        new Query(this, GoogleSheetDataProvider.SCRIPT_ID_RELEASE, GoogleSheetDataProvider.TOKEN, null).getRecords(ContentItem.Response.class, new int[]{id}, new ResponseListener() {
                    @Override
                    public void onSuccess(SheetResponse response) {
                        if (response != null && response.getResponse() != null && response.getResponse().size() > 0) {
                            List<ContentItem> contentItems = response.getResponse();
                            final ContentItem contentItem = contentItems.get(0);
                            if (contentItem != null) {
                                if (contentItem.getPhoto() != null && contentItem.getPhoto()[0] != null && !contentItem.getPhoto()[0].isEmpty()) {
                                    AsyncTask<Void, Void, Bitmap> loadImage = new AsyncTask<Void, Void, Bitmap>() {
                                        @Override
                                        protected Bitmap doInBackground(Void... params) {
                                            try {
                                                return Picasso.with(GcmListenerService.this).load(contentItem.getPhoto()[0]).get();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            return null;
                                        }

                                        @Override
                                        protected void onPostExecute(Bitmap bitmap) {
                                            showNotification(contentItem, bitmap);
                                        }
                                    };
                                    loadImage.execute();
                                } else {
                                    showNotification(contentItem, null);
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {

                    }
                }

        );
    }

    private void showNotification(ContentItem contentItem, Bitmap bitmap) {
        Log.i(TAG, "showNotification bitmap = " + bitmap);
        Bitmap smallBitmap = centerCropBitmap(bitmap);
        Intent intent = new Intent(GcmListenerService.this, ContentActivity.class);
        intent.putExtra(ContentActivity.BUNDLE_KEY_ITEM, contentItem);
//        myIntent.setAction(MainActivity.ACTION_START_FROM_NOTIFICATION);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(ContentActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(notificationId, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent deleteIntent = new Intent(GcmListenerService.this, NotificationEventService.class);
        deleteIntent.putExtra(NotificationEventService.EXTRA_NOTIDICATION_ID, notificationId);
        PendingIntent deletePendingIntent = PendingIntent.getService(GcmListenerService.this, notificationId,deleteIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        String s = null;
        if (contentItem.getDatetimeFrom() != null) {
            DateFormat dateFormat = DateFormat.getDateTimeInstance();
            s = dateFormat.format(contentItem.getDatetimeFrom());
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(GcmListenerService.this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(contentItem.getName())
                .setContentText(contentItem.getDescriptionShort())
                .setContentIntent(pendingIntent)
                .setSubText(s)
                .setDeleteIntent(deletePendingIntent)
                .setCategory(Notification.CATEGORY_EVENT)
                .setGroup(KEY_NOTIFICATION_GROUP_SPECIAL_EVENT)
                .setPriority(Notification.PRIORITY_HIGH);

        if (bitmap != null) {
            mBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                    .setSummaryText(contentItem.getDescriptionShort())
                    .bigPicture(bitmap))
                    .setLargeIcon(smallBitmap);

        }


        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(notificationId, mBuilder.build());
//        showGroupSummaryNotification(mNotifyMgr);
        notificationId++;
        notificationCount++;

    }

    private void showGroupSummaryNotification(NotificationManager notificationManager) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle("Dummy content title")
                .setContentText("Dummy content text")
                .setNumber(3)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setCategory(Notification.CATEGORY_EVENT)
                .setGroupSummary(true)
                .setGroup(KEY_NOTIFICATION_GROUP_SPECIAL_EVENT);

        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle()
                .setSummaryText("Inbox summary text")
                .setBigContentTitle("Big content title");
        for (int i = 0; i < 3; i++) {
            style.addLine("Name " + i + 1);
        }
        builder.setStyle(style);

        Notification notification = builder.build();
        notificationManager.notify(999, notification);

    }

    private Bitmap centerCropBitmap(Bitmap srcBmp) {
        Bitmap dstBmp;
        if (srcBmp.getWidth() >= srcBmp.getHeight()) {

            dstBmp = Bitmap.createBitmap(
                    srcBmp,
                    srcBmp.getWidth() / 2 - srcBmp.getHeight() / 2,
                    0,
                    srcBmp.getHeight(),
                    srcBmp.getHeight()
            );

        } else {

            dstBmp = Bitmap.createBitmap(
                    srcBmp,
                    0,
                    srcBmp.getHeight() / 2 - srcBmp.getWidth() / 2,
                    srcBmp.getWidth(),
                    srcBmp.getWidth()
            );
        }
        return dstBmp;
    }

}
