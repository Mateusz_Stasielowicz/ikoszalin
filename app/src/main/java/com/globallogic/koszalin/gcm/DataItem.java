package com.globallogic.koszalin.gcm;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

/**
 * Created by mateusz.stasielowicz on 2016-06-02.
 */
public class DataItem {

    public enum GcmDataType{
        @SerializedName("updatedRecords")
        UPDATED_RECORDS,
        @SerializedName("specialEvent")
        SPECIAL_EVENT
    }

    private GcmDataType type;
    private int[] ids;


    public DataItem(GcmDataType type, int[] ids) {
        this.type = type;
        this.ids = ids;
    }

    public GcmDataType getType() {
        return type;
    }

    public void setType(GcmDataType type) {
        this.type = type;
    }

    public int[] getIds() {
        return ids;
    }

    public void setIds(int[] ids) {
        this.ids = ids;
    }

    @Override
    public String toString() {
        return "DataItem{" +
                "type=" + type +
                ", ids=" + Arrays.toString(ids) +
                '}';
    }
}
