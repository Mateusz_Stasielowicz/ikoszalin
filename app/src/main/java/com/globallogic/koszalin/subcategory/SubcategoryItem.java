package com.globallogic.koszalin.subcategory;

/**
 * Created by mateusz.stasielowicz on 2016-04-28.
 */
public class SubcategoryItem {
    private int position;
    private String title;


    public SubcategoryItem(int position, String title) {
        this.position = position;
        this.title = title;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
