package com.globallogic.koszalin.subcategory;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.util.SortedList;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.globallogic.koszalin.R;
import com.globallogic.koszalin.shared.view.AbstractRecyclerViewAdapter;
import com.globallogic.koszalin.shared.view.CustomSortedList;

import java.util.Collection;

/**
 * Created by mateusz.stasielowicz on 2016-04-28.
 */
public class SubcategoryRecyclerView extends RecyclerView {

    private CustomSortedList<SubcategoryItem> dataset;
    private SubcategoryAdapter subcategoryAdapter;
    private AdapterView.OnItemClickListener onItemClickListener;

    public SubcategoryRecyclerView(Context context) {
        super(context);
        init();
    }

    public SubcategoryRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SubcategoryRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));

        dataset = new CustomSortedList<>(SubcategoryItem.class, new SortedList.Callback<SubcategoryItem>() {
            @Override
            public int compare(SubcategoryItem o1, SubcategoryItem o2) {
                return o1.getPosition() - o2.getPosition();
            }

            @Override
            public void onInserted(int position, int count) {

            }

            @Override
            public void onRemoved(int position, int count) {

            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {

            }

            @Override
            public void onChanged(int position, int count) {

            }

            @Override
            public boolean areContentsTheSame(SubcategoryItem oldItem, SubcategoryItem newItem) {
                return false;
            }

            @Override
            public boolean areItemsTheSame(SubcategoryItem item1, SubcategoryItem item2) {
                return false;
            }
        });

        subcategoryAdapter = new SubcategoryAdapter(dataset,getContext());
        setAdapter(subcategoryAdapter);
        setHasFixedSize(true);

    }

    public void setDataset(Collection<SubcategoryItem> dataset) {
        this.dataset.clear();
        this.dataset.addAll(dataset);
        subcategoryAdapter.notifyDataSetChanged();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        subcategoryAdapter.setOnItemClickListener(onItemClickListener);
    }



    public static class SubcategoryAdapter extends AbstractRecyclerViewAdapter<SubcategoryItem> {



        public SubcategoryAdapter(CustomSortedList<SubcategoryItem> mDataset, Context mContext) {
            super(mDataset, mContext);
        }

        public SubcategoryAdapter(CustomSortedList<SubcategoryItem> mDataset, Context mContext, AdapterView.OnItemClickListener onClickListener) {
            super(mDataset, mContext, onClickListener);
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {

            protected View mainView;
            protected TextView title;



            public ViewHolder(View v) {
                super(v);
                this.mainView = v;
                this.title = (TextView) v.findViewById(R.id.subcategory_title);


            }
        }

        @Override
        public SubcategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {

            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_item_subcategory, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }


        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder h, int position) {
            SubcategoryItem item = getItem(position);
            SubcategoryAdapter.ViewHolder holder = (ViewHolder) h;
            if (mOnClickListener != null) {
                holder.mainView.setTag(holder);
                holder.mainView.setOnClickListener(this);
            }

            if (item == null) {
                return;
            }
            holder.title.setText(item.getTitle());


        }
    }

}
