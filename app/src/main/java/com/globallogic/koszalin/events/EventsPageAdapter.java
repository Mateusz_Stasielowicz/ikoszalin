package com.globallogic.koszalin.events;

/**
 * Created by mateusz.stasielowicz on 2016-04-28.
 */
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


/**
 * Created by mateuszstasielowicz on 3/15/16.
 */
public class EventsPageAdapter extends FragmentStatePagerAdapter {
    private Context context;

    private static Fragment[] fragments = {
            new EventsContentFragment(),
            new EventsContentFragment(),
            new EventsContentFragment(),
            new EventsContentFragment(),
            new EventsContentFragment(),
            new EventsContentFragment()
    };

    public EventsPageAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }


}
