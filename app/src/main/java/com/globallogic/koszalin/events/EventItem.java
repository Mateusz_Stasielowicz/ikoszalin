package com.globallogic.koszalin.events;

/**
 * Created by mateusz.stasielowicz on 2016-04-28.
 */
public class EventItem {

    private int imageRes;
    private String imageUrl;
    private String title;
    private String content;

    public EventItem(int imageRes, String title, String content) {
        this.imageRes = imageRes;
        this.title = title;
        this.content = content;
    }

    public int getImageRes() {
        return imageRes;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
