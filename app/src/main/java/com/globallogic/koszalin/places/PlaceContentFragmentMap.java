package com.globallogic.koszalin.places;

import android.os.Bundle;
import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.globallogic.koszalin.R;
import com.globallogic.koszalin.places.data.PlaceItem;
import com.globallogic.koszalin.shared.Utils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.Arrays;

/**
 * PlaceContentFragment
 * <hr /> Created by damian.giedrys on 2016-04-28.
 */
public class PlaceContentFragmentMap extends Fragment implements OnMapReadyCallback {

    private static final String TAG = PlaceContentFragmentMap.class.getSimpleName();

    // ui
    private SupportMapFragment mapFragment;

    // vars
    private GoogleMap map;
    private PlaceItem[] model;
    private View rootView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        if (bundle != null) {
            model = (PlaceItem[])bundle.getParcelableArray(PlacesActivity.BUNDLE_KEY_PLACES);
            Log.i(TAG, "Got parcel: " + Arrays.toString(model));
        } else {
            Log.w(TAG, "Empty initial arguments");
        }

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.i(TAG, "onCreateView, savedInstanceState=" + savedInstanceState);

        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }

        try {
            rootView = inflater.inflate(R.layout.fragment_place_content_map, container, false);
        } catch (InflateException e) {
            /* map is already there, just return view as it is */
        }

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        } else {
            Log.i(TAG, "mapFragment is empty");
        }
//        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
//        SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.map = googleMap;

        if (model != null) {
            for (PlaceItem pi : model) {
                if (pi != null) {
                    map.addMarker(PlaceItem.asMarker(pi));
                }
            }
        }
    }
}
