package com.globallogic.koszalin.places.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * PlaceItem
 * <hr /> Created by damian.giedrys on 2016-04-28.
 */
public class PlaceItem implements Parcelable {

    private int imageRes;
    private String imageUrl;

    private String title;
    private String category;
    private String description_short;
    private String description;
    private String address;
    private String telephone;
    private double latitude;
    private double longitude;

    public PlaceItem(int imageRes, String title, String category, String desc, String descShort,
                     String telephone, String address, double latitude, double longitude) {

        this.imageRes = imageRes;
        this.title = title;
        this.description = desc;
        this.description_short = descShort;
        this.telephone = telephone;
        this.address = address;
        this.category = category;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    protected PlaceItem(Parcel in) {
        imageRes = in.readInt();
        imageUrl = in.readString();
        title = in.readString();
        category = in.readString();
        description_short = in.readString();
        description = in.readString();
        address = in.readString();
        telephone = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(imageRes);
        dest.writeString(imageUrl);
        dest.writeString(title);
        dest.writeString(category);
        dest.writeString(description_short);
        dest.writeString(description);
        dest.writeString(address);
        dest.writeString(telephone);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PlaceItem> CREATOR = new Creator<PlaceItem>() {
        @Override
        public PlaceItem createFromParcel(Parcel in) {
            return new PlaceItem(in);
        }

        @Override
        public PlaceItem[] newArray(int size) {
            return new PlaceItem[size];
        }
    };

    public int getImageRes() {
        return imageRes;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription_short() {
        return description_short;
    }

    public void setDescription_short(String description_short) {
        this.description_short = description_short;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "PlaceItem{" +
                "title='" + title + '\'' +
                '}';
    }

    public static MarkerOptions asMarker(PlaceItem item) {
        MarkerOptions ret = null;
        if (item != null) {
//            Log.i("PlaceItem", item.getLatitude() + ", " + item.getLongitude());
            ret = new MarkerOptions()
                    .title(item.getTitle())
                    .position(new LatLng(item.getLatitude(), item.getLongitude()));
        }
        return ret;
    }

}
