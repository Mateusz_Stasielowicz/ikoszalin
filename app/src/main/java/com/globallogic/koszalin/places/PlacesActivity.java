package com.globallogic.koszalin.places;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.globallogic.koszalin.R;
import com.globallogic.koszalin.base.AbstractActivity;
import com.globallogic.koszalin.places.data.PlaceItem;
import com.globallogic.koszalin.shared.Utils;
import com.globallogic.koszalin.shared.view.SimpleFragmentPagerAdapter;

import java.util.Arrays;

/**
 * PlacesActivity
 * <hr /> Created by damian.giedrys on 2016-04-29.
 */
public class PlacesActivity extends AbstractActivity {

    private static final String TAG = PlacesActivity.class.getSimpleName();
    static final String BUNDLE_KEY_PLACE = "bundle_key_place";
    static final String BUNDLE_KEY_PLACES = "bundle_key_places";
    static final String BUNDLE_KEY_TITLE = "bundle_key_title";
    static final String BUNDLE_KEY_SELECTED_INDEX = "bundle_key_selected_index";

    private static final int MODE_UNKNOWN = 0;
    private static final int MODE_SINGLE = 1;
    private static final int MODE_MULTI = 2;

    // ui
    private TabLayout tabLayout;
    private ViewPager viewPager;

    // vars
    private PlaceItem[] model;
    private int mode = MODE_UNKNOWN;
    private String strTitle;

    public PlacesActivity() {
        Log.i(TAG, "PlacesActivity()");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Log.i(TAG, "onCreate");
        mode = MODE_UNKNOWN;
        model = null;

        Intent i = getIntent();
        if (i != null) {
            PlaceItem pi = i.getParcelableExtra(BUNDLE_KEY_PLACE);
            if (pi != null) {
                model = new PlaceItem[]{pi};
                mode = MODE_SINGLE;
            } else {
                strTitle = i.getStringExtra(BUNDLE_KEY_TITLE);
                Parcelable[] items2 = i.getParcelableArrayExtra(BUNDLE_KEY_PLACES);
                if (items2 != null) {
                    model = new PlaceItem[items2.length];
                    for (int j = 0; j < items2.length; j++) {
                        model[j] = (PlaceItem) items2[j];
                    }
                    mode = MODE_MULTI;
                }
            }
            Log.i(TAG, "Got parcel: mode: " + mode + ", model: " + Arrays.toString(model));

            i.getParcelableArrayExtra(BUNDLE_KEY_PLACES);
        } else {
            Log.w(TAG, "Empty initial arguments!");
        }


        setContentView(R.layout.activity_place);
        initToolbar(R.id.toolbar, true);
        if (actionBar != null)
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.toolbar_transparent)));

        if (model != null) {
            if (mode == MODE_SINGLE) {
                actionBar.setTitle(model[0].getTitle());
            } else if (mode == MODE_MULTI) {
                actionBar.setTitle(strTitle);
            }
        }

        viewPager = (ViewPager) findViewById(R.id.view_pager);

        // adapter
        SimpleFragmentPagerAdapter adapter = new SimpleFragmentPagerAdapter(getSupportFragmentManager());
        PlaceContentFragmentMap frMap = new PlaceContentFragmentMap();
        PlaceContentFragmentInfo frInfo = new PlaceContentFragmentInfo();
        PlaceContentFragmentPlaces frList = null;
        if (mode == MODE_MULTI) {
            frList = new PlaceContentFragmentPlaces();
        }

        if (model != null) {
            Bundle args = new Bundle();
            args.putParcelableArray(BUNDLE_KEY_PLACES, model);
            args.putInt(BUNDLE_KEY_SELECTED_INDEX, 0);
            frMap.setArguments(args);
            frInfo.setArguments(args);
            if (mode == MODE_MULTI) {
                frList.setArguments(args);
            }
        }

        adapter.addFragment(frMap, getString(R.string.title_place_fragment_map));
        adapter.addFragment(frInfo, getString(R.string.title_place_fragment_desc));
        if (mode == MODE_MULTI) {
            adapter.addFragment(frList, getString(R.string.title_place_fragment_places));
        }
        if (viewPager != null)
            viewPager.setAdapter(adapter);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        if (tabLayout != null) {
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
            tabLayout.setupWithViewPager(viewPager);
//            tabLayout.getTabAt(0).setIcon(R.drawable.ic_my_location_24dp_white);
//            tabLayout.getTabAt(1).setIcon(R.drawable.ic_location_searching_24dp_white);
            //tabLayout.getTabAt(0).setCustomView(createCustomTabView(R.string.fragment_copy, R.drawable.ic_my_location_24dp_white));
            //tabLayout.getTabAt(1).setCustomView(createCustomTabView(R.string.fragment_paste, R.drawable.ic_location_searching_24dp_white));
        }

    }

}
