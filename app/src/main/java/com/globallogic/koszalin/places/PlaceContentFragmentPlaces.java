package com.globallogic.koszalin.places;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.globallogic.koszalin.R;
import com.globallogic.koszalin.places.data.PlaceItem;
import com.globallogic.koszalin.shared.Utils;
import com.globallogic.koszalin.shared.view.CustomSortedList;

import java.util.Arrays;

/**
 * PlaceContentFragment
 * <hr /> Created by damian.giedrys on 2016-04-28.
 */
public class PlaceContentFragmentPlaces extends Fragment implements AdapterView.OnItemClickListener {

    private static final String TAG = PlaceContentFragmentPlaces.class.getSimpleName();

    private PlaceItem[] model;
//    private ArrayAdapter<PlaceItem> adapter;
    private PlacesAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        if (bundle != null) {

//            Parcelable[] items2 = bundle.getParcelableArray(PlacesActivity.BUNDLE_KEY_PLACES);
//            if (items2 != null) {
//                model = new PlaceItem[items2.length];
//                for (int j=0; j<items2.length ; j++) {
//                    model[j] = (PlaceItem)items2[j];
//                }
//            }
            model = (PlaceItem[])bundle.getParcelableArray(PlacesActivity.BUNDLE_KEY_PLACES);

            Log.i(TAG, "Got parcel: " + Arrays.toString(model));
        } else {
            Log.w(TAG, "Empty initial arguments");
        }

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView, savedInstanceState="+savedInstanceState);
        return inflater.inflate(R.layout.fragment_place_content_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onViewCreated, savedInstanceState="+savedInstanceState);
        super.onViewCreated(view, savedInstanceState);

//        ListView listView = (ListView) view.findViewById(R.id.listView);
//        adapter = new ArrayAdapter<PlaceItem>(getContext(), R.layout.view_place_info, model) {
//            @Override
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = convertView;
//                if (v == null) {// tworzy view z xml'a
//                    LayoutInflater vi = LayoutInflater.from(getContext());
//                    v = vi.inflate(R.layout.view_place_info, null);
//                    v.setBackgroundResource(R.drawable.card_view_bg);
//                }
//
//                PlaceItem sr = getItem(position);
//                PlacesUtils.fillPlaceItem(getContext(), sr, v);
//                return v;
//            }
//        };
//        listView.setOnItemClickListener(this);
//        listView.setAdapter(adapter);

        CustomSortedList dataset = new CustomSortedList<>(PlaceItem.class);
        dataset.addAll(model);
        adapter = new PlacesAdapter(dataset, getContext(), this);

        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.places_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position >= 0) {
            PlaceItem item = adapter.getItem(position);
            Toast.makeText(getContext(), item.toString(), Toast.LENGTH_LONG).show();
        }
    }
}
