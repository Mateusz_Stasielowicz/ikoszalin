package com.globallogic.koszalin.places;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.globallogic.koszalin.MainActivity;
import com.globallogic.koszalin.R;
import com.globallogic.koszalin.AppUtils;
import com.globallogic.koszalin.base.AbstractFragment;
import com.globallogic.koszalin.places.data.PlaceItem;
import com.globallogic.koszalin.subcategory.SubcategoryItem;
import com.globallogic.koszalin.subcategory.SubcategoryRecyclerView;
import com.globallogic.koszalin.shared.view.CustomSortedList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mateusz.stasielowicz on 2016-04-28.
 */
public class PlacesContentFragment extends AbstractFragment implements AdapterView.OnItemClickListener {

    private static final String TAG = PlacesContentFragment.class.getSimpleName();

    private RecyclerView recyclerView;
    private SubcategoryRecyclerView subcategoryRecyclerView;
    private PlacesAdapter placesAdapter;
    private CustomSortedList<PlaceItem> dataset;
    private List<SubcategoryItem> subcategoryDataset;

    private int positon;

    private static final String KEY_POSITION = "KEY_POSITION";

    public static PlacesContentFragment newInstance(int position) {

        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        PlacesContentFragment fragment = new PlacesContentFragment();
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(KEY_POSITION)) {
                positon = getArguments().getInt(KEY_POSITION);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_places_content, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        subcategoryRecyclerView = (SubcategoryRecyclerView) view.findViewById(R.id.subcategory_recyclerview);
        recyclerView = (RecyclerView) view.findViewById(R.id.places_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        dataset = new CustomSortedList<>(PlaceItem.class);
        subcategoryDataset = new ArrayList<>();

        placesAdapter = new PlacesAdapter(dataset, getContext(), this);
        recyclerView.setAdapter(placesAdapter);
        recyclerView.setHasFixedSize(true);


        initDataset();

    }

    private void initDataset() {
        dataset.clear();
        for (int i=0 ; i<10 ; i++) {
            dataset.add(createStub(i));
        }
        placesAdapter.notifyDataSetChanged();

        subcategoryDataset.clear();
        subcategoryDataset.add(new SubcategoryItem(0, "Podkategoria 1"));
        subcategoryDataset.add(new SubcategoryItem(1, "Podkategoria 2"));
        subcategoryDataset.add(new SubcategoryItem(2, "Podkategoria 3"));
        subcategoryDataset.add(new SubcategoryItem(3, "Podkategoria 4"));
        subcategoryDataset.add(new SubcategoryItem(4, "Podkategoria 5"));
        subcategoryDataset.add(new SubcategoryItem(5, "Podkategoria 6"));
        subcategoryDataset.add(new SubcategoryItem(6, "Podkategoria 7"));

        subcategoryRecyclerView.setDataset(subcategoryDataset);
    }

    static PlaceItem createStub(int no) {

        double[] lanLon = AppUtils.DEV_getLocationStub();

        PlaceItem ret = new PlaceItem(R.drawable.logo_koszalin_tr_100alpha, "Miejsce " + no,
                "Category" + no, "Lorem ipsum dolor sit amet, viris iudico timeam vix ea, cu mei commodo albucius. Dicant efficiendi no usu, ei est sapientem gubergren, iusto paulo scripserit eum et. Est munere fabulas fastidii no. Vis malis fierent ne, at probo aliquando vim. Ex cum placerat tractatos iracundia, est in ubique interesset.",
                "A short description of a place", "+48 111 222 " + String.format("%03d", no),
                "Address " + no, lanLon[0], lanLon[1]);
        return ret;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PlaceItem item = dataset.get(position);
        Log.i(TAG, "Clicked: position=" + position + ", id=" + id + ", item=" + item);
        //Log.i(TAG, "getActivity()=" + getActivity());

        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {

//            PlaceContentFragment fr = new PlaceContentFragment();
//            Bundle args = new Bundle();
//            args.putParcelable(PlaceContentFragment.BUNDLE_KEY_PLACE, item);
//            fr.setArguments(args);
//            activity.clearAllTabs();
//            activity.replaceFragment(fr, MainActivity.FRAGMENT_PLACES_CONTENT);

            Log.i(TAG, "Starting PlacesActivity...");
            Intent intent = new Intent(getActivity(), PlacesActivity.class);
            intent.putExtra(PlacesActivity.BUNDLE_KEY_PLACE, item);
            startActivity(intent);
//            getActivity().overridePendingTransition(R.anim.launch_animation, R.anim.exit_animation);

        } else {
            Log.w(TAG, "Empty parent!");
        }

    }

}
