package com.globallogic.koszalin.places;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.globallogic.koszalin.R;
import com.globallogic.koszalin.places.data.PlaceItem;

/**
 * PlacesUtils
 * <hr /> Created by damian.giedrys on 2016-05-04.
 */
public class PlacesUtils {

    private PlacesUtils() {
    }

    public static void fillPlaceItem(Context ctx, PlaceItem model, View view) {

        // init model
        if (model != null) {
            TextView txtView = (TextView) view.findViewById(R.id.place_item_title);
            txtView.setText(model.getTitle());

            txtView = (TextView) view.findViewById(R.id.place_item_address);
            txtView.setText(Html.fromHtml(ctx.getString(R.string.format_address, model.getAddress())));

            txtView = (TextView) view.findViewById(R.id.place_item_telephone);
            txtView.setText(Html.fromHtml(ctx.getString(R.string.format_telephone, model.getTelephone())));

            txtView = (TextView) view.findViewById(R.id.place_item_category);
            txtView.setText(model.getCategory());

            txtView = (TextView) view.findViewById(R.id.place_item_description_long);
            if (txtView != null) {
                txtView.setText(model.getDescription());
            }

            ImageView imgView = (ImageView) view.findViewById(R.id.place_item_image);
            if (model.getImageRes() > 0) {
                imgView.setImageResource(model.getImageRes());
            }
        }
    }
}
