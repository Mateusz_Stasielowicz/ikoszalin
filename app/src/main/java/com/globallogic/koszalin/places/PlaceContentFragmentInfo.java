package com.globallogic.koszalin.places;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.globallogic.koszalin.R;
import com.globallogic.koszalin.shared.Utils;
import com.globallogic.koszalin.places.data.PlaceItem;

import java.util.Arrays;

/**
 * PlaceContentFragment
 * <hr /> Created by damian.giedrys on 2016-04-28.
 */
public class PlaceContentFragmentInfo extends Fragment {

    private static final String TAG = PlaceContentFragmentInfo.class.getSimpleName();

    private PlaceItem[] model;
    private int selectedIdx;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        if (bundle != null) {
            model = (PlaceItem[])bundle.getParcelableArray(PlacesActivity.BUNDLE_KEY_PLACES);
            Log.i(TAG, "Got parcel: " + Arrays.toString(model));

            selectedIdx = bundle.getInt(PlacesActivity.BUNDLE_KEY_SELECTED_INDEX);
            Log.i(TAG, "selectedIdx: " + selectedIdx);

        } else {
            Log.w(TAG, "Empty initial arguments");
        }

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView, savedInstanceState="+savedInstanceState);
        return inflater.inflate(R.layout.fragment_place_content_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onViewCreated, savedInstanceState="+savedInstanceState);
        super.onViewCreated(view, savedInstanceState);

        // init model
//        if (model != null) {
//            TextView txtView = (TextView)view.findViewById(R.id.place_item_title);
//            txtView.setText(model.getTitle());
//
//            txtView = (TextView)view.findViewById(R.id.place_item_address);
//            txtView.setText(Html.fromHtml(getString(R.string.format_address, model.getAddress())));
//
//            txtView = (TextView)view.findViewById(R.id.place_item_telephone);
//            txtView.setText(Html.fromHtml(getString(R.string.format_telephone, model.getTelephone())));
//
//            txtView = (TextView)view.findViewById(R.id.place_item_category);
//            txtView.setText(model.getCategory());
//
//            txtView = (TextView)view.findViewById(R.id.place_item_description_long);
//
//            txtView.setText(model.getDescription());
//
//            ImageView imgView = (ImageView)view.findViewById(R.id.place_item_image);
//            if (model.getImageRes() > 0) {
//                imgView.setImageResource(model.getImageRes());
//            }
//        }
        PlacesUtils.fillPlaceItem(getContext(), model[selectedIdx], view);
    }



}
