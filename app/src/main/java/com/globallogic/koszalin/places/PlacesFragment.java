package com.globallogic.koszalin.places;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.globallogic.koszalin.shared.logic.HasDrawerTitle;
import com.globallogic.koszalin.MainActivity;
import com.globallogic.koszalin.R;

/**
 * Created by mateusz.stasielowicz on 2016-04-28.
 */
public class PlacesFragment extends Fragment implements HasDrawerTitle {

    private static final String TAG = PlacesFragment.class.getSimpleName();
    private ViewPager viewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_places,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);
//        ((MainActivity)getActivity()).setTitle("Miejsca");
        ((MainActivity)getActivity()).tabLayout.removeAllTabs();
        TabLayout tabLayout = ((MainActivity)getActivity()).tabLayout;
        tabLayout.addTab(tabLayout.newTab().setText("Zabytki"));
        tabLayout.addTab(tabLayout.newTab().setText("Gastronomia"));
        tabLayout.addTab(tabLayout.newTab().setText("Noclegi"));
        tabLayout.addTab(tabLayout.newTab().setText("Ciekawe"));
        tabLayout.addTab(tabLayout.newTab().setText("Kultura"));
        tabLayout.addTab(tabLayout.newTab().setText("Inne"));

        PlacesPageAdapter placesPageAdapter = new PlacesPageAdapter(getChildFragmentManager(), getContext());

        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        viewPager.setAdapter(placesPageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
//                selectedNavOption = tab.getPosition();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_places,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_show_all_cat_places:
                if (viewPager != null) {
                    // TODO stub data

                    int iCnt = 6;
                    Parcelable[] items = new Parcelable[iCnt];
                    for (int i =0 ; i<iCnt ; i++) {
                        items[i] = PlacesContentFragment.createStub(i);
                    }

                    Log.i(TAG, "Starting PlacesActivity mode MULTI ...");
                    Intent intent = new Intent(getActivity(), PlacesActivity.class);
                    intent.putExtra(PlacesActivity.BUNDLE_KEY_PLACES, items);
                    intent.putExtra(PlacesActivity.BUNDLE_KEY_TITLE, "Gastronomia");
                    startActivity(intent);
                }

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public String getDrawerTitle(@NonNull Context ctx) {
        return ctx.getString(R.string.fragment_places);
    }
}
