package com.globallogic.koszalin.places;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.globallogic.koszalin.R;
import com.globallogic.koszalin.places.data.PlaceItem;
import com.globallogic.koszalin.shared.view.AbstractRecyclerViewAdapter;
import com.globallogic.koszalin.shared.view.CustomSortedList;

/**
 * Created by mateusz.stasielowicz on 2016-04-28.
 */
public class PlacesAdapter extends AbstractRecyclerViewAdapter<PlaceItem> {

    private static final String TAG = PlacesAdapter.class.getSimpleName();

    public PlacesAdapter(CustomSortedList<PlaceItem> mDataset, Context mContext) {
        super(mDataset, mContext);
    }

    public PlacesAdapter(CustomSortedList<PlaceItem> mDataset, Context mContext, AdapterView.OnItemClickListener onClickListener) {
        super(mDataset, mContext, onClickListener);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View mainView;
        protected ImageView icon;
        protected TextView title;
//        protected TextView content;
        protected TextView category;
        protected TextView address;
        protected TextView telephone;

        public ViewHolder(View v) {
            super(v);
            this.mainView = v;
            this.icon = (ImageView) v.findViewById(R.id.place_item_image);
            this.title = (TextView) v.findViewById(R.id.place_item_title);
//            this.content = (TextView) v.findViewById(R.id.place_item_);
            this.category = (TextView) v.findViewById(R.id.place_item_category);
            this.address = (TextView) v.findViewById(R.id.place_item_address);
            this.telephone = (TextView) v.findViewById(R.id.place_item_telephone);
        }
    }

    @Override
    public PlacesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_item_places, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder h, int position) {
        PlaceItem item = getItem(position);
        PlacesAdapter.ViewHolder holder = (ViewHolder) h;
        if (mOnClickListener != null) {
            holder.mainView.setTag(holder);
            holder.mainView.setOnClickListener(this);
        }
        Log.i(TAG, "onBindViewHolder: " + item);
        if (item == null) {
            Log.i(TAG, "onBindViewHolder: empty item!");
            return;
        }

        if (item.getImageRes() > 0) {
            holder.icon.setImageResource(item.getImageRes());
        } else if (item.getImageUrl() != null) {
            // TODO
        }

        holder.title.setText(item.getTitle());
//        holder.content.setText(item.getDescription());
        holder.title.setText(item.getTitle());
        holder.category.setText(item.getCategory());
        holder.address.setText(Html.fromHtml(mContext.getString(R.string.format_address, item.getAddress())));
        holder.telephone.setText(Html.fromHtml(mContext.getString(R.string.format_telephone, item.getTelephone())));
    }
}
