package com.globallogic.koszalin.places;

/**
 * Created by mateusz.stasielowicz on 2016-04-28.
 */
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


/**
 * Created by mateuszstasielowicz on 3/15/16.
 */
public class PlacesPageAdapter extends FragmentStatePagerAdapter {
    private Context context;

    private static Fragment[] fragments = {
            PlacesContentFragment.newInstance(0),
            PlacesContentFragment.newInstance(1),
            PlacesContentFragment.newInstance(2),
            PlacesContentFragment.newInstance(3),
            PlacesContentFragment.newInstance(4),
            PlacesContentFragment.newInstance(5)
    };

    public PlacesPageAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

}
