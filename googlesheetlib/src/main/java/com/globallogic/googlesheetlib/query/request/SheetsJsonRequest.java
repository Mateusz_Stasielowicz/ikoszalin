
package com.globallogic.googlesheetlib.query.request;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.globallogic.googlesheetlib.query.response.ResponseError;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class SheetsJsonRequest extends JsonObjectRequest {
    private String TAG = "SheetsJsonRequest";

    public SheetsJsonRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
        setRetryPolicy(new DefaultRetryPolicy(
                1000 * 15,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            try {
                ResponseError responseError = new Gson().fromJson(jsonString, ResponseError.class);

                if (responseError != null && responseError.message != null) {
                    Log.e(TAG, responseError.message);
                    return Response.error(new VolleyError(responseError.message));
                }
            } catch (IllegalStateException ee) {
                return Response.error(new VolleyError(ee));
            } catch (JsonSyntaxException eee) {
                return Response.error(new VolleyError(eee));
            }
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }

    }

    @Override
    public String toString() {
        return "[" + RequestHelper.requestCodeToString(getMethod()) + "] Body:" + new String(getBody()) + ", " + super.toString();
    }
}