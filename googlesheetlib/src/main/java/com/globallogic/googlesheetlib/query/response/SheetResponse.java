
package com.globallogic.googlesheetlib.query.response;


import java.util.ArrayList;
import java.util.List;

public class SheetResponse<T>{
    private float version;
    private String status;
    private List<T> response = new ArrayList<T>();


    public float getVersion() {
        return version;
    }

    public void setVersion(float version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<T> getResponse() {
        return response;
    }

    public void setResponse(List<T> response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "SheetResponse{" +
                "version=" + version +
                ", status='" + status + '\'' +
                ", response=" + response +
                '}';
    }
}