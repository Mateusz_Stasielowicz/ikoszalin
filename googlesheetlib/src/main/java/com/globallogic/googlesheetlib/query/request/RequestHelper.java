package com.globallogic.googlesheetlib.query.request;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.globallogic.googlesheetlib.query.Utils;
import com.globallogic.googlesheetlib.query.response.SheetResponse;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;


public class RequestHelper {
    private static final String TAG = "RequestHelper";
    private String token;
    private static String URL = "https://script.google.com/macros/s/";
    private static String URL_TO_IMAGE = "https://drive.google.com/thumbnail?id=%s&sz=w%s";
    private static String URL_ARGS_DEV = "/dev";
    private static String URL_ARGS_RELEASE = "/exec";
    private String scriptKey;
    private boolean testServer = false;
    private double[] localization;

    public RequestHelper(String scriptKey, String token, double[] localization, boolean isTestServer) {
        this.scriptKey = scriptKey;
        this.token = token;
        this.testServer = isTestServer;
        this.localization = localization;
    }

    public RequestHelper(String scriptKey, String token, double[] localization) {
        this.scriptKey = scriptKey;
        this.token = token;
        this.localization = localization;
    }


    public <T> void query(Context context, RequestItem requestItem, final Class clazz, final ResponseListener responseListener) {
        if(requestItem == null){
            return;
        }
        requestItem.setToken(token);
        requestItem.setLocalization(localization);

//        SheetsJsonRequest jsObjRequest = new SheetsJsonRequest(Request.Method.POST, this.combineURL(),params, new Response.Listener<SheetResponse>() {
//            public void onResponse(SheetResponse response) {
//                Log.i("RequestHelper", "Response: " + response.toString());
//                responseListener.onSuccess(new GoogleSheetParser().fromGoogleSheetTable(response.getResponseTable(),clazz));
//            }
//        }, new ErrorListener() {
//            public void onErrorResponse(VolleyError error) {
//                Log.i("RequestHelper", "VolleyError: " + error.toString());
//                responseListener.onError(error);
//
//            }
//        });

        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(new Gson().toJson(requestItem));
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        SheetsJsonRequest jsObjRequest = new SheetsJsonRequest(Request.Method.POST, this.combineURL(), jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "<< Response: " + response.toString());
//                Type listType = new TypeToken<List<T>>(){}.getType();
                SheetResponse<T> sheetResponse = (SheetResponse<T>) Utils.getGson().fromJson(response.toString(), clazz);
                responseListener.onSuccess(sheetResponse);
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "VolleyError: " + error.toString());
                if (error.networkResponse != null) {
                    Log.i("RequestHelper", "VolleyErrorCode: " + error.networkResponse.statusCode);
                } else {
                    Log.i("RequestHelper", "NoErrorCode");
                }

                responseListener.onError(error);

            }
        });

        VolleyRequestQueue.getInstance(context).addToRequestQueue(jsObjRequest);
//        Log.i(TAG, "Request sent: url : " + jsObjRequest.getUrl()+" content : "+jsonObject);
        Log.i(TAG, ">> Request sent: " + jsObjRequest);
    }


    public String getImageUrl(String id, int width) {
        if (id == null) {
            return null;
        }
        String formatted = String.format(URL_TO_IMAGE, new String[]{id, String.valueOf(width)});
        return formatted;
    }

    private String combineURL() {
        if (testServer) {
            return URL + scriptKey + URL_ARGS_DEV;
        } else {
            return URL + scriptKey + URL_ARGS_RELEASE;
        }
    }

    public static String requestCodeToString(int code) {
//        int DEPRECATED_GET_OR_POST = -1;
//        int GET = 0;
//        int POST = 1;
//        int PUT = 2;
//        int DELETE = 3;
//        int HEAD = 4;
//        int OPTIONS = 5;
//        int TRACE = 6;
//        int PATCH = 7;
        switch (code) {
            case Request.Method.POST:
                return "POST";
            case Request.Method.GET:
                return "GET";
            case Request.Method.PUT:
                return "PUT";
            case Request.Method.DELETE:
                return "DELETE";
            case Request.Method.HEAD:
                return "HEAD";
            case Request.Method.OPTIONS:
                return "OPTIONS";
            case Request.Method.TRACE:
                return "TRACE";
            case Request.Method.PATCH:
                return "PATCH";
            default:
                return "?";
        }
    }

}