//
//package com.globallogic.googlesheetlib.query.request;
//
//import android.util.Log;
//
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.NetworkResponse;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.VolleyError;
//import com.android.volley.Response.ErrorListener;
//import com.android.volley.toolbox.HttpHeaderParser;
//import com.globallogic.googlesheetlib.query.response.ResponseError;
//import com.globallogic.googlesheetlib.query.response.SheetResponse;
//import com.globallogic.googlesheetlib.query.response.Table;
//import com.google.gson.Gson;
//import com.google.gson.JsonSyntaxException;
//
//import java.io.UnsupportedEncodingException;
//import java.util.Map;
//
//public class SheetsJsonRequest extends Request<String> {
//    private Map<String, String> mParams;
//    private Response.Listener<String> listener;
//    private String TAG = "SheetsJsonRequest";
//
//    public SheetsJsonRequest(int method, String url, Map<String, String> params, Response.Listener<String> listener, ErrorListener errorListener) {
//        super(method, url, errorListener);
//        this.mParams = params;
//        this.listener = listener;
//        setRetryPolicy(new DefaultRetryPolicy(
//                1000*15,
//                3,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//    }
//
//
//    protected Response<String> parseNetworkResponse(NetworkResponse response) {
//        try {
//            String e = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
//            Log.i(TAG, "raw request " + e);
////            e = e.replace("/*O_o*/", "");
////            e = e.replace("google.visualization.Query.setResponse(", "");
////            e = e.replace(");", "");
//            try {
//                ResponseError responseError = new Gson().fromJson(e, ResponseError.class);
//
//                if (responseError != null && responseError.message != null) {
//                    Log.e(TAG, responseError.message);
//                    return Response.error(new VolleyError(responseError.message));
//                }
//            } catch (IllegalStateException ee) {
//            }catch (JsonSyntaxException eee){}
////            SheetResponse<T> sheetResponse = new Gson().fromJson(e, SheetResponse.class);
//
//            return Response.success(e, HttpHeaderParser.parseCacheHeaders(response));
//        } catch (UnsupportedEncodingException var4) {
//            var4.printStackTrace();
//            return Response.error(new VolleyError(var4));
//        }
//    }
//
//
//    @Override
//    protected void deliverResponse(String response) {
//        listener.onResponse(response);
//    }
//
//    @Override
//    protected Map<String, String> getParams() {
//        Log.i("TEST", "getParams = " + mParams.toString());
//        return mParams;
//    }
//
//    @Override
//    public String toString() {
//        return "[" + RequestHelper.requestCodeToString(getMethod()) + ']' + super.toString();
//    }
//}