package com.globallogic.googlesheetlib.query.request;

import com.android.volley.VolleyError;
import com.globallogic.googlesheetlib.query.response.SheetResponse;

/**
 * Created by mateusz.stasielowicz on 2016-05-16.
 */
public interface ResponseListener {

    void onSuccess(SheetResponse response);
    void onError(VolleyError error);

}
