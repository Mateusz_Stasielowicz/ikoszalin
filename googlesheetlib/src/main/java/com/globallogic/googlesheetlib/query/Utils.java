package com.globallogic.googlesheetlib.query;

import com.globallogic.googlesheetlib.query.response.BooleanSerializer;
import com.globallogic.googlesheetlib.query.response.DateSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

/**
 * Created by mateusz.stasielowicz on 2016-05-18.
 */
public class Utils {

    public static Gson getGson(){
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapter(Boolean.class, new BooleanSerializer());
        b.registerTypeAdapter(Boolean.TYPE, new BooleanSerializer());
        b.registerTypeAdapter(Date.class, new DateSerializer());

        return b.create();
    }

}
