package com.globallogic.googlesheetlib.query;

import android.util.Log;

import com.globallogic.googlesheetlib.query.response.Cols;
import com.globallogic.googlesheetlib.query.response.GoogleSheetTable;
import com.globallogic.googlesheetlib.query.response.Table;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by mateusz.stasielowicz on 2016-05-16.
 */
public class GoogleSheetParser {

    private static final String TAG = "GoogleSheetParser";

    public <T> List<T> fromGoogleSheetTable(Table table, Class<T> tClass) {
        if (table == null) {
            return null;
        }
        List<T> classObjectList = new ArrayList<>();

        for (int row = 0; row < table.getRows().size(); row++) {
            Constructor<T> ctor = null;
            T classObject;
            try {
                ctor = tClass.getConstructor();
                classObject = ctor.newInstance();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return null;
            } catch (InstantiationException e) {
                e.printStackTrace();
                return null;
            } catch (InvocationTargetException e) {
                e.printStackTrace();
                return null;
            }

            Field[] fields = classObject.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(GoogleSheetTable.class)) {

                    Annotation annotation = field.getAnnotation(GoogleSheetTable.class);
                    GoogleSheetTable googleSheetTable = (GoogleSheetTable) annotation;
                    parseField(table, field, googleSheetTable, row, classObject);
                }
            }
            classObjectList.add(classObject);
        }

        return classObjectList;
    }


    private void parseField(Table table, Field field, GoogleSheetTable googleSheetTable, int row, Object classObject) {
        for (int col = 0; col < table.getCols().size(); col++) {
            Cols cols = table.getCols().get(col);
            String label = null;
            if (googleSheetTable.columnLabel() != null) {
                label = cols.getLabel();
            } else if (googleSheetTable.columnId() != null) {
                label = cols.getId();
            }

            if (label != null) {
                if (label.equals(googleSheetTable.columnLabel()) || label.equals(googleSheetTable.columnId())) {
                    field.setAccessible(true);
                    try {
                        if (table.getRows().get(row).getC().get(col) == null || table.getRows().get(row).getC().get(col).v == null) {
                            field.set(classObject, null);
                        } else {
                            checkFieldType(field, classObject, table, row, col);
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (IllegalArgumentException e) {
                        Object value = table.getRows().get(row).getC().get(col).v;
                        Log.e(TAG, "IllegalArgumentException Class name = " + classObject.getClass().getSimpleName() + " Field name = " + field.getName() + " row = " + row + " value = " + value);
                        e.printStackTrace();
                    }
                    break;
                }
            } else {
                Log.w(TAG, "Field " + field.getName() + " in class " + classObject.getClass().getSimpleName() + " has missing GoogleSheetTable annotation");
            }
        }
    }

    private void checkFieldType(Field field, Object classObject, Table table, int row, int col) throws IllegalAccessException {
        if (field.getType().isAssignableFrom(Date.class)) {
            if (table.getCols().get(col).getType().equals("datetime")) {
                field.set(classObject, getDateFromGoogleSheetDate(table.getRows().get(row).getC().get(col).v));
            }
        } else if (field.getType().isAssignableFrom(Integer.TYPE) || field.getType().isAssignableFrom(Integer.class)) {
            try {
                field.set(classObject, Double.valueOf(table.getRows().get(row).getC().get(col).v).intValue());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        } else if (field.getType().isAssignableFrom(Float.TYPE) || field.getType().isAssignableFrom(Float.class)) {
            try {
                field.set(classObject, Double.valueOf(table.getRows().get(row).getC().get(col).v).floatValue());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        } else if (field.getType().isAssignableFrom(Double.TYPE) || field.getType().isAssignableFrom(Double.class)) {
            try {
                field.set(classObject, Double.valueOf(table.getRows().get(row).getC().get(col).v));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        } else if (field.getType().isAssignableFrom(Boolean.class) || field.getType().isAssignableFrom(Boolean.TYPE)) {
            String value = table.getRows().get(row).getC().get(col).v;
            field.set(classObject, Boolean.getBoolean(value));
            if (value != null) {
                try {
                    Double v = Double.valueOf(value);
                    if (v == 0) {
                        field.set(classObject, false);
                    } else {
                        field.set(classObject, true);
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        } else {
            field.set(classObject, table.getRows().get(row).getC().get(col).v);
        }
    }

    private Date getDateFromGoogleSheetDate(String value) {
        if (value == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        if (value.contains("Date")) {
            value = value.replace("Date(", "");
            value = value.replace(")", "");
            String dateParts[] = value.split(",");
            if (dateParts != null) {
                calendar.set(Integer.parseInt(dateParts[0]),
                        Integer.parseInt(dateParts[1]),
                        Integer.parseInt(dateParts[2]),
                        Integer.parseInt(dateParts[3]),
                        Integer.parseInt(dateParts[4]),
                        Integer.parseInt(dateParts[5]));
                return calendar.getTime();
            }
        }
        return null;
    }
}
