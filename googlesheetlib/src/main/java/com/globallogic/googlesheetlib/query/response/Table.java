package com.globallogic.googlesheetlib.query.response;

import java.util.List;

public class Table {

    private List<Cols> cols;
    private List<Rows> rows;

    public Table() {
    }

    public List<Cols> getCols() {
        return this.cols;
    }

    public void setCols(List<Cols> cols) {
        this.cols = cols;
    }

    public List<Rows> getRows() {
        return this.rows;
    }

    public void setRows(List<Rows> rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        return "Table{" +
                "cols=" + cols +
                ", rows=" + rows +
                '}';
    }
}
