package com.globallogic.googlesheetlib.query.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Arrays;
import java.util.Locale;

/**
 * Created by mateusz.stasielowicz on 2016-06-02.
 */
public class RequestItem {

    private String token;
    private String method;
    private Localization localization;
    private Integer category;
    private String lang;
    private int[] ids;



    public RequestItem(@NonNull String method, @Nullable Integer category, @Nullable int[] ids) {
        this.method = method;
        this.category = category;
        this.ids = ids;
        this.lang = Locale.getDefault().getLanguage();
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public void setLocalization(Localization localization) {
        this.localization = localization;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Localization getLocalization() {
        return localization;
    }

    public void setLocalization(double[] localization) {
        if (localization != null && localization.length == 2) {
            this.localization = new Localization(localization);
        } else {
            this.localization = null;
        }
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public int[] getIds() {
        return ids;
    }

    public void setIds(int[] ids) {
        this.ids = ids;
    }

    @Override
    public String toString() {
        return "RequestItem{" +
                "token='" + token + '\'' +
                ", method='" + method + '\'' +
                ", localization=" + localization +
                ", category='" + category + '\'' +
                ", ids=" + Arrays.toString(ids) +
                '}';
    }

    public class Localization {
        private double lat;
        private double lng;

        public Localization(double lat, double lng) {
            this.lat = lat;
            this.lng = lng;
        }

        public Localization(double[] latLng) {
            if(latLng != null && latLng.length > 1) {
                this.lat = latLng[0];
                this.lng = latLng[1];
            }
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }

        @Override
        public String toString() {
            return "Localization{" +
                    "lat=" + lat +
                    ", lng=" + lng +
                    '}';
        }
    }
}
