package com.globallogic.googlesheetlib.query.response;

/**
 * Created by mateusz.stasielowicz on 2016-05-16.
 */
public @interface GoogleSheetTable {

    String columnLabel();
    String columnId();
}
