
package com.globallogic.googlesheetlib.query.response;

import java.util.List;

public class Rows {
    private List<C> c;

    public Rows() {
    }

    public List<C> getC() {
        return this.c;
    }

    public void setC(List<C> c) {
        this.c = c;
    }

    public class C {
        public String v;
        public String f;

        @Override
        public String toString() {
            return "C{" +
                    "v='" + v + '\'' +
                    ", f='" + f + '\'' +
                    '}';
        }
    }


    @Override
    public String toString() {
        return "Rows{" +
                "c=" + c +
                '}';
    }
}