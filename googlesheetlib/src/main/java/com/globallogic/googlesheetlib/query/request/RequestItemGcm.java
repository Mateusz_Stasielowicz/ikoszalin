package com.globallogic.googlesheetlib.query.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by mateusz.stasielowicz on 2016-06-02.
 */
public class RequestItemGcm extends RequestItem {

    private String clientAppId;
    private String registrationToken;
    private static final String sendGcmTokenIdMethod = "sendGcmTokenId";

    public RequestItemGcm(@NonNull String clientAppId, @NonNull String reqistrationToken) {
        super(sendGcmTokenIdMethod, null, null);
        this.clientAppId = clientAppId;
        this.registrationToken = reqistrationToken;
    }
}
