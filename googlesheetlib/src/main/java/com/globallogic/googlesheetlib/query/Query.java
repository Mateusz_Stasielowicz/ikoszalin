package com.globallogic.googlesheetlib.query;

import android.content.Context;
import android.net.UrlQuerySanitizer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.globallogic.googlesheetlib.query.request.RequestHelper;
import com.globallogic.googlesheetlib.query.request.RequestItem;
import com.globallogic.googlesheetlib.query.request.RequestItemGcm;
import com.globallogic.googlesheetlib.query.request.ResponseListener;
import com.globallogic.googlesheetlib.query.response.SheetResponse;

import java.lang.reflect.Type;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Query {

//    private String token;
    private RequestHelper requestHelper;
    private Context context;

    public Query(Context context, String scriptKey, String token, double[] localization) {
//        this.token = token;
        this.context = context;
        requestHelper = new RequestHelper(scriptKey, token,localization,false);
    }
    public Query(Context context, String scriptKey, String token, double[] localization,boolean isTestServer) {
//        this.token = token;
        this.context = context;
        requestHelper = new RequestHelper(scriptKey, token,localization,isTestServer);
    }

    public void getThingsTodo(Class clazz, ResponseListener listener) {
        RequestItem requestItem = new RequestItem("getThingsToDo",null,null);
        requestHelper.query(context, requestItem, clazz, listener);
    }

    public void getCategoryItems(Class clazz, int categoryId, ResponseListener listener) {
        RequestItem requestItem = new RequestItem("getData",categoryId,null);
        requestHelper.query(context, requestItem, clazz, listener);
    }

    public void getCategories(Class clazz, ResponseListener listner) {
        RequestItem requestItem = new RequestItem("getCategories",null,null);
        requestHelper.query(context, requestItem, clazz, listner);
    }

    public void getOutdoors(Class clazz, ResponseListener listner) {
        RequestItem requestItem = new RequestItem("getOutdoor",null,null);
        requestHelper.query(context, requestItem, clazz, listner);
    }

    public void getEvents(Class clazz, ResponseListener listner) {
        RequestItem requestItem = new RequestItem("getEvents",null,null);
        requestHelper.query(context, requestItem, clazz, listner);
    }

    public void getRecords(Class clazz, int[] ids, ResponseListener listner) {
        RequestItem requestItem = new RequestItem("getDataById",null,ids);
        requestHelper.query(context, requestItem, clazz, listner);
    }

    public void sendGcmReqistrationToken(Class clazz, RequestItemGcm requestItemGcm, ResponseListener listner) {
        requestHelper.query(context, requestItemGcm, clazz, listner);
    }

    /**
     * Converts google drive image id to url with given width in pixels. If URL is passed as parameter
     * it is checked if that is google drive url or not - if not method returns that url.
     * @param idOrUrl id to google drive image or URL to google drive image or URL to image in the Internet
     * @param width width in pixels
     * @return URL to image
     */
    public String getImageUrl(String idOrUrl, int width){
        if(idOrUrl == null){
            return null;
        }
        if(idOrUrl.contains("http") || idOrUrl.contains("https")){
            if(idOrUrl.contains("https://drive.google.com")){
                UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(idOrUrl);
                String id = sanitizer.getValue("id");
                return requestHelper.getImageUrl(id,width);
            }else{
                return idOrUrl;
            }
        }else{
            return requestHelper.getImageUrl(idOrUrl,width);
        }
    }

}
