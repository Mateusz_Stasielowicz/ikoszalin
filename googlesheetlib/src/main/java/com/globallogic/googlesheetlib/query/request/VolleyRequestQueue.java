package com.globallogic.googlesheetlib.query.request;

import android.content.Context;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleyRequestQueue {
    private static VolleyRequestQueue mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    private VolleyRequestQueue(Context context) {
        mCtx = context;
        this.mRequestQueue = this.getRequestQueue();
    }

    public static synchronized VolleyRequestQueue getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new VolleyRequestQueue(context);
        }

        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if(this.mRequestQueue == null) {
            this.mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }

        return this.mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        this.getRequestQueue().add(req);
    }
}