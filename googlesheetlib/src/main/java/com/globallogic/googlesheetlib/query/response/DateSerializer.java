package com.globallogic.googlesheetlib.query.response;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by mateusz.stasielowicz on 2016-05-18.
 */
public class DateSerializer implements JsonDeserializer<Date>, JsonSerializer<Date> {

    private final static DateFormat df;
    private final static TimeZone tz_utc;
//    private final static TimeZone tz_local;
//    private static final long offset;

    static {
        tz_utc = TimeZone.getTimeZone("UTC");
        df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(tz_utc);
//        tz_local = TimeZone.getDefault(); // Europe/Warsaw
//        offset = tz_local.getOffset(new Date().getTime());
    }

    @Override
    public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
            throws JsonParseException {

//        Log.d("DateSerializer", "parsing: " + json.getAsString());
        if (json.getAsString() == null || json.getAsString().length() == 0) {
            return null;
        }
        try {
            Date ret = df.parse(json.getAsString());
//            Log.d("DateSerializer", "parsed: " + ret);
            return ret;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {

        // return new JsonPrimitive(arg0 == null ? 1 : 0);
        return new JsonPrimitive(src == null ? ""  : df.format(src)) ;
    }

//    public static long toUTCTime(@NonNull Date d) {
//        return d.getTime();
//    }
//    public static long getLocalTime(@NonNull Date utcDate) {
//        return (utcDate.getTime() + offset);
//    }
}
